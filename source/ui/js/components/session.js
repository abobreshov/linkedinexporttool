ko.components.register('session', {
    viewModel: function(params) {
        this.enable = params.enable;
        this.session = params.session;
    },
    template: 'Message: <input data-bind="value: text" /> (length: <span data-bind="text: text().length"></span>)'
});	