(function(){

/*
INIT
1. Get License
2. Get Settings
3. Get History
METHODS
1. Lite export
2. Full export
3. Resume
4. Download session
5. Resume from number

*/

    'use strict';

    var helper = linkedInTool.modules.require('helper'),
        exportHelper = linkedInTool.modules.require('exportHelper'),
        profile = linkedInTool.modules.require('mainProfile'),
        license = linkedInTool.modules.require('license'),
        settings = linkedInTool.modules.require('settings');

    var viewModel = (function() {
      var self = this;

      var exportFunc = function(actionName) {
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          console.log('action: ' + actionName);
          var msg = {
            action: actionName, 
            license: self.licenseLevel(),
            settings: self.settings,
            history: self.history
          };
          chrome.tabs.sendMessage(tab.id, msg);
          window.close();
        });
      };

      self = {
        licenseDescription: '',
        licenseLevel: ko.observable(0),
        hasLicense: ko.observable(false)
      };
      
      self.fullExport = function() {
        if(self.licenseLevel() > 0){
          return exportFunc('doExport');
        }
      };

      self.liteExport = function() {
        return exportFunc('doLiteExport');
      };



      self.getInfo = function(session) {
        chrome.tabs.create({url: 'ui/html/details.html', active: false}, function(tab){
          setTimeout(function(){    // hack: to wait tab load 
            chrome.tabs.sendMessage(tab.id, session);
          }, 300);
        });
      };

      self.downloadSession = function(session) {
        var results = session.results.map(function(r) {
          var p = profile();
          p.init(r);
          return p;
        });

        if(results.length > 0){
          if(settings.data.fileFormat.type === 'XML'){
            var xml = exportHelper.makeXML(results);
            exportHelper.download(xml, 'xml');
          } else if(settings.data.fileFormat.type === 'CSV'){
            var csv = exportHelper.makeCSV(results, settings.data.fileFormat.settings.separator);
            exportHelper.download(csv, 'csv');
          }
        }
      };

      self.resumeExport = function(session) {
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          var msg = {
            action: 'resumeExport',
            data: session.id,
            settings: self.settings,
            history: self.history,
            license: self.licenseLevel() 
          };
          chrome.tabs.sendMessage(tab.id, msg);
          window.close();
        });
      };

      self.resumeFrom = function(session) {
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          var msg = {
            action: 'resumeExportFrom',
            data: session.id,
            settings: self.settings,
            history: self.history,
            license: self.licenseLevel() 
          };
          chrome.tabs.sendMessage(tab.id, msg);
          window.close();
        }); 
      };

      self.removeSession = function(session) {
        self.sessions.remove(session);
        chrome.storage.local.set({'history': self.sessions() }, function(){
          if(chrome.runtime.lastError){
            console.error('Error on saving to local storage');
          }
        });
      };

      return self;
    })();
 
    viewModel.buttonStatus = ko.pureComputed(function() {
          return  (this.hasLicense() === true && this.licenseLevel > 0)? 'btn-primary' : 'btn-default btn-disabled';
      }, viewModel);



    var initializeLicense = function() {
      $('#popupLi').block({ message: '<h5><small>Retriving license<small></h5>' });
      return license.license;
    };

    var domReady = Rx.DOM.ready().flatMap(initializeLicense).retry(3);

    var source = Rx.Observable.forkJoin(domReady, settings.settings, settings.history).subscribe(
        function(x) {
          viewModel.licenseDescription = x[0].accessDescription;
          viewModel.licenseLevel(x[0].accessLevel());
          if (viewModel.licenseLevel() < 1 && viewModel.licenseDescription === 'FREE_TRIAL') {
            viewModel.licenseDescription = 'TRIAL_EXPIRED';
          }
          viewModel.hasLicense(x[0].result);
          viewModel.todayParsed = x[1].stat.dayCounter;
          viewModel.totalParsed = x[1].stat.contactsCounter;
          viewModel.limit = x[1].maxContacts;
          viewModel.sessions = ko.observableArray(x[2]);
          viewModel.settings = x[1];
          viewModel.history = x[2];

          console.log(viewModel.hasLicense());
        },
        function(error) {
          console.error(error);
        },
        function() {
          console.log('Complete');
          console.log(viewModel);
          $('div.container').unblock();
          ko.applyBindings(viewModel, document.getElementById('popupLi'));
        });
})();