(function(){
	'use strict';

	$('#description').block();

	chrome.runtime.onMessage.addListener(function(session, sender, sendResponse){	
		var viewModel = (function(s){
			var self = {};
			self.id = s.id;
			self.date = s.date;

			self.members = ko.observableArray([]);

			for (var i = 0; i < s.results.length; i++) {
				self.members.push({ isActive: false, member: s.results[i] });
			}

			for (i = 0; i < s.members.length; i++) {
				self.members.push({ isActive: true, member: s.members[i] });
			}
			//todo problem with cyrilic symbols it shows as unicode html :(
			return self;
		})(session);

		ko.applyBindings(viewModel, document.getElementById('description'));
		$('#description').unblock();
	});
})();