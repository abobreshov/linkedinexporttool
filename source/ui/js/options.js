(function(){
    'use strict';
    
    var helper = linkedInTool.modules.require('helper'),
        exportHelper = linkedInTool.modules.require('exportHelper'),
        profile = linkedInTool.modules.require('mainProfile'),
        settings = linkedInTool.modules.require('settings');

    var viewModel = (function() {
      var fileFormatSave = function(fileFormat) {
          var obj = { 'fileFormat': fileFormat };
          chrome.storage.sync.set(obj, function(resp) {
              console.log('file format is saved');
          });
      };

      var self = {
        settings: null,
        saveSeparator: function(sep) {
          //dump validation
          if (sep !== ';' && sep !== ',' && sep !== '|' ) {
            sep = ';';
          }

          self.settings.separator(sep);
          self.settingsSource.fileFormat.settings.separator = sep;
          fileFormatSave(self.settingsSource.fileFormat);
        },
        saveFileType: function(data) {
          //dump validation
          if (data !== 'XML' && data !== 'CSV' && data !== 'vCard' ) {
            data = 'CSV';
          }

          self.settings.fileFormat(data);
          self.settingsSource.fileFormat.type = data;
          fileFormatSave(self.settingsSource.fileFormat);
        },
        saveMaxContacts: function() {
          var number = self.settings.maxContacts();
          
          //dump validation and save/or restore
          if(!isNaN(number) && number > -1) {
            self.settingsSource.maxContacts = number;
            var obj = {'maxContacts': number };
            chrome.storage.sync.set(obj, function(resp) {
              console.log('maxContacts is saved');
            });
          } else {
            self.settings.maxContacts(self.settingsSource.maxContacts);
          }
        },
        saveDelay: function() {
          var number = self.settings.delay();

          //dump validation and save/or restore
          if(!isNaN(number) && number >= 10 && number <=10000) {
            var obj = {'delay': number };
            chrome.storage.sync.set(obj, function(resp){
                console.log('delay is saved');
            });
          } else {
            self.settings.delay(self.settingsSource.delay);
          }
        }
      };

      return self;
    })();
    
    var domReady = Rx.DOM.ready();

    var source = Rx.Observable.forkJoin(domReady, settings.settings).subscribe(
        function(x) {
          viewModel.settings = {
            delay: ko.observable(x[1].delay),
            maxContacts: ko.observable(x[1].maxContacts),
            fileFormat: ko.observable(x[1].fileFormat.type),
            separator: ko.observable(x[1].fileFormat.settings.separator)
          };
          viewModel.settingsSource = x[1];

          console.log('Next');
        },
        function(error) {
          console.error(error);
        },
        function() {
          console.log('Complete');
          ko.applyBindings(viewModel);
        });
})();

