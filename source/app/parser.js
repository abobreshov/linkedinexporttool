linkedInTool.modules.define('parser', function (module, exports) {
  'use strict';
  var info_url = 'https://www.linkedin.com/profile/mappers?id={{id}}&locale=en_US&authType=name&x-a=profile_v2_contact_info',
      companyBaseUrl = 'https://www.linkedin.com/company/';

  var self = exports,
   parseHelper = linkedInTool.modules.require('parseHelper'),
   mainProfile = linkedInTool.modules.require('mainProfile'),
   helper = linkedInTool.modules.require('helper');

  var parseContactJson = function (jsonObj) {
    var pr,
      p = mainProfile();
    if (jsonObj.content && jsonObj.content.ContactInfo && jsonObj.content.ContactInfo.relationship_info) {
      pr = jsonObj.content.ContactInfo.relationship_info.contactDetails;
    }

    if (pr) {
      p.fullname = pr.name;

      if (pr.emails && pr.emails.length > 0) {
        var primaryEmail = _.find(pr.emails, function(x) { return x.primary; });
        if (!primaryEmail) {
          primaryEmail = pr.emails[0];
        }
        p.email = primaryEmail.text;
      }

      if (pr.instantMessageHandles && pr.instantMessageHandles.length > 0) {
        var primaryIm = _.find(pr.instantMessageHandles, function(x) { return x.primary; });
        if (!primaryIm) {
          primaryIm = pr.instantMessageHandles[0];
        }
        p.im = primaryIm.type.concat(': ', primaryIm.text);
      }


      // "sites": [
      //{"name": "Personal Website", "visible": true, "url": "http://", "blog": false, "source": "LinkedIn", "id": "Personal Website"},
      //{"name": "Company Website", "visible": true, "url": "http://", "blog": false, "source": "LinkedIn", "id": "Company Website"},
      //{"name": "abobreshov", "visible": true, "url": "http://twitter.com/", "blog": false, "source": "LinkedIn", "id": "abobreshov"}]
      if (pr.websites && pr.websites.length > 0) {
        var primaryWebsite = _.find(pr.websites, function(x) { return x.name === 'Personal Website' || x.primary; });
        if (!primaryWebsite) {
          primaryWebsite = pr.websites[0];
        }
        p.site = primaryWebsite.text;
      }

      return p;
    } else {
      //Todo error
      return null;
    }
  };


  var findAllDataContent = function (html) {
    var content = [], i = 0, j, s, js;

    i = html.indexOf('<!--{', i + 1);
    j = html.indexOf('}-->', i);
    while (i > 0) {
      s = html.substr(i + 4, j - i - 3)
       .replace(/&dsh;/g, '-')
       .replace(/\\u002d([1-9])/g, '$1');

      try {
        js = JSON.parse(s);
        content.push(js);
      } catch (err) {
        js = {};
        console.log('Could not parse:');
        console.error(s);
      }

      i = html.indexOf('<!--{', i + 1);
      j = html.indexOf('}-->', i);
    }

    return content;
  };

  var getDataFromContact = function (data) {
    var parser = parseHelper.contactData;
    var p = mainProfile();

    p.fullname = parser.getFullName(data);
    p.title = parser.getTitle(data);
    p.location = parser.getLocation(data);
    p.company = parser.getCompany(data);
    p.email = parser.getEmail(data);
    p.profile_url = parser.getProfileUrl(data);

    return p;
  };

  var getDataFromContent = function (data) {
    var parser = parseHelper.content;
    var p = mainProfile();

    p.fullname = parser.getFullName(data);
    p.title = parser.getTitle(data);
    p.location = parser.getLocation(data);
    p.company = parser.getCompany(data);
    p.email = parser.getEmail(data);
    p.profile_url = parser.getProfileUrl(data);
    p.industry = parser.getIndustry(data);
    p.distance = parser.getDistance(data);
    p.companyUrl = parser.getCompanyUrl(data);
    p.multipleComp = parser.getIsMultipleComp(data);

    return p;
  };

  var getDataFromHtml = function (html) {
    var data = $(html),
     parser = parseHelper.html,
     p = mainProfile();

    p.fullname = parser.getFullName(data);
    p.title = parser.getTitle(data);
    p.location = parser.getLocation(data);
    p.company = parser.getCompany(data);
    p.email = parser.getEmail(data);
    p.profile_url = parser.getProfileUrl(data);
    p.industry = parser.getIndustry(data);
    p.distance = parser.getDistance(data);
    p.companyUrl = parser.getCompanyUrl(data);
    p.multipleComp = '';

    return p;
  };

  var parseContactHtml = function (htmlText) {
    var profileInfo = [];
    //TODO: there only one data section need to be reviewed
    /*
    var data = findBaseDataContent(htmlText);
    for (var property in data) {
      for (var i = 0; i < data[property].length; i++) {
        var info = data[property][i].data;
        var profile = data[property][i].getData(info);
        profileInfo.push(profile);
      }
    }*/
    var parsedHtmlProfile = getDataFromHtml(htmlText);
    if (parsedHtmlProfile) {
      profileInfo.push(parsedHtmlProfile);
    }

    if (profileInfo.length >= 1) {
      return profileInfo;
    }

    return null;
  };

  var findBaseDataContent = function (html) {
    var data = { content: [], contact_data: [] };
    var content = findAllDataContent(html);
    //debugger;
    try {
      content.map(function (js) {
          if (js.content && js.content.BasicInfo && js.content.TopCard && js.content.ContactInfo) {
            data.content.push({ data: js.content, getData: getDataFromContent });
          } else if (js.contact_data) {
            data.contact_data.push({ data: js.contact_data, getData: getDataFromContact });
          }
        });
    } catch (e) {
      console.log("Can't parse data [findAllDataContent]");
      Bugsnag.notifyException(e, 'parser.js');
      console.error(e);
    }

    return data;
  };


  var validateHtmlResponse = function (html) {
    if ($(html).find('#login').length > 0) {
      return false;
    }

    return true;
  };

  var parseContactsProfile = function (data) {
    try {
      var html = data.htmlData;//HtmlResp[0];
      if (!validateHtmlResponse(html)) {
        return null;
      }

      var json = data.jsonData;//JsonResp[0];

      var contacts = parseContactHtml(html);
      if (!contacts) {
        contacts = [];
      }

      var jsonContact;
      if (json) {
        jsonContact = parseContactJson(json);
      }

      if (jsonContact && contacts && contacts.length >= 0) {
        contacts.push(jsonContact);
      }

      return contacts;

    } catch (err) {
      Bugsnag.notifyException(err, 'parser.js');
      console.error(err);
      return null;
    }
  };

  var parseBaseProfile = function (resp) {
    try {
      var html = resp;
      if (!validateHtmlResponse(html)) {
        return null;
      }

      var contacts = parseContactHtml(html);
      if (!contacts) {
        contacts = [];
      }

      return contacts;
    } catch (err) {
      Bugsnag.notifyException(err, 'parser.js');
      console.error(err);
      return null;
    }

  };

  var parseSalesProfile = function (html) {
    var data = findAllDataContent(html);
    var profileCandidates = [];
    data.filter(function (json) {
      return json.profile && json.currentPosition;
    })
     .forEach(function (json) {
      var p = mainProfile();
      try {
        p.fullname = json.profile.fullname;
        p.location = json.profile.location || '';
        p.title = json.profile.headline;

        if (json.currentPosition.position) {
          p.company = json.currentPosition.position.companyName || '';
          p.companyUrl = companyBaseUrl.concat(json.currentPosition.position.companyId);
        }

        p.industry = json.profile.industry;
        p.distance = json.profile.degree;
        p.profile_url =	json.profile.profileLink;
        p.id = json.profile.memberId;

        //Json.profile.firstName
        //json.profile.lastName
        //json.profile.numConnections

        if (json.profile.contactInfo) {
          var info = json.profile.contactInfo;
          p.profile_url = info.publicProfileUrl || '';
          if (info.emails && info.emails.length > 0) {
            p.email = info.emails.pop();
          }

          if (info.websites && info.websites.length > 0) {
            p.site = '';
            info.websites.map(function (website) {
              return website.join(': ');
            }).forEach(function (websiteItem) {
              p.site = p.site.concat(websiteItem, '  ');
            });
          }

          if (info.twitterAccounts && info.twitterAccounts.length > 0) {
            var twitter = info.twitterAccounts.pop();
            p.im = 'twitter: '.concat(twitter);
          }

          if (info.phones && info.phones.length > 0) {
            p.phone =  info.phones.pop();
          }

          if (info.address && info.address.length > 0) {
            //Todo
          }

        }

        profileCandidates.push(p);
      } catch (e) {
        Bugsnag.notify('parser.js', 'Sales Navigator: Error on parsing contact info', { contactInfo: json.profile.contactInfo, error: e });
      }

    });

    return profileCandidates.length > 0 ? profileCandidates[0] : null;
  };

  //Todo [ab] make constructor class for it
  self.contacts = {
    id: 'contacts',
    parse: function (data) {
      return parseContactsProfile(data);
    },

    requestor: function (url, intId) {
      var id = helper.getParameterByName('id', url);
      var jsonUrl = info_url.replace('{{id}}', intId);

      return Rx.Observable.zip(
       helper.getRequest(url),
       helper.getRequest(jsonUrl),
       function (htmlResp, jsonResp) {
        return {
          htmlData: htmlResp,
          jsonData: jsonResp,
        };
      });
    },
  };

  self.base = {
    id: 'base',
    parse: function (data) {
      return parseBaseProfile(data);
    },

    requestor: function (url) {
      return helper.getRequest(url);
    },
  };

  self.sales = {
    id: 'sales',
    parse: function (data) {
      return parseSalesProfile(data);
    },

    requestor: function (url) {
      return helper.getRequest(url);
    },
  };

  module.exports = self;
});
