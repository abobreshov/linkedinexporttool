linkedInTool.processSearchPage = (function(){
  'use strict';

  if (!Array.prototype.last){
      Array.prototype.last = function(){
          return this[this.length - 1];
      };
  }

  var baseProfile = linkedInTool.modules.require('baseProfile'),
    helper = linkedInTool.modules.require('helper');

  var exporter = linkedInTool.modules.require('exporter'),
   parser = linkedInTool.modules.require('parser'),
   ui = linkedInTool.modules.require('ui');

  var fillMembers = function(searchResults, members) {
    for (var i = 0; i < searchResults.length; i++) {
       var member = searchResults[i].person;
        if(member){
          var p = baseProfile();
          if(member.link_nprofile_view_3){
            p.href = member.link_nprofile_view_3;
          } else if(member.link_nprofile_view_headless){
            p.href  = member.link_nprofile_view_headless;
          } else {
            console.error(member);
            continue;
          }

          if(member.fmt_name){
            p.name = helper.cleanStrValue(member.fmt_name);
          }

          if(member.fmt_location){
            p.location = member.fmt_location;
          }

          if(member.fmt_industry){
            p.industry = member.fmt_industry;
          }

          p.distance = member.distance;
          
          if (p.id) {
            p.id = member.id;
          }

          members.push(p);
        }
    }
  };

  var getMembers = function(settings) {
    var offset = 1;
    var members = [];
    var defer = jQuery.Deferred();
    var subscription = null;
    var endPageFlag = false;

    var url_path = document.location.pathname;
    var info_url = $('#results-pagination a').attr('href');
    if(!info_url && offset !== 1){
      if (members.length > 0) {
        defer.resolve(members);
      }
      Bugsnag.notify('search.js', 'get members from search page: info_url is null or undefined', { currentUrl: document.location }, 'warn');
      ui.viewModel.doClose();
      return;
    } else {
      
      if(!info_url) {
        info_url = document.location.toString();
        endPageFlag = true;
      }

      var url = info_url.replace(url_path, url_path + 'j');
      ui.viewModel.doInit();
      var pauser = ui.viewModel.getPauser();

      ui.viewModel.subscriptions.push(ui.viewModel.onClose.subscribe(
        function() {
          if (subscription) {
            subscription.dispose();
          }
          ui.viewModel.reset();
        },
        function(e) {
          console.error(e);
        },
        function() {
          console.log('onClose complite');
        }
      ));

      var noPagesToParse = function(currentPageNum, pages) {
        var page = pages.last();
        return page.isCurrentPage && currentPageNum >= page.pageNum;
      };


      (function next_page(){
        url = helper.setParameterByName(url, 'page_num', offset);
        var doRequest = true;
        
        function reportError(e) {
          Bugsnag.notifyException(e, "search.js");
          console.error(e);
          doRequest = false;
        }

        subscription = helper.getRequest(url)
         .delay(settings.delay)
         .pausableBuffered(pauser)
         .subscribe(
            function(resp) {
              if (resp.content && resp.content.page && resp.content.page.voltron_unified_search_json && resp.content.page.voltron_unified_search_json.search && (resp.content.page.voltron_unified_search_json.search.baseData.resultPagination || offset === 1) ) {
                var search = resp.content.page.voltron_unified_search_json.search;
                var pagination = search.baseData.resultPagination;
                if (search.results && search.results.length > 0) {
                  ui.viewModel.updateProgress(null, null, '', 'Get contacts links from page ' + offset);
                  try{
                    fillMembers(search.results, members);
                  } catch (e){
                    reportError(e);
                  }
                }
                offset++;
                if (endPageFlag || (!pagination.nextPage && noPagesToParse(offset, pagination.pages))) {
                  doRequest = false;
                }
              } else {
                  var err = { 
                    message: 'Bad response. No search results for page ' + offset, 
                    url: url,
                    response: resp
                  };
                  reportError(err);
              }
            },
            function(err) {
              Bugsnag.notify('search.js', 'Error on members GET request', { response: err });
              console.error(err);
              ui.viewModel.doClose();
              alert("Can't get members. Please refresh the page and try it again");
            },
            function() {
              if (doRequest) {
                next_page();
              } else {
                ui.viewModel.doClose();
                if (members.length > 0) {
                  defer.resolve(members);
                }
              }
            }
          );
         pauser.onNext(true);
      })();
    }
    return defer.promise();
  };


  var resumeExportConnections = function (sessionId, settings, isPause) {
    exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser, settings, isPause);
  };


  var exportConnections = function (isFastTrack, settings) {
    var accId = linkedInTool.account.getAccountId();
    var promise;
    try {
      promise = getMembers(settings);
    } catch (e) {
      Bugsnag.notifyException(e, 'search.js');
    }

    promise.done(function (members) {
      if (!members.length) {
        console.error('Please, specify target contacts to process.');
        return;
      }

      if (isFastTrack) {
        exporter.fastExport(members, settings);
      } else {
        exporter.export(members, null, accId, parser.base, settings);
      }
    });
  };

  chrome.runtime.onMessage.addListener(function(message) {
    if (message.action === 'doExport') {
      exportConnections(false, message.settings);
    }

    if (message.action === 'doLiteExport') {
      exportConnections(true, message.settings);
    }
    
    if (message.action === 'resumeExport' && message.data) {
     resumeExportConnections(message.data, message.settings, false);
    }

    if (message.action === 'resumeExportFrom' && message.data) {
     resumeExportConnections(message.data, message.settings, true);
    }
  });

  ui.init();
})();