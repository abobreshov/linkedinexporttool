linkedInTool.processContactsPage = (function () {
  'use strict';

  var membersLink = 'https://www.linkedin.com/connected/api/{{4}}/{{2}}?start={{0}}&count={{1}}&fields=id%2Cname%2CfirstName%2ClastName%2Ccompany%2Ctitle%2Clocation%2Ctags%2Cemails%2Csources%2CdisplaySources%2CconnectionDate%2CsecureProfileImageUrl&{{3}}',
   contactUrl = '/profile/view?id={{0}}&trk=contacts-contacts-list-contact_name-0';

  var exporter = linkedInTool.modules.require('exporter'),
   parser = linkedInTool.modules.require('parser'),
   ui = linkedInTool.modules.require('ui');

  var getMembers = function (settings) {
    var members = [],
     offset = 0,
     contactsCount = 25,
     link = membersLink,
     defer = jQuery.Deferred(),
     subscription = null;

    var helper = linkedInTool.modules.require('helper'),
     baseProfile = linkedInTool.modules.require('baseProfile');

    var getContact = function (c) {

      var profile = baseProfile();
      profile.company = '';
      if (c.company) {
        profile.company = c.company.name;
      }

      profile.location = '';
      if (c.location) {
        profile.location = c.location;
      }

      profile.href = c.profileUrl;
      profile.id = c.memberId;
      profile.name = c.name;
      profile.title = c.title;
      if(c.phoneNumbers && c.phoneNumbers.length > 0) {
        for (var i = 0; i++; i < c.phoneNumbers.length) {
          profile.phones.push(c.phoneNumbers[i].number);
        }
      }

      //if (c.emails && c.emails.length > 0) {
      //  debugger;
      //}
      return profile;
    };

    var currentUrl = window.location.href;
    
    var filter = (function(url) {

      var param = helper.getParameterByName('source', url);
      if(param) {
        return {
          key: 'connections-only',
          val: 'source=' + param,
          prefix: 'v1/contacts'
        };
      }

      param = helper.getParameterByName('tagId', url);
      if(param) {
        return {
          key: 'search-by-tag',
          val: 'tagId=' + param,
          prefix: 'v1/contacts'
        };
      }

      return {
        key: 'contacts',
        val: '',
        prefix: 'v2'
      };

    })(currentUrl);
    link = link.replace('{{2}}', filter.key).replace('{{3}}', filter.val).replace('{{4}}', filter.prefix);
    ui.viewModel.doInit();
    var pauser = ui.viewModel.getPauser();
    
    ui.viewModel.subscriptions.push(ui.viewModel.onClose.subscribe(
      function() {
        if (subscription) {
          subscription.dispose();
        }
        ui.viewModel.reset();
      },
      function(e) {
        console.error(e);
      },
      function() {
        console.log('onClose complite');
      }
    ));

    (function next_page() {
      var doRequest = true;
      var url = link.replace('{{0}}', offset).replace('{{1}}', contactsCount);
      subscription = helper.getRequest(url)
       .delay(settings.delay)
       .pausableBuffered(pauser)
       .subscribe(
        function (resp) {
          if (resp.values && resp.values.length > 0) {
            for (var i = 0; i < resp.values.length; i++) {
              var contact = getContact(resp.values[i]);
              ui.viewModel.updateProgress(i + offset + 1, resp.paging.total, '', 'Get contacts links: ');
              members.push(contact);
            }

            if (resp.paging && resp.paging.total > (resp.paging.count + resp.paging.start)) {
              offset += contactsCount;
            } else {
              doRequest = false;
            }
          } else {
            doRequest = false;
            Bugsnag.notify('ContactsMembersError', 'Contacts parsing problem.', { response: resp });
            console.error("Can't get list of contacts to export. Seems like something went wrong");
          }
        },

        function (err) {
          Bugsnag.notify('connected.js', 'Error on members GET request', { response: err });
          console.error(err);
          ui.viewModel.doClose();
          alert("Can't get members. Please refresh the page and try it again");
        },

        function () {
          if (doRequest) {
            next_page();
          } else {
            ui.viewModel.doClose();
            defer.resolve(members);
          }
        });

       pauser.onNext(true);
    })();
    //pauser.onNext(true);
    return defer.promise();
  };

  var resumeExportConnections = function (sessionId, settings, isPause) {
    exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser, settings, isPause);
  };

  var exportConnections = function (isFastTrack, settings) {
    var accId = linkedInTool.account.getAccountId();
    var promise;
    try {
      promise = getMembers(settings);
    } catch (e) {
      Bugsnag.notifyException(e, 'connected.js');
    }

    promise.done(function (members) {
      if (!members.length) {
        console.error('Please, specify target contacts to process.');
        return;
      }

      if (isFastTrack) {
        exporter.fastExport(members, settings);
      } else {
        exporter.export(members, null, accId, parser.contacts, settings);
      }
    });
  };

  chrome.runtime.onMessage.addListener(function (message/*, sender, sendResponse*/) {
    if (message.action === 'doExport') {
      exportConnections(false, message.settings);
    }

    if (message.action === 'doLiteExport') {
      exportConnections(true, message.settings);
    }
    
    if (message.action === 'resumeExport' && message.data) {
     resumeExportConnections(message.data, message.settings, false);
    }

    if (message.action === 'resumeExportFrom' && message.data) {
     resumeExportConnections(message.data, message.settings, true);
    }

  });

  ui.init();

})();
