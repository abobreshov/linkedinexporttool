linkedInTool.modules.define('exportHelper', function (module, exports) {
  'use strict';
  var self = exports;

  var titles = linkedInTool.modules.require('mainProfile')().getProfileFields();

  self.makeXMLElement = function (name, value) {
    return '<' + name + '>' + value + '</' + name + '>';
  };

  var getCSVHeaderFromObject = function (object, separator) {
    var result = '';
    for (var property in object) {
      if (object.hasOwnProperty(property)) {
        var t = object[property];
        result = result.concat('"', t, '"', separator);
      }
    }

    return result.concat('\n');
  };

  self.makeCSV = function (results, separator) {
    var header = getCSVHeaderFromObject(titles, separator);
    return header +
     results.map(function (tp) {
                if (tp) {
                  return tp.toCSV(separator);
                } else {
                  return '';
                }
              }).join('\n');
  };

  self.makeXML = function (results) {
    return '<result>\n' +
     results.map(function (tp) {
                if (tp) {
                  return tp.toXML();
                } else {
                  return '';
                }

              }).join('\n') +
              '\n</result>';
  };

  self.makeVCard = function (results) {
    return results.map(function (tp) {
                if (tp) {
                  return tp.toVCard();
                } else {
                  return '';
                }
              }).join('\n');
  };

  self.download = function (str, contentType, fileName) {
    if (!fileName || fileName === '' || typeof fileName !== 'string') {
      fileName = 'LinkedIn Connections';
    }

    var a = document.createElement('a');
    a.id = 'mysave';
    a.download = fileName + '.' + contentType;
    a.href = 'data:application/' + contentType + ';charset=utf-8,' + encodeURIComponent(str);
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(document.getElementById('mysave'));
  };

  module.exports = self;
});
