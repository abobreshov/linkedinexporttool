linkedInTool.modules.define('counter', function (module, exports) {
  'use strict';
  var self = exports,
   _dailyCounter = 0,
   _counter = 0,
   _date = new Date().getTime(),
   obj = { dayCounter: _dailyCounter, contactsCounter: _counter, lastDate: _date },
   helper = linkedInTool.modules.require('helper');

  self.set = function (dailyContacts, contacts, lastDate) {
    _counter = contacts;
    if (lastDate && typeof lastDate === 'number') {
      var ld = new Date(lastDate);
      var cd = new Date(_date);
      if (helper.isSameDay(cd, ld)) {
        _dailyCounter = dailyContacts;
      }
    }
  };

  self.get = function () {
    obj.dayCounter = _dailyCounter;
    obj.contactsCounter = _counter;
    obj.lastDate = _date;
    return obj;
  };

  self.getDayCounter = function () {
    return _dailyCounter;
  };

  self.getCommonCounter = function () {
    return _counter;
  };

  self.increment = function () {
    var d = new Date();
    var ld = new Date(_date);
    if (helper.isSameDay(d, ld)) {
      _dailyCounter++;
    } else {
      _dailyCounter = 0;
      _date = d.getTime();
    }

    _counter++;
  };

  self.commit = function () {
    var defer = jQuery.Deferred();
    var stat = self.toJson();

    chrome.storage.sync.set(stat, function (resp) {
                console.info('Export statistics saved...');
                defer.resolve(resp);
              });

    return defer.promise();
  };

  self.toJson = function () {
    return { stat: {
      dayCounter: _dailyCounter,
      lastDate: new Date().getTime(),
      contactsCounter: _counter
    },
    };
  };

  self.parseJson = function (obj) {
    self.set(obj.stat.dayCounter, obj.stat.contactsCounter, obj.stat.lastDate);
  };

  module.exports =  self;
});
