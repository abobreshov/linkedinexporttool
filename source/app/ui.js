linkedInTool.modules.define('ui', function (module, exports) {
  'use strict';
  var self = exports;

  self.viewModel = (function () {
      var states = {
        pause: 'pause',
        working: 'working',
        closed: 'closed',
        init: 'init'
      };


      //-------------------------------------------------- private fields
      var prv = {
        state: ko.observable(states.closed),
        pauser: new Rx.Subject()
      };


      //---------------------------------------------- public fields & computed
      var vm = {
        name: ko.observable(''),
        progressTxt: ko.observable(''),
        title: ko.observable('Exporting connections'),
        goToValue: ko.observable(1),
        minGoToValue: ko.observable(1),
        maxGoToValue: ko.observable(2)
      };

      vm.isPause = ko.computed(function () {
        return this.state() === states.pause;
      }, prv);

      vm.isWorking = ko.computed(function () {
        return this.state() === states.working;
      }, prv);

      vm.isVisible = ko.computed(function () {
        return this.state() !== states.closed;
      }, prv);

      vm.isControl = ko.computed(function () {
        return this.state() !== states.init;
      }, prv);

      //-------------------------------------------------- methods

      vm.setState = function (s) {
        var tmp = states[s];
        if (tmp) {
          prv.state(tmp);
        }
      };


      vm.getState = function () {
        return prv.state();
      };

      vm.updateProgress = function (currentVal, maxVal, description, currentStateInfo) {
        if (!description) {
          description = '';
        } else {
          vm.title(description);
        }

        if (!currentStateInfo) {
          currentStateInfo = '';
        }

        var progressStr = '';
        if (currentVal && maxVal) {
          progressStr = currentVal + ' / ' + maxVal;
          vm.maxGoToValue(maxVal);
        }

        if (currentVal) {
          vm.minGoToValue(currentVal);
          vm.goToValue(currentVal);
        }

        vm.name(currentStateInfo);
        vm.progressTxt(progressStr);
      };

      vm.onSave = new Rx.Subject();
      vm.doSave = function () {
        vm.onSave.onNext();
        vm.setState(states.closed);
        prv.pauser.onNext(false);
      };

      vm.onClose = new Rx.Subject();
      vm.doClose = function () {
        vm.onClose.onNext();
        vm.setState(states.closed);
        prv.pauser.onNext(false);
      };


      vm.onPause = new Rx.Subject();
      vm.doPause = function () {
        prv.pauser.onNext(false);
        vm.setState(states.pause);
        vm.onPause.onNext();
      };

      vm.onGoTo = new Rx.Subject();
      vm.doGoTo = function () {
        vm.onGoTo.onNext(vm.goToValue());
        prv.pauser.onNext(false);
      };


      vm.onStart = new Rx.Subject();
      vm.doStart = function () {
        vm.setState(states.working);
        prv.pauser.onNext(true);
        vm.onStart.onNext();
      };

      vm.subscriptions = [];

      vm.doInit = function () {
        vm.dispose();
        vm.reset();
        vm.setState(states.init);
        prv.pauser.onNext(true);
      };

      vm.reset = function () {
        vm.name('');
        vm.progressTxt('');
        prv.pauser = new Rx.Subject();
        vm.onStart = new Rx.Subject();
        vm.onPause = new Rx.Subject();
        vm.onClose = new Rx.Subject();
        vm.onGoTo = new Rx.Subject();
      };

      vm.dispose = function() {
        prv.pauser.dispose();
        vm.onStart.dispose();
        vm.onPause.dispose();
        vm.onClose.dispose();
        vm.onGoTo.dispose();
        for (var i = 0; i < vm.subscriptions.length; i++) {
          vm.subscriptions.pop().dispose();
        }
      };

      vm.getPauser = function () {
        return prv.pauser;
      };

      return vm;
    })();

  var createProgressBox = function () {
    var template = `
    <div id="extProgressBox" class="ext_ProgressBox" data-bind="visible: isVisible">
      <span class="ext_title" data-bind="text: title"></span> <button class="ext_ActionButton ext_close" id="exp_conn_close_btn" data-bind="click: doClose"></button>
      <div class="ext_ExportBox ext_info">
        <span data-bind="text: name"></span>
        <br>
        <span data-bind="text: progressTxt"></span>
        <div data-bind="visible: isControl">
          <div class="ext_row">
            <button class="ext_ActionButton" id="exp_conn_start_btn" data-bind="enable: isPause, click: doStart">Resume</button>
            <button disabled="" class="ext_ActionButton" id="exp_conn_pause_btn" data-bind="enable: isWorking, click: doPause">Pause</button>       
          </div>
          <div data-bind="visible: isPause">
            <div class="ext_row">
              <input type="number" class="ext_NumberInput" name="quantity" data-bind="textInput: goToValue, attr: { min: minGoToValue, max: maxGoToValue }" />
              <button class="ext_ActionButton" id="exp_conn_goto_btn" data-bind="click: doGoTo">GoTo</button>   
            </div>
            <div class="ext_row">
              <button class="ext_ActionButton" id="exp_conn_save_btn" data-bind="click: doSave">Save & Close</button>
            </div>
          </div>
        </div>
      </div>
    `;
    $(template).appendTo('body');

    ko.applyBindings(self.viewModel, document.getElementById('extProgressBox'));
  };

  self.init = function () {
    createProgressBox();
  };

  module.exports = self;
});
