linkedInTool.modules.define('parseHelper', function (module, exports) {
  var self = exports;

  self.html = {
    getFullName: function (htmlJQuery) {
      return htmlJQuery.find('div.profile-overview span.full-name').text();
    },

    getTitle: function (htmlJQuery) {
      return htmlJQuery.find('#headline p.title').text();
    },

    getLocation: function (htmlJQuery) {
      return htmlJQuery.find('#location a[name="location"]').text();
    },

    getDistance: function (htmlJQuery) {
      var distanceStr = htmlJQuery.find('span.fp-degree-icon abbr.degree-icon')
       .clone()    //Clone the element
       .children() //Select all the children
       .remove()   //Remove all the children
       .end()		//Again go back to selected element
       .text();
      var distance = parseInt(distanceStr, 10);
      if (isNaN(distance)) {
        distance = '';
      }

      return distance;
    },

    getIndustry: function (htmlJQuery) {
      return htmlJQuery.find('#location a[name="industry"]').text();
    },

    getCompany: function (htmlJQuery) {
      return htmlJQuery.find('h5 span.new-miniprofile-container a').first().text();
    },

    getCompanyUrl: function (htmlJQuery) {
      return htmlJQuery.find('h5 span.new-miniprofile-container a').first().attr('href');
    },

    getProfileUrl: function (htmlJQuery) {
      return htmlJQuery.find('.view-public-profile').text();
    },

    getEmail: function (htmlJQuery) {
      return htmlJQuery.find('#email-view a').first().text();
    },

    getAddress: function (htmlJQuery) {
      return htmlJQuery.find('#address-view li').first().text();
    },

    getPhone: function (htmlJQuery) {
      return htmlJQuery.find('#phone-view li').text();
    },

    //Todo im <div id='im'/> <div id='im'/>	<div id='website'/>	<div id='twitter'/>

  };

  self.contactData = {
    getFullName: function (data) {
      if (data && data.name) {
        return data.name;
      } else {
        console.log("Can't find fullname from parsed data");
        return '';
      }
    },

    getTitle: function (data) {
      if (data && data.title) {
        return data.title;
      } else {
        console.log("Can't find title from parsed data");
        return '';
      }
    },

    getLocation: function (data) {
      if (data && data.location) {
        return data.location;
      } else {
        console.log("Can't find location from parsed data");
        return '';
      }
    },

    getCompany: function (data) {
      if (data && data.company && data.company.name) {
        return data.company.name;
      } else {
        console.log("Can't find company name from parsed data");
        return '';
      }
    },

    getEmail: function (data) {
      if (data && data.emails && data.emails.length > 0) {
        return data.company.emails[0].email;
      } else {
        console.log("Can't find email from parsed data");
        return '';
      }
    },

    getProfileUrl: function (data) {
      if (data && data.profiles && data.profiles.length > 0) {
        return data.profiles[0].url;
      } else {
        console.log("Can't find profile url from parsed data");
        return '';
      }
    },

    // Also can get ims, twitter, addresses, sites, phone, companyId
    // todo ?

  };

  self.content = {
    getFullName: function (data) {
      if (data.BasicInfo && data.BasicInfo.basic_info && data.BasicInfo.basic_info.fullname) {
        return data.BasicInfo.basic_info.fullname;
      } else {
        console.log("Can't find fullname from parsed data");
        return '';
      }
    },

    getTitle: function (data) {
      if (data.TopCard && data.TopCard.basic_info && data.TopCard.basic_info.memberHeadline) {
        return data.TopCard.basic_info.memberHeadline;
      } else {
        console.log("Can't find title from parsed data");
        return '';
      }
    },

    getLocation: function (data) {
      if (data.BasicInfo && data.BasicInfo.basic_info && data.BasicInfo.basic_info.fmt_location) {
        return data.BasicInfo.basic_info.fmt_location;
      } else {
        //Todo try to get from another source
        //data.BasicInfo.basic_info.location_highlight
        console.log("Can't find location from parsed data");
        return '';
      }
    },

    getDistance: function (data) {
      if (data.ContactInfo && data.ContactInfo.distance && data.ContactInfo.distance.distance) {
        return data.ContactInfo.distance.distance;
      } else {
        console.log("Can't find distance from parsed data");
        return 0;
      }
    },

    getIndustry: function (data) {
      if (data.BasicInfo && data.BasicInfo.basic_info && data.BasicInfo.basic_info.industry_highlight) {
        return data.BasicInfo.basic_info.industry_highlight;
      } else {
        console.log("Can't find industry from parsed data");
        return '';
      }
    },

    getCompany: function (data) {
      if (data.TopCard && data.TopCard.positionsMpr && data.TopCard.positionsMpr.firstTopCurrentPosition && data.TopCard.positionsMpr.firstTopCurrentPosition.companyName) {
        return data.TopCard.positionsMpr.firstTopCurrentPosition.companyName;
      } else {
        console.log("Can't find company name from parsed data");
        return '';
      }
    },

    getEmail: function (data) {
      if (data.ContactInfo && data.ContactInfo.contact_info && data.ContactInfo.contact_info.emails && data.ContactInfo.contact_info.emails.length > 0) {
        return data.ContactInfo.contact_info.emails[0].email;
      } else {
        console.log("Can't find email from parsed data");
        return '';
      }
    },

    getProfileUrl: function (data) {
      //Debugger;
      if (data.TopCard && data.TopCard.public_url && data.TopCard.public_url.canonicalUrl) {
        return data.TopCard.public_url.canonicalUrl;
      } else {
        console.log("Can't find profile url from parsed data");
        return '';
      }
    },

    getIsMultipleComp: function (data) {
      //Debugger;
      if (data.TopCard && data.TopCard.positionsMpr && data.TopCard.positionsMpr.topCurrent) {
        return data.TopCard.positionsMpr.topCurrent.length > 1 ? 'yes' : 'no';
      } else {
        console.log("Can't find multipleComp from parsed data");
        return '';
      }
    },

    getCompanyUrl: function (data) {
      if (data.TopCard && data.TopCard.positionsMpr && data.TopCard.positionsMpr.firstTopCurrentPosition && data.TopCard.positionsMpr.firstTopCurrentPosition.companyId) {
        var url = 'https://linkedin.com/company/' +
                  data.TopCard.positionsMpr.firstTopCurrentPosition.companyId;
        return url;
      } else {
        console.log("Can't find company url from parsed data");
        return '';
      }
    },
  };

  module.exports = self;
});
