linkedInTool.modules.define('exporter', function (module, exports) {
  'use strict';
  var self = exports;

  var ui = linkedInTool.modules.require('ui'),
   profile = linkedInTool.modules.require('mainProfile'),
   exportHelper = linkedInTool.modules.require('exportHelper'),
   counter = linkedInTool.modules.require('counter'),
   history = linkedInTool.modules.require('history'),
   results = [];

  var downloadFile = function (results, settings) {
    if (settings.fileFormat.type === 'XML') {
      var xml = exportHelper.makeXML(results);
      exportHelper.download(xml, 'xml');
    } else if (settings.fileFormat.type === 'CSV') {
      var csv = exportHelper.makeCSV(results, settings.fileFormat.settings.separator);
      exportHelper.download(csv, 'csv');
    } else if (settings.fileFormat.type === 'vCard') {
      var vCard = exportHelper.makeVCard(results);
      exportHelper.download(vCard, 'vcf');
    }
  };

  self.fastExport = function (members, settings) {
    ui.viewModel.doInit();

    var results = [],
     contactsAmount = members.length;

    for (var i = 0; i < members.length; i++) {
      var p = profile(members[i]);
      p.fillEmptyFields();
      ui.viewModel.updateProgress(i, contactsAmount, 'Contacts:');
      results.push(p);
    }

    ui.viewModel.doClose();

    downloadFile(results, settings);
  };

  self.export = function (members, historyResults, userId, parser, settings, startWithPause) {
    var subscription = null,
     member = null,
     contactsAmount = members.length,
     delayTime = settings.delay;

    results = [];
    counter.set(settings.stat.dayCounter, settings.stat.contactsCounter, settings.stat.lastDate);

    userId = userId || null;

    if (historyResults) {
      contactsAmount += historyResults.length;
      historyResults.map(function (p) {
        var mp = profile();
        mp.init(p);
        results.push(mp);
      });
    }

    ui.viewModel.doInit();
    var pauser = ui.viewModel.getPauser();
    ui.viewModel.doStart();

    if (startWithPause) {
      ui.viewModel.doPause();
    }



    ui.viewModel.subscriptions.push(ui.viewModel.onGoTo.subscribe(function(numText) {
        var num = parseInt(numText);
        var delta = num - (contactsAmount - members.length);
        if (!(delta <= 0 || delta >= members.lenght)) {
          members = members.slice(delta, members.lenght);
        }
      }, function(e) {
        console.error(e);
      }
    ));

    ui.viewModel.subscriptions.push(ui.viewModel.onSave.subscribe(function() {
        if (member && !(_.find(members, function(x) { return x.href === member.href; } ))) {
          members.push(member);
        }
        history.refresh().then(function () {
          history.push(results, members, userId, parser.id);
        });
        if (subscription) {
          subscription.dispose();
        }
        ui.viewModel.reset();
        counter.commit();
      }));

    ui.viewModel.subscriptions.push(ui.viewModel.onClose.subscribe(function() {
        if (subscription) {
          subscription.dispose();
        }
        ui.viewModel.reset();
        counter.commit();
      }, function(e) {
        console.error(e);
      }
    ));


    try {
      (function next_contact() {
        var url;

        if (!members.length || !ui.viewModel.isVisible()) {
          
          if(ui.viewModel.isVisible()) {
            ui.viewModel.doClose(); // <-------------------   ???
          }

          try {
            downloadFile(results, settings);
          } catch (e) {
            Bugsnag.notifyException(e, 'DownloadFileError');
          }

          counter.commit();
          return;
        }

        var todayExported = counter.getDayCounter();
        
        if (settings.maxContacts > 0 && settings.maxContacts <= todayExported)
        {
          history.refresh().then(function () {
            history.push(results, members, userId, parser.id);
          });

          counter.commit();
          if (results.length > 0) {
            downloadFile(results, settings);
          }
          Bugsnag.notify('exporter.js', 'Daily limit (' + settings.maxContacts + ') has reached', settings, 'info');
          ui.viewModel.doClose();
          return;
        }
        
        member = members.shift();
        ui.viewModel.updateProgress(contactsAmount - members.length, contactsAmount, 'Contact:', member.name);
        url = member.href;
        url = url.replace(/^http:\/\//i, 'https://');
        
        subscription = parser.requestor(url, member.id)
          .pausableBuffered(pauser)
          .delay(delayTime)
          .subscribe(
           function (x) {
            var data = parser.parse(x);
            var p = profile(member);
            if (!data || data.length === 0) {
              members.push(member);
              history.refresh().then(function () {
                history.push(results, members, userId, parser.id);
              });

              if (results.length > 0) {
                downloadFile(results, settings);
              }

              Bugsnag.notify('exporter.js', "ParsingProblem: Can't extract any data from url. Possible banned.", { url: url, response: x });
              ui.forceStop = true;
              alert('LinkedIn has asked to logon or banned you. Extraction progress has stored in local storage. Please, refresh the page and resume extraction process from history.');
              return;
            }

            counter.increment();
            p.merge(data);
            results.push(p);

          },
           function (err) {
            //Todo stop and save - notify user
            Bugsnag.notify('exporter.js', 'ContactsRequestException: Contacts GET request fail', { response: err });
            members.push(member);
            history.refresh().then(function () {
              history.push(results, members, userId, parser.id);
            });

            if (results.length > 0) {
              downloadFile(results, settings);
            }

            alert('Upss! Something goes wrong. Possible LinkedIn has asked to logon or banned you. Extraction progress has stored in local storage. Please, refresh the page and resume extraction process from history.');
          },
           function () {
            next_contact();
            pauser.onNext(true);
          }
        );
      })();
    } catch (err) {
      Bugsnag.notifyException(err, 'exporter.js');

      history.refresh().then(function () {
        history.push(results, members, userId, parser.id);
      });

      counter.commit();
      ui.removeProgressBox();
      if (results.length > 0) {
        downloadFile(results, settings);
      }

      console.error(err);
      alert('Upss! Something goes wrong. Possible LinkedIn has asked to logon or banned you. Extraction progress has stored in local storage. Please, refresh the page and resume extraction process from history.');
    }
    if (!startWithPause) {
      pauser.onNext(true);
    }
  };

  self.resume = function (sessionId, userId, parsers, settings, isPause) {
    var session = history.popById(sessionId);
    if (session) {
      if (userId && session.userId && userId !== session.userId) {
        if (history) {
          history.pushSession(session).then(function () {
            console.error("Can't resume export. Session was saved by another user. (Current user id:" + userId + ' Session user id:' + session.userId + ')');
          });
        }
      } else {
        var members = session.members;
        var historyResults = session.results;
        if (!session.parserId) {
          session.parserId = 'base';
        }

        var parser = parsers[session.parserId];

        self.export(members, historyResults, userId, parser, settings, isPause);
      }
    } else {
      var msg = 'Please, refresh the page and try it again.';
      Bugsnag.notify('exporter.js', 'ResumeError: ', 'Can not get session from history. Session is null or undefined');
      console.error(msg);
    }
  };

  module.exports = self;
});
