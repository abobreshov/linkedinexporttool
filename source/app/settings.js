linkedInTool.modules.define('settings', function (module, exports) {
  'use strict';
  var self = exports;
  var counter = linkedInTool.modules.require('counter');

  self.settings = Rx.Observable.create(function (observer) {
    var isActive = true;
    chrome.storage.sync.get(['fileFormat', 'stat', 'maxContacts', 'delay'],
     function (data) {
      var s = {};
      if (!isActive) {
        return;
      }

      if (!data) {
        observer.onError("Can't get settings from sync storage");
        return;
      }

      if (data.fileFormat) {
        s.fileFormat = data.fileFormat;
      } else {
        s.fileFormat = { type: 'XML', settings: { separator: ';' } };
      }

      if (data.stat) {
        if (data.stat.lastDate && typeof data.stat.lastDate === 'number') {
          counter.parseJson(data);
        }
      }
      s.stat = counter.get();

      if (data.maxContacts) {
        s.maxContacts = parseInt(data.maxContacts);
      } else {
        s.maxContacts = linkedInTool.licenseRestriction.maxContacts;
      }

      if (data.delay) {
        s.delay = parseInt(data.delay);
      } else {
        s.delay = 0;
      }

      self.data = s;
      
      //console.log('settings:');
      //console.log(s);

      observer.onNext(s);
      observer.onCompleted();
    });

    return function () {
      isActive = false;
    };
  });

  self.history = Rx.Observable.create(function (observer) {
    var isActive = true;

    chrome.storage.local.get('history',
      function (data) {
        console.log('history:');
        console.log(data);
        if (!isActive) {
          return;
        }

        if (!data) {
          observer.onError("Can't get history from local storage");
        } else if (!data.history) {
          data.history = [];
        }

        observer.onNext(data.history);
        observer.onCompleted();
      }
    );

    return function () {
      isActive = false;
    };
  });

  module.exports = self;
});
