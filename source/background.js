(function(){
'use strict';
  chrome.runtime.onInstalled.addListener(function() {
    // Replace all rules ...
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
      // With a new rule ...
      chrome.declarativeContent.onPageChanged.addRules([
        {
          conditions: [
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/connected' }
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/vsearch/p' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/vsearch/f' },
            })//,
            //new chrome.declarativeContent.PageStateMatcher({
            //  pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/sales/search' },
            //})
          ],
          // And shows the extension's page action.
          actions: [ new chrome.declarativeContent.ShowPageAction() ]
        }
      ]);
    });
  });
})();