linkedInTool.modules.define('exporter', function(module, exports){
	'use strict';
	var self = exports;
		
	var ui = linkedInTool.modules.require('ui'),
		profile = linkedInTool.modules.require('mainProfile'),
		settings = linkedInTool.modules.require('settings'),
		exportHelper = linkedInTool.modules.require('exportHelper'),
		counter = linkedInTool.modules.require('counter'),
		history = linkedInTool.modules.require('history');

	var downloadFile = function (results){
		if(settings.fileFormat.type === 'XML'){
			var xml = exportHelper.makeXML(results);
			exportHelper.download(xml, 'xml');
		} else if(settings.fileFormat.type === 'CSV'){
			var csv = exportHelper.makeCSV(results, settings.fileFormat.settings.separator);			
			exportHelper.download(csv, 'csv');
		} else if(settings.fileFormat.type === 'vCard'){
			var vCard = exportHelper.makeVCard(results);
			exportHelper.download(vCard, 'vcf');
		}		
	};

	self.fastExport = function(members){
		ui.createProgressBox();

		var results = [],
			contactsAmount = members.length;
		
		for(var i = 0; i < members.length; i++){
			var p = profile(members[i]);
			p.fillEmptyFields();
			ui.updateCurrentProgress(i, contactsAmount, 'Contacts:');
			results.push(p);
		}

		ui.removeProgressBox();

		downloadFile(results);
	};

	self.export = function(members, historyResults, userId, parser){
		var results = [],
			contactsAmount = members.length,
			delayTime = settings.delay;

		console.log('delayTime:' + delayTime);

		userId = userId || null;

		if(historyResults){
			contactsAmount += historyResults.length;
			historyResults.map(function(p){
				results.push(profile(p));
			});
		}

		ui.createProgressBox();
		ui.enablePauseButton();
		try {		
			(function next_contact() {
				var url;

				if (!members.length || ui.forceStop) {
					ui.removeProgressBox();
					
					try{				
						downloadFile(results);
					} catch(e) {
						Bugsnag.notifyException(e, 'DownloadFileError');
					}
					
					counter.commit();
					ui.forceStop = false;				
					return;
				}

				var todayExported = counter.getDayCounter();
				
				if(	ui.onPause() || 
					(settings.maxContacts > 0 && 
					settings.maxContacts <= todayExported)
				){
					history.refresh().then(function(){
						history.push(results, members, userId, parser.parserId);
					});		
					counter.commit();		
					if(results.length > 0){
						downloadFile(results);
					}	
					if(ui.onPause()){
						alert('Pause. All other contacts have stored in local storage.');
					} else {
						alert('You have downloaded ' + settings.maxContacts + ' for today. All other contacts have stored in local storage. Please, resume export tomorrow or use fast export.');
					}
					ui.removeProgressBox();

					return;
				}

				var member = members.shift();	
				ui.updateCurrentProgress(contactsAmount - members.length, contactsAmount, 'Contacts:', member.name);
				url = member.href;
				url = url.replace(/^http:\/\//i, 'https://');			
				
				parser.requestor(url).delay(delayTime).subscribe(
					function(x){
						var data = parser.parse(x);
						var p = profile(member);
						if(!data || data.length === 0){
							members.push(member);
							history.refresh().then(function(){
								history.push(results, members, userId, parser.parserId);
							});
							if(results.length > 0){
								downloadFile(results);
							}

							Bugsnag.notify('ParsingProblem', 'Can\'t extract any data from url. Possible banned.', {url: url, response: x});
							ui.forceStop = true;
							alert('LinkedIn has asked to logon or banned you. Extraction progress has stored in local storage. Please, refresh the page and resume extraction process from history.');							
							return;				
						}

						counter.increment();
						p.merge(data);
						results.push(p);

					},
					function(err){
						//todo stop and save - notify user
						Bugsnag.notify('ContactsRequestException', 'Contacts GET request fail', { response: err });
						members.push(member);
						history.refresh().then(function(){
							history.push(results, members, userId, parser.parserId);
						});

						if(results.length > 0){
							downloadFile(results);
						}
						alert('Upss! Something goes wrong. Possible LinkedIn has asked to logon or banned you. Extraction progress has stored in local storage. Please, refresh the page and resume extraction process from history.');
					},
					function(){
						next_contact();
					});
			})();
		} catch (err){
			Bugsnag.notifyException(err, 'ExporterModule');
			
			history.refresh().then(function(){
				history.push(results, members, userId, parser.parserId);
			});		
			counter.commit();
			ui.removeProgressBox();
			if(results.length > 0){
				downloadFile(results);
			}
			

			console.error(err);
			alert('Upss! Something goes wrong. Possible LinkedIn has asked to logon or banned you. Extraction progress has stored in local storage. Please, refresh the page and resume extraction process from history.');
		}
	};

	self.resume = function(sessionId, userId, parsers){
		var session = history.popById(sessionId);
		if(session){
			if(userId && session.userId && userId !== session.userId){
				if(history){	
					history.pushSession(session).then(function(){
						console.error('Can\'t resume export. Session was saved by another user. (Current user id:' + userId + ' Session user id:' + session.userId + ')');
					});
				}
			} else {
				var members = session.members;
				var historyResults = session.results;
				if(!session.parserId){
					session.parserId = 'base';
				}
				var parser = parsers[session.parserId];

				self.export(members, historyResults, userId, parser);
			}
		} else{
			var msg = 'Please, refresh the page and try it again.';
			Bugsnag.notify('ResumeError', msg);
			console.error(msg);
		}
	};

	module.exports = self;
});