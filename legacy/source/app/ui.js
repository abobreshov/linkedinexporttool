linkedInTool.modules.define('ui', function(module, exports){
	'use strict';
	var self = exports;

	self.viewModel = function(){
      var vm = this;
      var states = {
      	'pause': 'pause',
      	'working': 'working',
      	'closed': 'closed',
      	'init': 'init'
      };


      //-------------------------------------------------- private fields
      var prv = {
      	state: ko.observable(states['closed'])
      };


      //---------------------------------------------- public fields & computed
      vm = {
      	name: ko.observable(''),
      	progressTxt: ko.observable(''),
      	title: ko.observable('Exporting connections')
      };

      vm.isPause = ko.computed(function() { 
      	return this.state() === states['pause'];
      }, prv);

      vm.isWorking = ko.computed(function(){
      	return this.state() === states['working'];
      }, prv);

      vm.isVisible = ko.computed(function(){
      	return this.state() != states['closed'];
      }, prv);

      vm.isControl = ko.computed(function(){
      	return this.state() != states['init'];
      }, prv);

      //-------------------------------------------------- methods
      
      vm.setState = function(s){
      	var tmp = states[s];
      	if(tmp){
      		prv.state(tmp);
      	}
      };

      vm.getState = function(){
      	return prv.state();
      };

      vm.updateProgress = function(currentVal, maxVal, description, currentStateInfo){
		if(!description){
			description = '';
		}
		if(!currentStateInfo){
			currentStateInfo = '';
		}
		var progressStr = '';
		if(currentVal && maxVal){
			progressStr = currentVal + ' / ' + maxVal;
		}

		vm.name(currentStateInfo);
		vm.progressTxt(progressStr);
      };

      vm.doSave = function(){
      	//todo
      };

      vm.doClose = function(){
      	vm.setState(states['closed']);
      	//todo
      };

      vm.doPause = function(){
      	vm.setState(states['pause']);
      };

      vm.doGoTo = function(){

      };

      vm.doStart = function(){
      	vm.setState(states['working']);
      };

      vm.reset = function(){
      	vm.name('');
      	vm.progressTxt('');
      }

      return vm;
	}();

	self.init = function(){
		createProgressBox();
	};

	var createProgressBox = function(){
		var template = `
		<div id="extProgressBox" class="ext_ProgressBox" data-bind="visible: isVisible">
			<p data-bind="text: title">Test</p>
			<div class="ext_ExportBox ext_info">
				<span data-bind="text: name"></span>
				<br>
				<span data-bind="text: progressTxt"></span>
				<div data-bind="visible: isControl">
					<div class="ext_row">
						<button class="ext_ActionButton" id="exp_conn_start_btn" data-bind="enable: isPause, click: doStart">Start</button>
						<button disabled="" class="ext_ActionButton" id="exp_conn_pause_btn" data-bind="enable: isWorking, click: doPause">Pause</button>				
					</div>
					<div data-bind="visible: isPause">
						<div class="ext_row">
							<input type="number" class="ext_NumberInput" name="quantity" /*min="1" max="5"*/>
							<button class="ext_ActionButton" id="exp_conn_goto_btn" data-bind="click: doGoTo">GoTo</button>		
						</div>
						<div class="ext_row">
							<button class="ext_ActionButton" id="exp_conn_save_btn" data-bind="click: doSave">Save</button>
							<button class="ext_ActionButton" id="exp_conn_close_btn" data-bind="click: doClose">Close</button>		
						</div>	
					</div>				
				</div>
			</div>
		`;		
		$(template).appendTo('body');
		
		ko.applyBindings(self.viewModel, document.getElementById('extProgressBox'));
	};

	module.exports = self;
});