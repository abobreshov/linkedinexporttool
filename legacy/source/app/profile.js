linkedInTool.modules.define('baseProfile', function(module, exports){
	'use strict';
	var self = exports;
	self = function() {		
		return {
			'href': null, 
			'name':  null,
			'title': null, 
			'company': null, 
			'location': null,
			'distance': null, 
			'industry': null,
			'id': null
		}; 
	};

	module.exports = self;
});

linkedInTool.modules.define('mainProfile', function(module, exports){
	'use strict';
	var that = exports,
		helper = linkedInTool.modules.require('helper');
	
	var titles = {
		'fullname': 'Contact_Name',
		//'birthday': 'Birthday',
		'email': 'Email',
		'phone': 'Phone',
		'location': 'Location',
		'title': 'Title',
		'industry': 'Industry',
		'companyUrl':  'Company_Link',
		'company': 'Company',
		'distance': 'Distance',		
		'profile_url': 'Profile',
		'site': 'Website',
		'im': 'IM',
		'id': 'ID'
	};

	function Profile() {
		this.fullname = null;
		this.birthday = null;
		this.location = null;
		this.title = null;
		this.company = null;
		this.industry = null;
		this.companyUrl = null;
		this.distance = null;
		this.profile_url = null;
		this.email = null;
		this.phone = null;
		this.im = null;
		this.site = null;
		this.id = null;
	}

	Profile.prototype.fillEmptyFields = function(){
		var self = this;
		for(var prop in self){
			if(self.hasOwnProperty(prop) && typeof self[prop] !== 'function' && !self[prop]){
				self[prop] = '-';
			}
		}
	};

	Profile.prototype.getProfileFields = function(){
		return titles;
	};
	
	
	Profile.prototype.toVCard = function(){
		var self = this;
		var result = 'BEGIN:VCARD\nVERSION:4.0\n';
		result = result.concat('FN:', self.fullname, '\n');
		result = result.concat('TITLE:', self.title || '', '\n');
		result =result.concat('ORG:', self.company || '', '\n');
		if(self.email){
			result =result.concat('EMAIL;TYPE=work:', self.email, '\n');
		}
		if(self.profile_url){
			result =result.concat('URL:', self.profile_url, '\n');
		}
		//todo: add logic for telephone
		//if(self.phone && self.phone.indexOf('work:' > 0)){
		//	result.concat('TEL;VALUE=uri;TYPE=work:tel:', self.phone, '\n');		
		//}
		result = result.concat('END:VCARD');
		return result;
	};

	Profile.prototype.toCSV = function(separator){
		var self = this;
		var result = '';
		for(var property in titles){
			if(self.hasOwnProperty(property) && typeof self[property] !== 'function'){
				var v = self[property];
				var str = '';
				if(typeof v === 'string'){
					str = v;
				} else if(v === parseInt(v)){
					str = (v).toString();
				} else{
					console.error('can not cast to string value:' + v);
				}

				str = helper.cleanHtml(str.replace(/,|;/g, " "));

				result = result.concat('"',str,'"',separator);
			}
		}
		return result;
	};

	Profile.prototype.init = function(o){
		var self = this;
		self.fullname = o.fullname;
		self.birthday = o.birthday;
		self.location = o.location;
		self.title = o.title;
		self.company = o.company;
		self.industry = o.industry;
		self.companyUrl = o.companyUrl;
		self.distance = o.distance;
		self.profile_url = o.profile_url;
		self.email = o.email;
		self.phone = o.phone;
		self.im = o.im;
		self.site = o.site;
		self.id = o.id;
	};

	Profile.prototype.toXML = function(){
		var self = this;
		var exportHelper = linkedInTool.modules.require('exportHelper');
		var result = '    <contact>\n';
		for(var property in titles){
			if(self.hasOwnProperty(property) && typeof self[property] !== 'function')
			{
				var v = self[property];
				v = helper.cleanIfStrValue(v);
				var t = titles[property];					
				var elem = exportHelper.makeXMLElement(t, v);
				result = result.concat(
					'      ',
					elem,
					'\n'
				);
			}
		}

		result = result.concat('    </contact>\n');
		return result;		
	};

	Profile.prototype.merge = function(contacts){
		var self = this;
		if(contacts instanceof Array){
			for (var i = contacts.length - 1; i >= 0; i--) {
				var contact = contacts[i];				
				mergeObject(self, contact);
			}			
		} else {
			mergeObject(self, contacts);
		}

		self.fillEmptyFields();
	};

	var mergeObject = function(obj, contact){
		for(var prop in contact){
			if(obj.hasOwnProperty(prop) && contact.hasOwnProperty(prop) && typeof contact[prop] !== 'function'){
				if(!obj[prop] || obj[prop] === ''){
					if(contact[prop] && contact[prop] !== ''){
						obj[prop] = contact[prop];
					}
				}
			}
		}
	};

	that = function(baseProfile) {
		var self = new Profile();

		if(baseProfile){
			self.fullname = baseProfile.name;
			self.location = baseProfile.location;
			self.title = baseProfile.title;
			self.company = baseProfile.company;
			self.industry = baseProfile.industry;
			self.href = baseProfile.href;
			self.id = baseProfile.id;
		}
		
		return self;		
	};

	module.exports = that;
});