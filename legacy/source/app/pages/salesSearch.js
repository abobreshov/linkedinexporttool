linkedInTool.processSalesSearchPage = (function(){
	'use strict';

	var settings = linkedInTool.modules.require('settings'),
		baseProfile = linkedInTool.modules.require('baseProfile'),
		helper = linkedInTool.modules.require('helper'),
		ui = linkedInTool.modules.require('ui'),
		exporter = linkedInTool.modules.require('exporter'),
		parser = linkedInTool.modules.require('parser'),
		accessLevel = 0;
		
	chrome.runtime.onMessage.addListener(function(message) {
		accessLevel = message.license;

		if (message.action === 'doExport'){
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			exportConnections(true);
		}	
		
		if(message.action === 'resumeDownload' && message.data){
			resumeExportConnections(message.data);
		}
		
		if(message.action === 'updateInfo' && message.data){			
			settings.update(message.data);
		}
	});

	var getMembers = function() {
		var offset = 0;
		var searchCount = 10;
		var members = [];
		var defer = jQuery.Deferred();

		var getMembersFromPage = function (htmlText, members){
			var list = $(htmlText).find('#people-results .search-results li.entity .entity-content');
			for(var i=0; i < list.length; i++ ){
				var c = list[i];
				var p = baseProfile();
				p.href = $(c).find('h3.name a').attr('href');
				p.name = helper.cleanStrValue($(c).find('h3.name a').text());
				p.title = $(c).find('p.headline').text();
				p.industry = $(c).find('.demographic dd.industry').not('.separator').text();
				p.location = $(c).find('.demographic dd.location').not('.separator').text();
				
				var distance = $(c).find('.degree-icon').first();
				if(distance && distance[0]){
					var digit = parseInt(distance[0].childNodes[0]);
					if(!isNaN(digit)){
						p.distance = digit;
					}
				}

				var id = helper.getParameterByName('id', p.href);
				if(id){
					p.id = id;
				}

				members.push(p);
			}
		};

		var fillMembers = function(array, members){
			for(var i = 0; i < array.length; i++){
				var member = array[i];
				if(member){
					var p = baseProfile();
					if(member.profileUrl){
						p.href = member.profileUrl.replace('http://', 'https://');
					} else {
						Bugsnag.notify('FillMembersError', 'Can\'t find profile url', { member: member} );
						console.error(member);
						continue;
					}

					if(member.formattedName){
						p.name = helper.cleanStrValue(member.formattedName);
					}

					if(member.headline){
						p.title = member.headline;
					}

					if(member.location){
						p.location = member.location;
					}

					if(member.industry){
						p.industry = member.industry;
					}

					if(member.badges.distance && member.badges.distance > 0){
						p.distance = member.badges.distance;
					}
					
					p.id = member.memberId;

					members.push(p);				
				}
			}
		};

		if(location.pathname[location.pathname.length - 1] !== '/'){
			location.pathname = location.pathname.concat('/');
		} 
		var current_url = ''.concat(location.origin, location.pathname, 'results', location.search); 

		var info_url = $('div.pagination-container a').attr('href');
		if(!info_url){
			var html = document.getElementsByTagName("body")[0].innerHTML;
			try{
				getMembersFromPage(html,members);
			} catch(e){
				Bugsnag.notifyException(e, "GetMembersFromPageError");
			}
			
			defer.resolve(members);
		} else {
			var url = helper.setParameterByName(current_url, 'count', searchCount);
			ui.createProgressBox();

			(function next_page(){	
				var doRequest = true;			
				url = helper.setParameterByName(url, 'start', offset);
				
				helper.getRequest(url)
					.delay(settings.delay)
					.subscribe(
						function(resp){
							if(resp.result && resp.result.searchResults && resp.pagination){
								var search = resp.result.searchResults;

								if(search.length > 0){
									ui.updateCurrentProgress(null, null, null, 'Get links from page: ' + offset);
									
									try{
										fillMembers(search, members);
									} catch (e){
										Bugsnag.notifyException(e, "FillMembersError");
									}						
								}

								if(offset < resp.pagination.total){
									offset+= searchCount;
								} else {
									doRequest = false;
								}
							} else {
								doRequest = false;
							}
						},
						function(err){
							Bugsnag.notify('SalesSearchMembersError', 'Error on members GET request', {response: err});
							console.error(err);
							ui.removeProgressBox();
							alert('Can\'t get members. Please refresh the page and try it again');					
						},
						function(){
							if(doRequest){
								next_page();
							} else{
								ui.removeProgressBox();
								defer.resolve(members);
							}
						});

			})();
		}
		return defer.promise();
	};


	var resumeExportConnections = function(sessionId){
		exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser);
	};

	var exportConnections = function (isFastTrack){			
        if(accessLevel === 0){
			alert('To enable export contacts of contact, please, buy subscription for LinkedIn Export Tool.');
			return;
        }


		var promise;		
		
		try{
			promise = getMembers();		
		} catch(e){
			Bugsnag.notifyException(e, "GetMembersPeopleSearchError");
		}			
		
		promise.done(function(members){
			if (!members.length) {
				console.error('Please, specify target contacts to process.');
				return;
			}
						
			if(isFastTrack){
				exporter.fastExport(members);
			} else {
				var accId = linkedInTool.account.getAccountId(); 
				exporter.export(members, null, accId, parser.sales);
			}
		});
	};

})();