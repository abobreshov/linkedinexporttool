linkedInTool.processSearchPage = (function(){
	'use strict';

	var settings = linkedInTool.modules.require('settings'),
		baseProfile = linkedInTool.modules.require('baseProfile'),
		helper = linkedInTool.modules.require('helper'),
		ui = linkedInTool.modules.require('ui'),
		exporter = linkedInTool.modules.require('exporter'),
		parser = linkedInTool.modules.require('parser'),
		accessLevel = 0;
		
	chrome.runtime.onMessage.addListener(function(message) {
		accessLevel = message.license;

		if (message.action === 'doExport'){
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			exportConnections(true);
		}	
		
		if(message.action === 'resumeDownload' && message.data){
			resumeExportConnections(message.data);
		}
		
		if(message.action === 'updateInfo' && message.data){			
			settings.update(message.data);
		}
	});

	var getMembers = function(isFastTrack) {
		var offset = 1;
		var members = [];
		var defer = jQuery.Deferred();

		var getMembersFromPage = function (htmlText, members){
			var list = $(htmlText).find('#results-container .search-results li.people .bd');
			for(var i=0; i < list.length; i++ ){
				var c = list[i];
				var p = baseProfile();
				p.href = $(c).find('a.title').attr('href');
				p.name = helper.cleanStrValue($(c).find('a.title').text());
				p.title = $(c).find('div.description').text();
				p.industry = $(c).find('.demographic dd').not('.separator').text();
				p.location = $(c).find('.demographic bdi').not('.separator').text();
				
				var distance = $(c).find('.degree-icon').first();
				if(distance && distance[0]){
					var digit = parseInt(distance[0].childNodes[0]);
					if(!isNaN(digit)){
						p.distance = digit;
					}
				}

				var id = helper.getParameterByName('id', p.href);
				if(id){
					p.id = id;
				}

				members.push(p);
			}
		};

		var fillMembers = function(array, members){
			for(var i = 0; i < array.length; i++){
				var member = array[i].person;
				if(member){
					var p = baseProfile();
					if(member.link_nprofile_view_3){
						p.href = member.link_nprofile_view_3;
					} else if(member.link_nprofile_view_headless){
						p.href  = member.link_nprofile_view_headless;
					} else {
						console.error(member);
						continue;
					}

					if(member.fmt_name){
						p.name = helper.cleanStrValue(member.fmt_name);
					}

					if(member.fmt_location){
						p.location = member.fmt_location;
					}

					if(member.fmt_industry){
						p.industry = member.fmt_industry;
					}

					if(isFastTrack && member.snippets && member.snippets.length > 0 && member.snippets[0].bodyList && member.snippets[0].bodyList.length > 0){
						var cmp = member.snippets[0].bodyList[0].split(' at ');
						if(cmp.length > 1){
							p.company = cmp[1];
						}						
					}

					if(member.fmt_headline){
						p.title = member.fmt_headline;
					}

					p.distance = member.distance;
					p.id = member.id;

					members.push(p);				
				}
			}
		};

		var current_urlpath = document.location.pathname;
		var info_url = $('#results-pagination a').attr('href');
		if(!info_url){
			var html = document.getElementsByTagName("body")[0].innerHTML;
			try{
				getMembersFromPage(html,members);
			} catch(e){
				Bugsnag.notifyException(e, "GetMembersFromPageError");
			}
			
			defer.resolve(members);
		} else {
			var url = info_url.replace(current_urlpath, current_urlpath + 'j');
			ui.createProgressBox();

			(function next_page(){			
				var doRequest = true;

				url = helper.setParameterByName(url, 'page_num', offset);

				helper.getRequest(url)
					.delay(settings.delay)
					.subscribe(
						function(resp){
							if(resp.content && resp.content.page && resp.content.page.voltron_unified_search_json && resp.content.page.voltron_unified_search_json.search && resp.content.page.voltron_unified_search_json.search.baseData.resultPagination){
								var search = resp.content.page.voltron_unified_search_json.search;
								if(search.baseData.resultPagination.nextPage &&
									offset > search.baseData.resultPagination.nextPage.pageNum){
									doRequest = false;
								}

								if(search.results && search.results.length > 0){
									ui.updateCurrentProgress(null, null, null, 'Get links from page: ' + offset);
									
									try{
										fillMembers(search.results, members);
									} catch (e){
										Bugsnag.notifyException(e, "FillMembersError");
									}
									
									offset++;							
								} else{
									doRequest = false;
								}
							} else {
								doRequest = false;
							}								
						},
						function(err){
							Bugsnag.notify('PeopleSearchMembersError', 'Error on members GET request', {response: err});
							console.error(err);	
							ui.removeProgressBox();
							alert('Can\'t get members. Please refresh the page and try it again');					
						},
						function(){
							if(doRequest){
								next_page();
							} else {
								ui.removeProgressBox();
								defer.resolve(members);								
							}						
						});
			})();
		}
		
		return defer.promise();
	};


	var resumeExportConnections = function(sessionId){
		exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser);
	};

	var exportConnections = function (isFastTrack){			
        if(accessLevel === 0){
			alert('To enable export contacts of contact, please, buy subscription for LinkedIn Export Tool.');
			return;
        }


		var promise;		
		
		try{
		 promise = getMembers(isFastTrack);		
		} catch(e){
			Bugsnag.notifyException(e, "GetMembersPeopleSearchError");
		}			
		
		promise.done(function(members){
			if (!members.length) {
				console.error('Please, specify target contacts to process.');
				return;
			}
						
			if(isFastTrack){
				exporter.fastExport(members);
			} else {
				var accId = linkedInTool.account.getAccountId(); 
				exporter.export(members, null, accId, parser.base);
			}
		});
	};

})();