linkedInTool.processContactsPage = (function(){
	'use strict';	

	var membersLink = 'https://www.linkedin.com/contacts/api/contacts/{{action}}/?start={{0}}&count={{1}}&fields=id%2Cname%2Cfirst_name%2Clast_name%2Ccompany%2Ctitle%2Cgeo_location%2Ctags%2Cemails%2Csources%2Cdisplay_sources%2Clast_interaction%2Csecure_profile_image_url&{{sort}}',
		contactUrl = '/contacts/view?id={{0}}&trk=contacts-contacts-list-contact_name-0';

	
	var filter = {
		queries: [
			'tag',
			'location',
			'company',
			'title',
			'source',
			'searchQuery',
			'letter'
		],
		mapper: {
			'tag': {name: 'tag_id', action: 'full'},
			'location': {name: 'location', action: 'full'},
			'company': {name: 'company_id', action: 'full'},
			'title': {name: 'title', action: 'full'},
			'source': {name: 'source', action: 'full'},
			'searchQuery': {name: '{{queryType}}', action: 'search'},
			'letter': {name: 'name', action: 'search', additional: 'starts_with=true'}	
		}
	};

	var order = {
		list: ['sortOrder',	'filter'],
		mapper: {
			'new': '-created',
			'recent': '-last_interaction',
			'first_name': 'name',
			'last_name': 'last_name'
		}
	};

	var settings = linkedInTool.modules.require('settings'),
		exporter = linkedInTool.modules.require('exporter'),
		parser = linkedInTool.modules.require('parser'),
		ui = linkedInTool.modules.require('ui');

	chrome.runtime.onMessage.addListener(function(message/*, sender, sendResponse*/) {
		debugger;
		console.log(message);
		settings.update(message.settings);

		if(message.action === 'test'){
			testPopupShow();
		}

		if (message.action === 'doExport'){
			// working on - make proper window
			// todo message.license
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			// working on - disable save&jump
			exportConnections(true);
		}	
		
		if(message.action === 'resumeExport' && message.data){
			//todo
			resumeExportConnections(message.data, false);
		}
		
		if(message.action === 'resumeExportFrom' && message.data){
			//todo
			resumeExportConnections(message.data, true);
		}
	});
	
	var getMembers = function () {
		var members = [],
			offset = 0,
			contactsCount = 25,
			link = membersLink,
			defer = jQuery.Deferred();
		
		var helper = linkedInTool.modules.require('helper'),
			//ui = linkedInTool.modules.require('ui'),
			baseProfile = linkedInTool.modules.require('baseProfile');
		
		//isAllChecked() detects if Select All checkbox checked on contacts page
		var isAllChecked = (function(){ 
			return $('#select-all-checkbox-container').has(':checked').length > 0;
		})();

		//define sort order for contacts on the page
		var detectSortOrder = function(url, linkPattern) {
			var sq =  _.find(order.list, 
						function(prop){
							return !_.isEmpty( helper.getParameterByName(prop, url) );
						});
			
			if(sq){
				var sortParam = helper.getParameterByName(sq, url) || 'recent';
				return linkPattern.replace('{{sort}}', 'sort=' + order.mapper[sortParam]);
			} else {
				return linkPattern.replace('{{sort}}', 'sort=-last_interaction');
			}
		};

		//define filer for contacts on the page
		var detectFilterQuery = function(url, linkPattern){
			var fq = _.find(filter.queries, 
				function(prop){
					return !_.isEmpty( helper.getParameterByName(prop, url) );
				});

			if(fq){				
				var value = helper.getParameterByName(fq, url);
				var mappedQuery = filter.mapper[fq];
				var query = '&'.concat(mappedQuery.name, '=', encodeURIComponent(value));
				if(mappedQuery.additional){
					query = query.concat('&', mappedQuery.additional);
				}
				if(fq === 'searchQuery'){
					query = query.replace('{{queryType}}', helper.getParameterByName('searchQueryType', url));
				}
				
				return linkPattern.replace('{{action}}', mappedQuery.action).concat(query);
			} else {
				return linkPattern.replace('{{action}}', 'more');
			}
		};

		var getContact = function(c){
			var profile = baseProfile();
			profile.company = '';
			if(c.company){
				profile.company = c.company.name;
			}
			profile.location = '';
			if(c.geo_location){
				profile.location = c.geo_location.name;
			}

			profile.href = contactUrl.replace('{{0}}', c.id);
			profile.id = c.id;
			profile.name = c.name;
			profile.title = c.title;
			
			return profile;
		};		

		if(isAllChecked){
			var currentUrl = window.location.href;

			//set sorting to member url patern
			link = detectSortOrder(currentUrl, link);

			//set filtering to member url pattern
			link = detectFilterQuery(currentUrl, link);

			ui.viewModel.setState('init');
			//ui.createProgressBox();

			(function next_page(){
				var doRequest = true;
				var url = link.replace('{{0}}', offset).replace('{{1}}', contactsCount);
				debugger;
				helper.getRequest(url)
					.delay(settings.data.delay)
					.subscribe(
						function(resp){
							debugger;
							if(resp.status === 'success' && resp.contacts && resp.contacts.length > 0){
								for(var i = 0; i < resp.contacts.length; i++){
									var contact = getContact(resp.contacts[i]);
									ui.viewModel.updateProgress(i + offset + 1, resp.paging.total, 'Get contacts links: ', '');
									members.push(contact);
								}

								if(resp.paging && resp.paging.total > (resp.paging.count + resp.paging.start)){
									offset += contactsCount;
								} else {
									doRequest = false;						
								}
							} else {
								doRequest = false;
								Bugsnag.notify('ContactsMembersError', 'Contacts parsing problem.', {response: resp});
								console.error('Can\'t get list of contacts to export. Seems like something went wrong');
							}
						},
						function(err){
							Bugsnag.notify('ContactsMembersError', 'Error on members GET request', {response: err});
							console.error(err);	
							ui.setState('closed');
							alert('Can\'t get members. Please refresh the page and try it again');	
						},
						function(){
							if(doRequest){
								next_page();
							} else {
								ui.setState('closed');
								defer.resolve(members);								
							}
						});
			})();
		} else {
			defer.resolve(getSelectedMembers());
		}		

		return defer.promise();
	};


	var getSelectedMembers = function () {
		var baseProfile = linkedInTool.modules.require('baseProfile');
		var helper = linkedInTool.modules.require('helper');
		return $.map(
			$('.contact-item-view:has(input:checked)'),
			function (elem) {
				var profile = baseProfile();

				var v = $(elem);
				var link = v.find('div.body .name a');
				profile.href = link.attr('href');

				profile.name = link.text();
				profile.title = v.find('div.body a.title').text();
				profile.company = v.find('div.body a.company').text();
				profile.location = v.find('div.body a.location').text();
				var id = helper.getParameterByName('id', profile.href);
				if(!id){
					id = '';
				}
				profile.id = id;

				return profile; 
			}
		);
	};

	
	var resumeExportConnections = function(sessionId){
		exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser);
	};

	var exportConnections = function (isFastTrack){
		console.info(settings);
		debugger;
		var accId = linkedInTool.account.getAccountId(); 
		
		var promise;		
		
		try{
			promise = getMembers();		
		} catch(e){
			Bugsnag.notifyException(e, "GetMembersContactsError");
		}	

		promise.done(function(members){
			if (!members.length) {
				console.error('Please, specify target contacts to process.');
				return;
			}
			debugger;

			if(isFastTrack){
				exporter.fastExport(members);
			} else {
				exporter.export(members, null, accId, parser.contacts);
			}
		});
	};


	ui.init();

	//ui.viewModel.setState('pause');

})();
