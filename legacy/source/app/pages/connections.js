linkedInTool.processConnectionsPage = (function(){
	'use strict';	
	var contact_url = 'https://www.linkedin.com/profile/view?id={{0}}';

	var settings = linkedInTool.modules.require('settings'),
		exporter = linkedInTool.modules.require('exporter'),
		parser = linkedInTool.modules.require('parser');

	chrome.runtime.onMessage.addListener(function(message/*, sender, sendResponse*/) {

		if (message.action === 'doExport'){
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			exportConnections(true);
		}	
		
		if(message.action === 'resumeDownload' && message.data){
			resumeExportConnections(message.data);
		}
		
		if(message.action === 'updateInfo' && message.data){			
			settings.update(message.data);
		}
	});
	

	var getMembers = function() {
		var baseProfile = linkedInTool.modules.require('baseProfile');

		var getContact = function(elem){
			var v = $(elem);
			var h = contact_url.replace('{{0}}',elem.id);
			var name = v.find('span.conn-name').text();				
			var title = v.find('span.conn-headline').text();
			var company = v.find('span.company-name').text();

			var profile = baseProfile();
						
			profile.href = h;
			profile.name = name;
			profile.title = title;
			profile.company = company;
			profile.id = elem.id;
		
			return profile;
		};

		return $.map(
			$('#list-panel li:has(input:checked)'),
			function (elem) { 
				return getContact(elem); 
			}
		);
	};

	var resumeExportConnections = function(sessionId){
		exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser);
	};

	var exportConnections = function (isFastTrack){					
		var members;
		
		try{
			members = getMembers();
		} catch(e){
			Bugsnag.notifyException(e, "GetMembersConnectionsError");
		}		

		if (!members.length) {
			console.error('Please, specify target contacts to process.');
			return;
		}
					
		if(isFastTrack){
			exporter.fastExport(members);
		} else {
			var accId = linkedInTool.account.getAccountId(); 
			exporter.export(members, null, accId, parser.contacts);
		}

	};

})();
