linkedInTool.processContactContactsPage = (function(){
	'use strict';	
	var info_url = 'https://www.linkedin.com/profile/profile-v2-connections?id={{id}}&offset={{offset}}&count=10&distance=1&type=ALL';

	var settings = linkedInTool.modules.require('settings'),
		exporter = linkedInTool.modules.require('exporter'),
		parser = linkedInTool.modules.require('parser'),
		helper = linkedInTool.modules.require('helper'),
		accessLevel = 0;

	chrome.runtime.onMessage.addListener(function(message/*, sender, sendResponse*/) {
		accessLevel = message.license;

		if (message.action === 'doExport'){
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			exportConnections(true);
		}	
		
		if(message.action === 'resumeDownload' && message.data){
			resumeExportConnections(message.data);
		}
		
		if(message.action === 'updateInfo' && message.data){			
			settings.update(message.data);
		}
	});
	
	var getMembers = function () {
		var members = [],
			offset = 0,
			id = linkedInTool.account.getCurrentId(),
			defer = jQuery.Deferred();
		
		var ui = linkedInTool.modules.require('ui'),
			baseProfile = linkedInTool.modules.require('baseProfile');
		

		var getContact = function(c){
			var profile = baseProfile();
			profile.company = '';
			var tmpStrArr = c.headline.split(' at ');
			if(tmpStrArr.length > 1){
				profile.company = tmpStrArr[1];
			}
						
			profile.href = c.pview;
			profile.name = c.fmt__full_name;
			profile.title = c.headline;
			profile.distance = c.distance; //need to be parseInt(, 10) ?
			profile.id = c.memberID;
		
			return profile;
		};		
		ui.createProgressBox();

		(function next_page(){
			var doRequest = true;
			var url = info_url.replace('{{id}}', id).replace('{{offset}}', offset);

			helper.getRequest(url)
				.delay(settings.delay)
				.subscribe(
					function(resp){
						if(resp.content.connections && resp.content.connections.connections && resp.content.connections.connections.length > 0){
							var connections = resp.content.connections.connections;
							for(var i = 0; i < connections.length; i++){
								var c = connections[i];
								var p = getContact(c);
								members.push(p);
							}
							ui.updateCurrentProgress(offset, resp.content.connections.numAll, 'Get contacts of contact:', '');
							offset += 10;
						} else {
							doRequest = false;
						}
					},
					function(err){
						Bugsnag.notify('2ndContatactsMembesRequestError', 'Members request fail', {response: err});
						console.error(err);	
						ui.removeProgressBox();
						alert('Can\'t get members. Please refresh the page and try it again');	
					},
					function(){
						if(doRequest){
							next_page();
						} else {
							ui.removeProgressBox();
							defer.resolve(members);								
						}
					});
		})();		

		return defer.promise();
	};


	
	var resumeExportConnections = function(sessionId){
		exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser);
	};

	var exportConnections = function (isFastTrack){
        if(accessLevel === 0){
			alert('To enable export contacts of contact, please, buy subscription for LinkedIn Export Tool.');
			return;
        }
		
		var accId = linkedInTool.account.getAccountId(); 
		
		var promise;
		
		try{
			promise = getMembers();		
		} catch(e){
			Bugsnag.notifyException(e, "GetMembers2ndConnectionsError");
		}
		
		promise.done(function(members){
			if (!members.length) {
				console.error('Please, specify target contacts to process.');
				return;
			}
						
			if(isFastTrack){
				exporter.fastExport(members);
			} else {
				exporter.export(members, null, accId, parser.base);
			}
		});
	};

})();