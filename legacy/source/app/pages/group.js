linkedInTool.processGroupMembersPage = (function(){
	'use strict';

	var settings = linkedInTool.modules.require('settings'),
		baseProfile = linkedInTool.modules.require('baseProfile'),
		helper = linkedInTool.modules.require('helper'),
		ui = linkedInTool.modules.require('ui'),
		exporter = linkedInTool.modules.require('exporter'),
		parser = linkedInTool.modules.require('parser'),
		accessLevel = 0;
		
	chrome.runtime.onMessage.addListener(function(message) {
		accessLevel = message.license;

		if (message.action === 'doExport'){
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			exportConnections(true);
		}	
		
		if(message.action === 'resumeDownload' && message.data){
			resumeExportConnections(message.data);
		}
		
		if(message.action === 'updateInfo' && message.data){			
			settings.update(message.data);
		}
	});

	var getMembers = function() {
		var offset = 1;
		var members = [];
		var defer = jQuery.Deferred();

		var getMembersFromPage = function (htmlText, members){
			var list = $(htmlText).find('div.content');
			for(var i=0; i < list.length; i++ ){
				var element = $(list[i]);
				var p = baseProfile();
				p.href = element.find('h4 a').attr('href');
				p.name = element.find('h4 a').text();
				
				var distance = element.find('.degree-icon').first();
				if(distance && distance[0]){
					var digit = parseInt(distance[0].childNodes[0]);
					if(!isNaN(digit)){
						p.distance = digit;
					}
				}

				var headline = element.find('.member-headline').text().split(',');
				if(headline.length > 0){
					p.title = headline[0];
					p.location = '';
					if(headline.length > 1){
						for(var j = 1; j < headline.length; j++){
							p.location += headline[j] + ' ';
						}
					}
				}

				var id = helper.getParameterByName('id', p.href);
				if(id){
					p.id = id;
				}

				members.push(p);
			}
		};

		var info_url = $('.paginate a').attr('href');
		if(!info_url){
			var html = document.getElementsByTagName("body")[0].innerHTML;
			getMembersFromPage(html,members);
			defer.resolve(members);
		} else {
			ui.createProgressBox();

			(function next_page(){
				var doRequest = true;				
				var url = helper.setParameterByName(info_url, 'split_page', offset);
				
				helper.getRequest(url)
					.delay(settings.delay)
					.subscribe(
						function(resp){
						var htmlElement = $(resp);
							//[ab] todo check condition
							if(offset < htmlElement.find('.paginate a').length){ 
								ui.updateCurrentProgress(null, null, null, 'Get links from page: ' + offset);
								getMembersFromPage(resp, members);
								offset++;											
							} else {
								doRequest = false;
							}	
						},
						function(err){
							Bugsnag.notify('GroupMembersError', 'Error on members GET request', {response: err});
							console.error(err);	
							ui.removeProgressBox();
							alert('Can\'t get members. Please refresh the page and try it again');	
						},
						function(){
							if(doRequest){
								next_page();
							} else {
								ui.removeProgressBox();
								defer.resolve(members);								
							}
						});
			})();
		}
		return defer.promise();
	};


	var resumeExportConnections = function(sessionId){
		exporter.resume(sessionId, linkedInTool.account.getAccountId(), parser);
	};

	var exportConnections = function (isFastTrack){			
        if(accessLevel === 0){
			alert('To enable export contacts of contact, please, buy subscription for LinkedIn Export Tool.');
			return;
        }

		var promise;		
		
		try{
			promise = getMembers();		
		} catch(e){
			Bugsnag.notifyException(e, "GetMembersGroupError");
		}		
		
		promise.done(function(members){
			if (!members.length) {
				console.error('Please, specify target contacts to process.');
				return;
			}
						
			if(isFastTrack){
				exporter.fastExport(members);
			} else {
				var accId = linkedInTool.account.getAccountId(); 
				exporter.export(members, null, accId, parser.base);
			}
		});
	};

})();