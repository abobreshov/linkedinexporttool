linkedInTool.modules.define('parser', function(module, exports){
	'use strict';
	var info_url = 'https://www.linkedin.com/contacts/api/contacts/{{id}}/?fields=id,name,emails,emails_extended,birthday,phone_numbers,sites,addresses,company,title,location,ims,profiles,twitter,wechat,display_sources',
		companyBaseUrl = 'https://www.linkedin.com/company/';
	
	var self = exports,
		parseHelper = linkedInTool.modules.require('parseHelper'),
		mainProfile = linkedInTool.modules.require('mainProfile'),
		helper = linkedInTool.modules.require('helper');

	//todo [ab] make constructor class for it
	self.contacts = {
		id: 'contacts',
		parse: function(data){
			return parseContactsProfile(data);
		},
		requestor: function(url){
			var id = helper.getParameterByName('id', url);			
			var jsonUrl = info_url.replace('{{id}}', id);

			return Rx.Observable.zip(
				helper.getRequest(url), 
				helper.getRequest(jsonUrl),
				function(htmlResp, jsonResp){
					return {
						htmlData: htmlResp,
						jsonData: jsonResp
					};
				});
		}
	};

	self.base = {
		id: 'base',
		parse: function(data){
			return parseBaseProfile(data);
		},
		requestor: function(url){
			return helper.getRequest(url);
		}
	};

	self.sales = {
		id: 'sales',
		parse: function(data){
			return parseSalesProfile(data);
		},
		requestor: function(url){
			return helper.getRequest(url);
		}
	};



	var parseContactJson = function(jsonObj){
		var pr = jsonObj.contact_data,
			p = mainProfile();

		if(pr){

			p.fullname = pr.name;
			p.title = pr.title;
			p.location = pr.location;
			if(pr.company){
				p.company = pr.company.name;
			}

			if(pr.emails_extended && pr.emails_extended.length > 0){
				p.email = pr.emails_extended[0].email;
			}

			var i = 0; //itterator

			var phone = '';
			if(pr.phone_numbers && pr.phone_numbers.length > 0){
				for(i = 0; i < pr.phone_numbers.length; i++){
					phone = phone.concat(pr.phone_numbers[i].type,': ',pr.phone_numbers[i].number, ';');
				}
			}

			if(phone.length > 0){
				p.phone = phone;
			}

			var ims = '';
			if(pr.ims && pr.ims.length > 0){
				for(i = 0; i < pr.ims.length; i++){
					ims = ims.concat(pr.ims[i].type,': ',pr.ims[i].name, ';');
				}
			}

			if(ims.length > 0){
				p.im = ims;
			}

			// "sites": [
			//{"name": "Personal Website", "visible": true, "url": "http://", "blog": false, "source": "LinkedIn", "id": "Personal Website"}, 
			//{"name": "Company Website", "visible": true, "url": "http://", "blog": false, "source": "LinkedIn", "id": "Company Website"}, 
			//{"name": "abobreshov", "visible": true, "url": "http://twitter.com/", "blog": false, "source": "LinkedIn", "id": "abobreshov"}]
			var website = '';
			if(pr.sites && pr.sites.length > 0){
				for(i = 0; i < pr.sites.length; i++){
					website = website.concat(pr.sites[i].url,'   ');
				}
			}
			
			if(website.length > 0){
				p.site = website;
			}
			
			
			//"birthday": "3/8/1991" "birthday": null
			if(pr.birthday){
				p.birthday = pr.birthday;
			}

			//empty fields
			//var profile_url = ''; //data has link but not to the public profile
			//var companyUrl = '';
			//var industry = '';
			//var distance = 0;

			return p;
		} else{
			//todo error
			return null;
		}
	};

	var parseContactHtml = function(htmlText){			
			var profileInfo = [];
			var data = findBaseDataContent(htmlText);
			for(var property in data){
				for(var i = 0; i < data[property].length; i++){
					var info = data[property][i].data;
					var profile = data[property][i].getData(info);
					profileInfo.push(profile);
				}
			}		

			var parsedHtmlProfile = getDataFromHtml(htmlText);
			if(parsedHtmlProfile){
				profileInfo.push(parsedHtmlProfile);
			}

			if(profileInfo.length >= 1){
				return profileInfo;				
			}

			return null;
	};


	var findAllDataContent = function(html){
		var content = [], i = 0, j, s, js; 
		
		i = html.indexOf('<!--{', i + 1);
		j = html.indexOf('}-->', i);
		while(i > 0){
			s = html.substr(i + 4, j - i - 3)
				.replace(/&dsh;/g, '-')
				.replace(/\\u002d([1-9])/g, '$1');	

			try {
				js = JSON.parse(s);
				content.push(js);
			} catch(err) {
				js = {};
				console.log('Could not parse:');
				console.error(s);
			}
			
			i = html.indexOf('<!--{', i + 1);
			j = html.indexOf('}-->', i);
		}

		return content;
	};

	var findBaseDataContent = function(html){
		var data = {content: [], contact_data: []};
		var content = findAllDataContent(html);		

		try {			
			content.map(function(js){
				if(js.content && js.content.BasicInfo && js.content.TopCard && js.content.ContactInfo){
					data.content.push({ data: js.content, getData: getDataFromContent });
				} else if(js.contact_data){
					data.contact_data.push({ data: js.contact_data, getData: getDataFromContact });
				}
			});
		} catch(e) {
			console.log("Can't parse data [findAllDataContent]");
			Bugsnag.notifyException(e, "FindAllDataContentError");
			console.error(e);
		}

		return data;
	};

	var getDataFromContact = function(data){
		var parser = parseHelper.contactData;
		var p = mainProfile(); 
		
		p.fullname = parser.getFullName(data);
		p.title = parser.getTitle(data);
		p.location = parser.getLocation(data);
		p.company = parser.getCompany(data);
		p.email = parser.getEmail(data);
		p.profile_url = parser.getProfileUrl(data);

		return p;	
	};

	var getDataFromContent = function(data){
		var parser = parseHelper.content;	
		var p = mainProfile(); 

		p.fullname = parser.getFullName(data);
		p.title = parser.getTitle(data);
		p.location = parser.getLocation(data);
		p.company = parser.getCompany(data);
		p.email = parser.getEmail(data);
		p.profile_url = parser.getProfileUrl(data);
		p.industry = parser.getIndustry(data);
		p.distance = parser.getDistance(data);
		p.companyUrl = parser.getCompanyUrl(data);
		p.multipleComp = parser.getIsMultipleComp(data);
		
		return p; 
	};

	var getDataFromHtml = function (html){
			var data = $(html),	
				parser = parseHelper.html,
				p = mainProfile();

			p.fullname = parser.getFullName(data);
			p.title = parser.getTitle(data);
			p.location = parser.getLocation(data);
			p.company = parser.getCompany(data);
			p.email = parser.getEmail(data);
			p.profile_url = parser.getProfileUrl(data);
			p.industry = parser.getIndustry(data);
			p.distance = parser.getDistance(data);
			p.companyUrl = parser.getCompanyUrl(data);
			p.multipleComp = '';

			return p; 
	};

	var validateHtmlResponse = function (html){
		if($(html).find('#login').length > 0){
			return false;
		}
		return true;
	};

	var parseContactsProfile = function (data){
		try{
			var html = data.htmlData;//htmlResp[0];
			if(!validateHtmlResponse(html)){
				return null;
			}

			var json = data.jsonData;//jsonResp[0];

			var contacts = parseContactHtml(html);
			if(!contacts){
				contacts = [];
			}

			var jsonContact;
			if(json){
				jsonContact = parseContactJson(json);
			}		
			
			if(jsonContact && contacts && contacts.length >= 0){
				contacts.push(jsonContact);
			}
			
			return contacts;		
			
		} catch(err){
			Bugsnag.notifyException(err, 'ParseContactsProfileError');
			console.error(err);
			return null;
		}
	};

	var parseBaseProfile = function(resp){
		try {
			var html = resp;
			if(!validateHtmlResponse(html)){
				return null;
			}	
			var contacts = parseContactHtml(html);
			if(!contacts){
				contacts = [];
			}

			return contacts;		
		} catch(err){
			Bugsnag.notifyException(err, 'ParseBaseProfileError');
			console.error(err);
			return null;
		}

	};

	var parseSalesProfile = function(html){
		var data = findAllDataContent(html);
		var profileCandidates = [];
		data.filter(function(json){
				return json.profile && json.currentPosition;
			})
			.forEach(function(json){			
				var p = mainProfile();
				try{
					p.fullname = json.profile.fullname;
					p.location = json.profile.location || '';
					p.title = json.profile.headline;
					
					if(json.currentPosition.position){
						p.company = json.currentPosition.position.companyName || '';
						p.companyUrl = companyBaseUrl.concat(json.currentPosition.position.companyId);
					}
					
					p.industry = json.profile.industry;
					p.distance = json.profile.degree;
					p.profile_url =	json.profile.profileLink;		
					p.id = json.profile.memberId;

					//json.profile.firstName
					//json.profile.lastName
					//json.profile.numConnections

					if(json.profile.contactInfo){
						var info = json.profile.contactInfo;
						p.profile_url = info.publicProfileUrl || '';	
						if(info.emails && info.emails.length > 0){
							p.email = info.emails.pop();
						}
						
						if(info.websites && info.websites.length > 0){
							p.site = '';
							info.websites.map(function(website){
								return website.join(': ');
							}).forEach(function(websiteItem){
								p.site = p.site.concat(websiteItem, '  ');
							});
						}
						
						if(info.twitterAccounts && info.twitterAccounts.length > 0){
							var twitter = info.twitterAccounts.pop();
							p.im = 'twitter: '.concat(twitter); 
						}

						if(info.phones && info.phones.length > 0) {
							p.phone =  info.phones.pop();
						}

						if(info.address &&info.address.length > 0){
							//todo 
						}

					}

					profileCandidates.push(p);
				} catch(e){
					Bugsnag.notify('SalesNavContactInfo', 'Error on parsing contact info', {contactInfo: json.profile.contactInfo, error: e});
				}				
				
		});

		return profileCandidates.length > 0? profileCandidates[0]: null;
	};

	module.exports = self;
});