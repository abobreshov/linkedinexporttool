linkedInTool.modules.define('license', function(module, exports){
	var self = exports;

	/*
	LIC_TYPES
	0 - free - no license
	1 - trial period license
	2 - full license
		
	var LIC_TYPES = ["EXPIRED", "FREE_TRIAL", "FULL"];
	*/

	var CWS_LICENSE_API_URL = 'https://www.googleapis.com/chromewebstore/v1.1/userlicenses/',
		TRIAL_PERIOD_DAYS = 1000, //20, 
		interactive = true;
	
	self.licenseData = {
		accessLevel :  null,
		accessDescription : '',
		result : false
	};

	self.license = Rx.Observable.create(function(observer){

		chrome.identity.getAuthToken({ interactive: interactive }, function(token){
			var lic = linkedInTool.modules.require('license');
			if (chrome.runtime.lastError){
				Bugsnag.notifyException(chrome.runtime.lastError, "ChromeRuntimeLicenseError");
				observer.onError(chrome.runtime.lastError);
			}
			var url = CWS_LICENSE_API_URL + chrome.runtime.id;

			$.ajax({
				url: url,
				type: 'GET',
				headers: {
					'Authorization': 'Bearer ' + token
				},
				success: function(data){
					if (this.status === 401) {
						chrome.identity.removeCachedAuthToken({ token: token },function(){
							observer.onError('Auth token has expired');
						});
					} else {
						lic.licenseData = parseLicense(data);
						observer.onNext(lic.licenseData);
						observer.onCompleted();
					}
				},
				error: function(e){
					Bugsnag.notifyException(e, "LicenseError");
					observer.onError(e);
				}
			});
		});

	});

	var parseLicense =	function (l) {
		var result = {
			accessLevel: function(){
				var self = this; 
				if(self.accessDescription === 'FULL' && self.result){
					return 2;
				} else if(self.result && self.accessDescription === "FREE_TRIAL"){
					var daysAgoLicenseIssued = Date.now() - parseInt(self.createdTime, 10);
					daysAgoLicenseIssued = daysAgoLicenseIssued / 1000 / 60 / 60 / 24;	
					if (daysAgoLicenseIssued <= TRIAL_PERIOD_DAYS) {			
						return 1; //FREE_TRIAL
					} else {			
						return 0; //EXPIRED
					}			
				} else {
					return 0; //EXPIRED
				}
			},
			accessDescription: l.accessLevel,
			result: l.result,
			createdTime: l.createdTime
		};

		return result;
	};	

	module.exports = self;
});