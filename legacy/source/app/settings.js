linkedInTool.modules.define('settings', function(module, exports){
	'use strict';
	var self = exports;
	var counter = linkedInTool.modules.require('counter');
	
	self.data = {};

	self.update = function(data){
		var counterStat = data.stat,
		maxContacts = data.maxContacts,
		delay = data.delay;

		self.data.maxContacts = maxContacts;
		self.data.delay = delay;
		counter.set(counterStat.dayCounter, counterStat.contactsCounter, counterStat.lastDate);
	};
	
	self.settings = Rx.Observable.create(function(observer){
		var isActive = true;
		chrome.storage.sync.get(['fileFormat', 'stat', 'maxContacts', 'delay'],
			function(data){
				//var s = linkedInTool.modules.require('settings');
				var s = {};
				if(!isActive){
					return;
				}

				if(!data){
					observer.onError('Can\'t get settings from sync storage');
					return;
				}

				if(data.fileFormat){
					s.fileFormat = data.fileFormat;
				} else {
					s.fileFormat = {type: 'XML', settings: {separator: ';'}, hasAdditionalSettings: false };
				}
		
				if(data.stat) {
					if(data.stat.lastDate && typeof data.stat.lastDate === 'number'){
						counter.parseJson(data);						
					}					
				}

				s.counter = counter;
				s.stat = s.counter.get();

				if(data.maxContacts){
					s.maxContacts = data.maxContacts;
				} else {
					s.maxContacts = linkedInTool.licenseRestriction.maxContacts;
				}

				if(data.delay){
					s.delay = data.delay;
				} else{
					s.delay = linkedInTool.delay;
				}

				self.data = s;
				observer.onNext(s);
				observer.onCompleted();
		});

		return function(){
			isActive = false;
		};		
	});

	self.history = Rx.Observable.create(function(observer){
		var isActive = true;

		chrome.storage.local.get('history',
			function(data){
				if(!isActive){
					return;
				}

				if(!data){
					observer.onError('Can\'t get history from local storage');
				} else if (!data.history){
					data.history = [];
				}

				observer.onNext(data.history);
				observer.onCompleted();
			}
		);
		
		return function(){
			isActive = false;
		};
	});

	module.exports = self;
});