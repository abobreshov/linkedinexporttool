linkedInTool.modules.define('helper', function(module, exports){
	'use strict';
	var self = exports,
		specialChars = {
            '&amp;'     :   '&',
            '&gt;'      :   '>',
            '&lt;'      :   '<',
            '&quot;'    :   '"',
            '&#39;'     :   "'"
        };

    self.cleanHtml = function (htmlStr){
    	var html = htmlStr;
		var div = document.createElement("div");
		div.innerHTML = html;
		return div.textContent || div.innerText || "";
    };
       
	self.isSameDay = function(date1, date2){
		var year1 = date1.getUTCFullYear(), 
			year2 = date2.getUTCFullYear();
		
		var month1 = date1.getUTCMonth(), 
			month2 = date2.getUTCMonth();
		
		var day1 = date1.getUTCDate(), 
			day2 = date2.getUTCDate();

		return year1 === year2 && month1 === month2 && day1 === day2;
	};

	self.setParameterByName = function(url, name, value){
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
		return url.replace(regex, '&' + name + '=' + value);
	};

	self.getParameterByName = function(name, url) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(url);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};

	self.cleanStrValue = function(str){
		if(str && typeof str === "string") {
			str = self.cleanHtml(str);
			var key;
			for(key in specialChars){
				var r = new RegExp('(' + specialChars[key] + ')', 'g');
				str = str.replace(r, key);
			} 
			return str.replace(/<\/?[^>]+(>|$)/g, "");
		} else {
			return '';
		}
	};

	self.cleanIfStrValue = function(v){
		if(typeof v === 'string'){
			return self.cleanStrValue(v);
		} else {
			return v;
		}
	};

	self.makeStringUrlFromLocation = function(location){
		return ''.concat(location.origin, location.pathname, location.search);
	};

	self.getRequest = function(url){
		var observable = Rx.Observable.create(function(observer) {
			var subscribed = true;
			$.ajax(url, 
			{
				method: 'GET',
				success: function(data){
					if(subscribed){
						observer.onNext(data);
						observer.onCompleted();
					}
				},
				error: function(err){
					if(subscribed){
						observer.onError(err);
					}
				}
			});

			return function() {
				subscribed = false;
			};
		});

		return observable;
	};

	module.exports = self;
});