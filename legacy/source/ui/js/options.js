(function(){
    'use strict';    
    var lic = linkedInTool.modules.require('license'),
        helper = linkedInTool.modules.require('helper'),
        exportHelper = linkedInTool.modules.require('exportHelper'),
        profile = linkedInTool.modules.require('mainProfile');

    function getValidStat(stat){
      if(stat.lastDate && typeof stat.lastDate === 'number'){
        var ld = new Date(stat.lastDate);
        var cd = new Date();
        if(!helper.isSameDay(cd, ld)){
          stat.dayCounter = 0;
        }
      }
      return stat;
    }

    var settingsApp = angular.module('options', []);

    settingsApp.controller('optionsController', ['optionsData', function(optionsData){
      var self = this;
      optionsData.getFileType().then(function(val){
        self.fileType = val;
      });
      
      optionsData.getStat().then(function(val){
        self.stat = getValidStat(val);
      });

      optionsData.getHistorySessions().then(function(val){
        self.historySessions = val;
      });

      optionsData.getMaxContacts().then(function(val){
        self.maxContacts = val;      
      });

      optionsData.getDelay().then(function(val){
        self.delay = val;      
      });

      self.exportFormatClicked = function(formatType){
        self.fileType.type = formatType;
        switch(formatType){
          case 'XML':              
              self.fileType.hasAdditionalSettings = false;
              break;
          case 'CSV':
              self.fileType.hasAdditionalSettings = true;
              break;
          case 'vCard':
              self.fileType.hasAdditionalSettings = false;
              break;
        }
        optionsData.setFileType(self.fileType);
      };

      self.exportFormatSeparatorClicked = function(separator){
        self.fileType.settings.separator = separator;
        optionsData.setFileType(self.fileType);
      };


      self.saveMaxContacts = function(){
        var number = parseInt(self.maxContacts);
        if(!isNaN(number) && number > -1) {
          if(lic.accessLevel === 0 || !lic.accessLevel){
            if(number > linkedInTool.licenseRestriction.maxContacts ){
              number = 500;
              alert('Please, buy subscription if you want to update daily limit');
            }
          } 
          optionsData.setMaxContacts(number);
        }
      }; 

      self.saveDelay = function(){
        var number = parseInt(self.delay);
        if(!isNaN(number) && number >= 10 && number <=10000) {
          optionsData.setDelay(number);
        }
      }; 

      self.calculateSessionProgress = function(resultsLength, membersLength){
        return 100*resultsLength/(resultsLength + membersLength);
      };

      self.removeResults = function(session){
        var hs = [];
        for (var i = 0; i < self.historySessions.length; i++) {
          if(self.historySessions[i].id !== session.id){
            hs.push(self.historySessions[i]);
          }
        }

        self.historySessions = hs;
        chrome.storage.local.set({'history': self.historySessions});
      };

      self.downloadResults = function(session){
        var results = session.map(function(r){
          var p = profile();
          p.init(r);
          return p;
        });
        if(results.length > 0){
          if(self.fileType.type === 'XML'){
            var xml = exportHelper.makeXML(results);
            exportHelper.download(xml, 'xml');
          } else if(self.fileType.type === 'CSV'){
            var csv = exportHelper.makeCSV(results, self.fileType.separator);      
            exportHelper.download(csv, 'csv');
          } else if(self.fileType.type === 'vCard'){
            var vCard = exportHelper.makeVCard(results);
            exportHelper.download(vCard, 'vcf');
          }
        }
      };
    }]);


    settingsApp.factory('optionsData', function ($q) {
        var service = {
            getHistorySessions: function(){
              var defer = $q.defer();
              chrome.storage.local.get('history',
                function(data){
                  if(data.history){
                      console.log('history');
                      console.log(data.history);                    
                      defer.resolve(data.history);
                  } else {
                    defer.resolve([]);
                  }
                });
              return defer.promise;
            },
            getDelay: function(){
                var defer = $q.defer();
                lic.license.then(function(){
                   chrome.storage.sync.get('delay', function(data){
                    if(!data.delay){
                      data.delay = linkedInTool.delay;
                    }
                    
                    defer.resolve(data.delay);
                  });                 
                }); 
                return defer.promise;
            },
            setDelay: function(number){
              var obj = {'delay': number };
              var defer = $q.defer();
              chrome.storage.sync.set(obj, function(resp){
                  console.log('delay is saved');
                  defer.resolve(resp);
              });
              return defer.promise; 
            },
            getMaxContacts: function(){
                var defer = $q.defer();
                lic.license.then(function(){
                   chrome.storage.sync.get('maxContacts', function(data){
                    if(!data.maxContacts){
                      data.maxContacts = linkedInTool.licenseRestriction.maxContacts;
                    }
                    if(lic.accessLevel === 0){
                      if(data.maxContacts > linkedInTool.licenseRestriction.maxContacts ){
                        data.maxContacts = linkedInTool.licenseRestriction.maxContacts;
                      }
                    }                     
                    defer.resolve(data.maxContacts);
                  });                 
                }); 
                return defer.promise;
            },

            setMaxContacts: function(number){
              var obj = {'maxContacts': number };
              var defer = $q.defer();
              chrome.storage.sync.set(obj, function(resp){
                  console.log('maxContacts is saved');
                  defer.resolve(resp);
              });
              return defer.promise; 
            },

            getFileType: function() {
                var defer = $q.defer();
                chrome.storage.sync.get('fileFormat',
                function(data){
                  if(data.fileFormat){      
                    console.log('file settings:');
                    console.log(data);  
                    //app.settings.fileFormat = data.fileFormat;                   
                    
                    defer.resolve(data.fileFormat);
                  } else{
                    var defaultSettings = {type: 'XML', settings: {separator: ';'}, hasAdditionalSettings: false };                    
                    defer.resolve(defaultSettings);
                  }
                });
                return defer.promise;
            },

            setFileType: function(fileType){
              var obj = {'fileFormat': fileType};
              var defer = $q.defer();
              chrome.storage.sync.set(obj, function(resp){
                  console.log('file format is saved');
                  defer.resolve(resp);
              });
              return defer.promise; 
            },

            getStat: function(){
                var defer = $q.defer();
                chrome.storage.sync.get('stat',
                  function(data){
                    if(data.stat){      
                      console.log('statistic');
                      console.log(data);                    
                      defer.resolve(data.stat);
                    } else{
                      var defaultSettings = {
                        dayCounter: 0,
                        lastDate: new Date(),
                        contactsCounter: 0
                      };    
                      defer.resolve(defaultSettings);
                    }                    
                  
                });

                return defer.promise;
            }
        };

        return service;
    });
})();

