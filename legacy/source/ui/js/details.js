(function(){
	'use strict';

	$('#description').block();

	chrome.runtime.onMessage.addListener(function(session, sender, sendResponse){	
		var viewModel = function(s){
			var self = {};
			self.id = s.id;
			self.date = s.date;
			self.members = ko.observableArray(s.members);
			//todo problem with cyrilic symbols it shows as unicode html :( 			
			return self;
		}(session);	

		ko.applyBindings(viewModel, document.getElementById('description'));
		$('#description').unblock();
	});
})();