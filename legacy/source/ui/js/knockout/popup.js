(function(){
    'use strict';   
    var helper = linkedInTool.modules.require('helper'),
        exportHelper = linkedInTool.modules.require('exportHelper'),
        profile = linkedInTool.modules.require('mainProfile'),
        license = linkedInTool.modules.require('license'),
        settings = linkedInTool.modules.require('settings');

    var getValidStat = function (stat){      
      if(stat.lastDate && typeof stat.lastDate === 'number'){
        var ld = new Date(stat.lastDate);
        var cd = new Date();
        if(!helper.isSameDay(cd, ld)){
          stat.dayCounter = 0;
        }
      }
      console.log('statisitcs:');
      console.log(stat);
      return stat;
    };

    var popupApp = angular.module('exportPopup', []);
    
    popupApp.controller('popupController', ['popupData', '$log', function(popupData){
      var self = this;
      popupData.getSettings().then(function(data){ 
        self.maxContacts = data.maxContacts;       
        self.stat = getValidStat(data.stat);
        console.log(data);
        self.fileType = data.fileFormat;
        self.delay = data.delay;
         
        license.license.done(function(accessLevel){
          chrome.tabs.getSelected(null, function(tab) {
            chrome.tabs.sendMessage(tab.id, {action: "updateInfo", data: data, license: accessLevel });        
          });           
        });    

      });

      popupData.getHistorySessions().then(function(val){
        self.historySessions = val;
      });

      self.removeSession = function(session){
        var hs = [];
        for (var i = 0; i < self.historySessions.length; i++) {
          if(self.historySessions[i].id !== session.id){
            hs.push(self.historySessions[i]);
          }
        }

        self.historySessions = hs;

        chrome.storage.local.set({'history': angular.copy(self.historySessions)});
      };

      self.downloadSession = function(session){
        var results = session.results.map(function(r){
          var p = profile();
          p.init(r);
          return p;
        });
        if(results.length > 0){
          if(self.fileType.type === 'XML'){
            var xml = exportHelper.makeXML(results);
            exportHelper.download(xml, 'xml');
          } else if(self.fileType.type === 'CSV'){
            var csv = exportHelper.makeCSV(results, settings.fileFormat.settings.separator);      
            exportHelper.download(csv, 'csv');
          }
        }
      };

      self.exportClicked = function(actionName){
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          chrome.tabs.sendMessage(tab.id, {action: actionName, license: license.accessLevel});
          window.close();          
        });        
      };

      self.calculateSessionProgress = function(resultsLength, membersLength){
        return 100*resultsLength/(resultsLength + membersLength);
      };

      self.resumeDownload = function(session){
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          chrome.tabs.sendMessage(tab.id, {action: "resumeDownload", data: session.id, license: license.accessLevel }, function(/*resp*/){

          });
          window.close();          
        });  
      };

    }]);

    popupApp.factory('popupData', function ($q, $log) {
        var service = {
            //todo use settings.js
            getSettings: function(){
                var lic_type = 2;
                var defer = $q.defer();
                //licDefer.then(function(lic_type){
                  chrome.storage.sync.get(['fileFormat', 'stat', 'maxContacts', 'delay'], function(data){
                    $log.info('geting settigs...');
                    $log.info(data);
                    var result = {};

                    if(data.delay){
                      result.delay = data.delay;
                    } else {
                      result.delay = linkedInTool.delay;
                    }

                    if(data.maxContacts){
                      result.maxContacts = data.maxContacts;
                    } else {
                      result.maxContacts = linkedInTool.licenseRestriction.maxContacts;
                    }

                    if(lic_type === 0 || !lic_type){
                      if(result.maxContacts > linkedInTool.licenseRestriction.maxContacts ){
                        result.maxContacts = linkedInTool.licenseRestriction.maxContacts;
                      }
                    }
                    console.log('fileFormat:', data.fileFormat);
                    if(data.fileFormat){
                      result.fileFormat = data.fileFormat;
                    }
                    else{
                      result.fileFormat = {type: 'XML', settings: {separator: ';'}, hasAdditionalSettings: false };
                    }

                    if(data.stat) {
                      result.stat = data.stat;
                    } else {
                      result.stat = {
                        dayCounter: 0,
                        lastDate: new Date().getTime(),
                        contactsCounter: 0
                      };
                    }
                    $log.info('settigs recived...');
                    $log.info(result);
                    defer.resolve(result);
                  });
                //});
    
                return defer.promise;
            },

            getHistorySessions: function(){
              var defer = $q.defer();
              chrome.storage.local.get('history',
                function(data){
                  $log.info('get history data...');
                  if(data.history){
                      $log.info('history:');
                      $log.info(data.history);                    
                      defer.resolve(data.history);
                  } else {
                    $log.info(data);
                    $log.warn('can\'t get history...');
                    defer.resolve([]);
                  }
                });
              return defer.promise;
            }
        };

        return service;
    });
})();