(function(){

/*
INIT
1. Get License
2. Get Settings
3. Get History
METHODS
1. Lite export
2. Full export
3. Resume
4. Download session
5. Resume from number

*/

    'use strict';

    var helper = linkedInTool.modules.require('helper'),
        exportHelper = linkedInTool.modules.require('exportHelper'),
        profile = linkedInTool.modules.require('mainProfile'),
        license = linkedInTool.modules.require('license'),
        settings = linkedInTool.modules.require('settings');

    var viewModel = function(){
      var self = this;

      self = {
        licenseDescription: '',
        licenseLevel: ko.observable(0),
        hasLicense: ko.observable(false)
      };
      
      self.fullExport = function(){
        if(self.licenseLevel() > 0){
          return exportFunc('doExport');
        }        
      };

      self.liteExport = function(){        
        return exportFunc('doLiteExport');
      };

      var testFunc = function(){
        chrome.tabs.getSelected(null, function(tab) {
          chrome.tabs.sendMessage(tab.id, {action: "test", license: self.licenseLevel()});
          window.close();          
        });      
      };

      var exportFunc = function(actionName){
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          console.log('action: ' + actionName);
          console.log('viewModel: ' + self);
          var msg = {
            action: actionName, 
            license: self.licenseLevel(),
            settings: settings.data
          };
          chrome.tabs.sendMessage(tab.id, msg);
          window.close();          
        });        
      };

      self.getInfo = function(session){
        chrome.tabs.create({url: 'ui/html/details.html', active: false}, function(tab){
          setTimeout(function(){    // hack: to wait tab load 
            chrome.tabs.sendMessage(tab.id, session);
          }, 300);          
        });
      };

      self.downloadSession = function(session){
        var results = session.results.map(function(r){
          var p = profile();
          p.init(r);
          return p;
        });

        if(results.length > 0){
          if(settings.data.fileFormat.type === 'XML'){
            var xml = exportHelper.makeXML(results);
            exportHelper.download(xml, 'xml');
          } else if(settings.data.fileFormat.type === 'CSV'){
            var csv = exportHelper.makeCSV(results, settings.data.fileFormat.settings.separator);      
            exportHelper.download(csv, 'csv');
          }
        } 
      };

      self.resumeExport = function(session){
        console.log('todo: resumeExport');
        console.log(session); 
      };

      self.resumeFrom = function(session){
        console.log('todo: resumeFrom');
        console.log(session);  
      };

      self.removeSession = function(session){
        /* code is fine uncoment when you be sure that all other part of code is working fine

        self.sessions.remove(session);
        chrome.storage.local.set({'history': self.sessions() }, function(){
          if(chrome.runtime.lastError){
            console.error('Error on saving to local storage');
          }
        });*/

      };

      return self;
    }();
 
    viewModel.buttonStatus = ko.pureComputed(function() {
          return  (this.hasLicense() === true)? 'btn-primary' : 'btn-default';
      }, viewModel);



    var initializeLicense = function(){
      $('#popupLi').block({ message: '<h5><small>Retriving license<small></h5>' });
      return license.license    
    };

    var domReady = Rx.DOM.ready().flatMap(initializeLicense).retry(3);

    var source = Rx.Observable.forkJoin(domReady, settings.settings, settings.history).subscribe(
        function(x){
          //console.log(x);
          viewModel.licenseDescription = x[0].accessDescription;
          viewModel.licenseLevel(x[0].accessLevel());
          viewModel.hasLicense(x[0].result);
          //console.log(x[1].counter);
          viewModel.todayParsed = x[1].counter.getDayCounter();
          viewModel.totalParsed = x[1].counter.getCommonCounter();
          viewModel.limit = x[1].maxContacts;
          viewModel.sessions = ko.observableArray(x[2]);

          console.log(viewModel.hasLicense());
        },
        function(error){
          console.error(error);
        },
        function(){
          console.log('Complete');
          console.log(viewModel);
          $('div.container').unblock();
          ko.applyBindings(viewModel, document.getElementById('popupLi'));
        });

    


    /*

    var getValidStat = function (stat){      
      if(stat.lastDate && typeof stat.lastDate === 'number'){
        var ld = new Date(stat.lastDate);
        var cd = new Date();
        if(!helper.isSameDay(cd, ld)){
          stat.dayCounter = 0;
        }
      }
      console.log('statisitcs:');
      console.log(stat);
      return stat;
    };

    popupApp.controller('popupController', ['popupData', '$log', function(popupData){
      var self = this;
      popupData.getSettings().then(function(data){ 
        self.maxContacts = data.maxContacts;       
        self.stat = getValidStat(data.stat);
        console.log(data);
        self.fileType = data.fileFormat;
        self.delay = data.delay;
         
        license.license.done(function(accessLevel){
          chrome.tabs.getSelected(null, function(tab) {
            chrome.tabs.sendMessage(tab.id, {action: "updateInfo", data: data, license: accessLevel });        
          });           
        });    

      });

      popupData.getHistorySessions().then(function(val){
        self.historySessions = val;
      });

      self.removeSession = function(session){
        var hs = [];
        for (var i = 0; i < self.historySessions.length; i++) {
          if(self.historySessions[i].id !== session.id){
            hs.push(self.historySessions[i]);
          }
        }

        self.historySessions = hs;

        chrome.storage.local.set({'history': angular.copy(self.historySessions)});
      };

      self.downloadSession = function(session){

      };

      self.exportClicked = function(actionName){
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          chrome.tabs.sendMessage(tab.id, {action: actionName, license: license.accessLevel});
          window.close();          
        });        
      };

      self.calculateSessionProgress = function(resultsLength, membersLength){
        return 100*resultsLength/(resultsLength + membersLength);
      };

      self.resumeDownload = function(session){
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          chrome.tabs.sendMessage(tab.id, {action: "resumeDownload", data: session.id, license: license.accessLevel }, function(){

          });
          window.close();          
        });  
      };

    }]);

    popupApp.factory('popupData', function ($q, $log) {
        var service = {
            //todo use settings.js
            getSettings: function(){
                var lic_type = 2;
                var defer = $q.defer();
                //licDefer.then(function(lic_type){
                  chrome.storage.sync.get(['fileFormat', 'stat', 'maxContacts', 'delay'], function(data){
                    $log.info('geting settigs...');
                    $log.info(data);
                    var result = {};

                    if(data.delay){
                      result.delay = data.delay;
                    } else {
                      result.delay = linkedInTool.delay;
                    }

                    if(data.maxContacts){
                      result.maxContacts = data.maxContacts;
                    } else {
                      result.maxContacts = linkedInTool.licenseRestriction.maxContacts;
                    }

                    if(lic_type === 0 || !lic_type){
                      if(result.maxContacts > linkedInTool.licenseRestriction.maxContacts ){
                        result.maxContacts = linkedInTool.licenseRestriction.maxContacts;
                      }
                    }
                    console.log('fileFormat:', data.fileFormat);
                    if(data.fileFormat){
                      result.fileFormat = data.fileFormat;
                    }
                    else{
                      result.fileFormat = {type: 'XML', settings: {separator: ';'}, hasAdditionalSettings: false };
                    }

                    if(data.stat) {
                      result.stat = data.stat;
                    } else {
                      result.stat = {
                        dayCounter: 0,
                        lastDate: new Date().getTime(),
                        contactsCounter: 0
                      };
                    }
                    $log.info('settigs recived...');
                    $log.info(result);
                    defer.resolve(result);
                  });
                //});
    
                return defer.promise;
            },

            getHistorySessions: function(){
              var defer = $q.defer();
              chrome.storage.local.get('history',
                function(data){
                  $log.info('get history data...');
                  if(data.history){
                      $log.info('history:');
                      $log.info(data.history);                    
                      defer.resolve(data.history);
                  } else {
                    $log.info(data);
                    $log.warn('can\'t get history...');
                    defer.resolve([]);
                  }
                });
              return defer.promise;
            }
        };

        return service;
    });*/
})();