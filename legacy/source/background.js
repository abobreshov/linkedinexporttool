(function(){
'use strict';
  chrome.runtime.onInstalled.addListener(function() {
    // Replace all rules ...
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
      // With a new rule ...
      chrome.declarativeContent.onPageChanged.addRules([
        {
          // That fires when a page's URL contains a 'www.linkedin.com/contacts OR www.linkedin.com/vsearch/j' ...
          conditions: [
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/contacts' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/people/connections' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/vsearch/p' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/vsearch/f' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/sales/search' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/groups', queryContains: 'viewMembers' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/grp/members', queryContains: 'gid' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/manageGroupMembers' },
            }),
            new chrome.declarativeContent.PageStateMatcher({
              pageUrl: { hostEquals: 'www.linkedin.com', schemes: ['https', 'http'], urlMatches: 'www.linkedin.com/profile/view' },
            })
          ],
          // And shows the extension's page action.
          actions: [ new chrome.declarativeContent.ShowPageAction() ]
        }
      ]);
    });
  });
})();