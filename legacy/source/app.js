var linkedInTool = (function(){
	'use strict';
	//Bugsnag integration
	Bugsnag.apiKey = "bd1ea675c4da74105917ba9c36216546";
	Bugsnag.beforeNotify = function (error/*, metaData*/) {
	    error.stacktrace = error.stacktrace.replace(/chrome-extension:/g, "chromeextension:");
	};

	/* define module section */
	/* ##################### */
	var modules = {};
	var define = function(name, fn){
		if(modules[name]){
			console.error('Module ' + modules[name] + ' is already defined');
		}

		modules[name] = {
			exports: {},
			fn: fn,
			executed: false
		};	
	};

	var require = function(name){
		var module = modules[name];

		if(!module){
			console.error('LinkedIn Tool Module ' + name + ' not found');
		}

		if(!module.executed){
			module.executed = true;
			module.fn.call(module, module, module.exports);
		}
		
		return module.exports;
	};

	/* main app with some predifined constants*/
	var app = {
		modules: {
			define: define,
			require: require
		},
		licenseRestriction: {
			maxContacts: 500
		},
		delay: 10
	};

	app.account = (function(){
		var self = {};
		self.getAccountId = function(){
			var helpers = app.modules.require('helper');
			var href = $('div.account-sub-nav-options li.self .act-set-icon a').attr('href');
			self.accountId = helpers.getParameterByName('id', href);
			return self.accountId;
		};

		self.getCurrentId = function(){
			try{
				return $('div.masthead').attr('id').split('member-')[1];
			} catch(err) {
				console.error('can not parse current id');
				console.log($('div.masthead').attr('id'));
			}		
		};
		return self;
	})();

	return app;
})();