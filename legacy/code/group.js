(function(){
	'use strict';
	var info_url = 'https://www.linkedin.com/profile/profile-v2-connections?id={{id}}&offset={{offset}}&count=10&distance=1&type=ALL';
	
	
	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		app.settings.init();
		app.license.accessLevel = message.license;

		if (message.action === "doExport" ){			
			exportConnections(false);			
		}

		if (message.action === "doLiteExport"){
			exportConnections(true);
		}	

		if(message.action === "resumeDownload" && message.data){
			resumeExportConnections(message.data);
		}	
	});
	app.settings.init();
	app.account.getAccountId();

	function getMembersFromPage(htmlText, members){
		var list = $(htmlText).find('div.content');
		for(var i = 0; i < list.length; i++){
			var element = $(list[i]);
			var name = element.find('h4 a').text();
			var href = element.find('h4 a').attr('href');
			var distance = element.find('.degree-icon').first();

			if(distance && distance.length > 0 ){
				var digit = parseInt(distance[0].childNodes[0]);
				if(!isNaN(digit))
					distance = digit;
				else
					distance = '';
			} else {
				console.log('can\'t take distance');
				console.log(element.find('.degree-icon'));
				distance = '';
			}
			var headlineText = element.find('.member-headline').text();

			var title = '';
			var location = '';
			var headline = headlineText.split(',');
			if(headline.length > 0){
				title = headline[0];
				if(headline.length > 1){
					for(var j = 1; j < headline.length; j++){
						location += headline[j] + ' ';
					}
				}
			}
			var id = app.helpers.url.getParameterByName('id', href);
			if(!id){
				id = '';
			}
			var m = {
				'href': href, 
				'name': app.helpers.cleanStrValue(name), 
				'title': app.helpers.cleanStrValue(title), 
				'company': '', 
				'location': app.helpers.cleanStrValue(location),
				'distance': distance,
				'industry': '',
				'connectionCount': '',
				'id': id
			};
			members.push(m);
		}
	}

	function getMembers() {
		var offset = 1;
		var members = [];
		var defer = jQuery.Deferred();
		var info_url = $('.paginate a').attr('href');
		if(!info_url){
			var html = document.getElementsByTagName("body")[0].innerHTML;
			getMembersFromPage(html,members);
			defer.resolve(members);
		} else {
			app.ui.createProgressBox();
			(function next_page(){				
				var url = app.helpers.url.setParameterByName(info_url, 'split_page', offset);
				$.ajax(url, { method: "GET" }).done(function(resp){
					var htmlElement = $(resp);
					if(offset < htmlElement.find('.paginate a').length){
						app.ui.updateCurrentProgress(null, null, null, 'Get links from page: ' + offset);
						getMembersFromPage(resp,members);
						offset++;
						next_page();					
					} else {
						app.ui.removeProgressBox();
						defer.resolve(members);							
					}
				});
			})();
		}
		return defer.promise();
	}
 
	function resumeExportConnections(sessionId){
		app.exporter.resumeExport(sessionId, app.account.accountId, app.exporter.doExport);
	}

	function exportConnections(isFastTrack){
        if(app.license.accessLevel === 0 || !app.license.accessLevel){
			alert('To enable export contacts of contact, please, buy subscription for LinkedIn Export Tool.');
			return;
        }		 
        try {
			var promise = getMembers();
			promise.done(function(members){
				var acc_type = app.account.getAccountType();
				var acc_id = app.account.getAccountId();
				if(app.settings.debugMode.enabled){
					console.log(acc_type);	
				}
				
				if (!members.length) {
					alert('Please, specify target contacts to process.');
					return;
				}

				if(isFastTrack){
					app.exporter.doFastExport(members);
				} else
					app.exporter.doExport(members);
			});        	
        } catch(error){
        	console.log('Error on group members export...');
        	console.log(error);
        }

	}

})(chrome, console, app);