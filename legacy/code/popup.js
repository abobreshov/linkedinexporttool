(function(){
    'use strict';
    var licDefer = app.license.init();

    function getValidStat(stat){
      console.log('statisitcs:');
      console.log(stat);
      if(stat.lastDate && typeof stat.lastDate === 'number'){
        var ld = new Date(stat.lastDate);
        var cd = new Date();
        console.log(ld);
        console.log(cd);
        if(!app.helpers.isSameDay(cd, ld)){
          console.log("set to 0");
          stat.dayCounter = 0;
        }
      }
      console.log(stat);
      return stat;
    }

    var popupApp = angular.module('exportPopup', []);
    var c = popupApp.controller('popupController', ['popupData', '$log', function(popupData, $log){
      var self = this;
      popupData.getSettings().then(function(data){ 
        self.maxContacts = data.maxContacts;       
        self.stat = getValidStat(data.stat);

        self.fileType = data.fileFormat;
        app.settings.fileFormat = data.fileFormat; 
               
        chrome.tabs.getSelected(null, function(tab) {
          chrome.tabs.sendMessage(tab.id, {action: "updateStat", data: data.stat, license: app.license.accessLevel });        
        }); 
      });

      popupData.getHistorySessions().then(function(val){
        self.historySessions = val;
      });

      self.removeSession = function(session){
        var hs = [];
        for (var i = 0; i < self.historySessions.length; i++) {
          if(self.historySessions[i].id != session.id)
            hs.push(self.historySessions[i]);
        }

        self.historySessions = hs;

        chrome.storage.local.set({'history': angular.copy(self.historySessions)});
      };

      self.downloadSession = function(session){
        var results = session.results;
        var profile = new app.base.profile(self.debugMode);
        if(results.length > 0){
          if(self.fileType.type === 'XML'){
            var xml = app.exporter.helpers.makeXML(results, profile.contactToXml);
            app.exporter.download(xml, 'xml');
          } else if(self.fileType.type === 'CSV'){
            var header = profile.makeCSVHeader(self.fileType.settings.separator) + '\n';
            var csv = header + app.exporter.helpers.makeCSV(results, profile.contactToCSV, self.fileType.settings.separator);
            app.exporter.download(csv, 'csv');
          }
        }
      };

      self.exportClicked = function(actionName){
        //debugger;
        chrome.tabs.getSelected(null, function(tab) {
          //debugger;
          // Send a request to the content script.
          chrome.tabs.sendMessage(tab.id, {action: actionName, license: app.license.accessLevel});
          window.close();          
        });        
      };

      self.calculateSessionProgress = function(resultsLength, membersLength){
        return 100*resultsLength/(resultsLength + membersLength);
      };

      self.resumeDownload = function(session){
        //debugger;
        chrome.tabs.getSelected(null, function(tab) {
          // Send a request to the content script.
          chrome.tabs.sendMessage(tab.id, {action: "resumeDownload", data: session.id, license: app.license.accessLevel }, function(resp){

          });
          window.close();          
        });  
      };

    }]);

    var f = popupApp.factory('popupData', function ($q, $log) {
        var service = {
            getSettings: function(){
                var defer = $q.defer();
                licDefer.then(function(lic_type){
                  chrome.storage.sync.get(['fileFormat', 'stat', 'maxContacts'], function(data){
                    $log.info('geting settigs...');
                    $log.info(data);
                    var result = {};
                    if(data.maxContacts){
                      result.maxContacts = data.maxContacts;
                    } else {
                      result.maxContacts = License_Restriction.maxContacts;
                    }

                    if(lic_type === 0 || !lic_type){
                      if(result.maxContacts > License_Restriction.maxContacts ){
                        result.maxContacts = License_Restriction.maxContacts;
                      }
                    }
                    console.log('fileFormat:', data.fileFormat);
                    if(data.fileFormat){
                      result.fileFormat = data.fileFormat;
                    }
                    else
                      result.fileFormat = app.settings.fileFormat;

                    if(data.stat) {
                      result.stat = data.stat;
                    } else{
                      result.stat = {
                        dayCounter: 0,
                        lastDate: new Date().getTime(),
                        contactsCounter: 0
                      };
                    }
                    $log.info('settigs recived...');
                    $log.info(result);
                    defer.resolve(result);
                  });
                });
    
                return defer.promise;
            },

            getHistorySessions: function(){
              var defer = $q.defer();
              chrome.storage.local.get('history',
                function(data){
                  $log.info('get history data...');
                  if(data.history){
                      $log.info('history:');
                      $log.info(data.history);                    
                      defer.resolve(data.history);
                  } else {
                    $log.info(data);
                    $log.warn('can\'t get history...');
                    defer.resolve([]);
                  }
                });
              return defer.promise;
            }
        };

        return service;
    });
})(chrome, console, angular, app);