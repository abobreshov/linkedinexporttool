(function(){
	'use strict';
	app.parseHelpers.getCurrentId = function(){
		try{
			return $('div.masthead').attr('id').split('member-')[1];
		} catch(err) {
			console.log('can not parse current id');
			console.log($('div.masthead').attr('id'));
		}
		
	};

	app.parseHelpers.html = {
		getFullName: function(htmlJQuery){
			var name = htmlJQuery.find('div.profile-overview span.full-name').text();
			return app.helpers.cleanStrValue(name);
		},

		getTitle: function(htmlJQuery){
			var title = htmlJQuery.find('#headline p.title').text();
			return app.helpers.cleanStrValue(title);
		},

		getLocation: function(htmlJQuery){
			var location = htmlJQuery.find('#location a[name="location"]').text();
			return app.helpers.cleanStrValue(location);
		},
		
		getIndustry: function(htmlJQuery){
			var industry = htmlJQuery.find('#location a[name="industry"]').text();
			return app.helpers.cleanStrValue(industry);
		},

		getDistance: function(htmlJQuery){
			var distanceStr = htmlJQuery.find('span.fp-degree-icon abbr.degree-icon')
				.clone()    //clone the element
				.children() //select all the children
				.remove()   //remove all the children
				.end()		//again go back to selected element
				.text();
			var distance = parseInt(distanceStr);
			if(isNaN(distance)){
				distance = '';
			}
			return distance;
		},

		getCompany: function(htmlJQuery){
			var company = htmlJQuery.find('span.miniprofile-container a').first().text();
			return app.helpers.cleanStrValue(company);
		},

		getCompanyUrl: function(htmlJQuery){
			var company_url = htmlJQuery.find('span.miniprofile-container a').first().attr('href');
			return app.helpers.cleanStrValue(company_url);
		},

		getProfileUrl: function(htmlJQuery){
			var profile_url = htmlJQuery.find('.view-public-profile').text();
			return app.helpers.cleanStrValue(profile_url);
		},


		getEmail: function(htmlJQuery){
			var email = htmlJQuery.find('#email-view a').first().text();
			return email;
		}
			
		
	};

	app.parseHelpers.content = {
		getFullName: function(data){
			if(data.BasicInfo && data.BasicInfo.basic_info && data.BasicInfo.basic_info.fullname){
				return app.helpers.cleanStrValue(data.BasicInfo.basic_info.fullname);
			} else{
				console.log("Can't find fullname from parsed data");
				return '';
			}
		},

		getTitle: function(data){
			if(data.TopCard && data.TopCard.basic_info && data.TopCard.basic_info.memberHeadline){
				return app.helpers.cleanStrValue(data.TopCard.basic_info.memberHeadline);
			} else{
				console.log("Can't find title from parsed data");
				return '';
			}
		},

		getLocation: function(data){
			if(data.BasicInfo && data.BasicInfo.basic_info && data.BasicInfo.basic_info.fmt_location){
				return app.helpers.cleanStrValue(data.BasicInfo.basic_info.fmt_location);
			} else {
				//todo try to get from another source
				//data.BasicInfo.basic_info.location_highlight
				console.log("Can't find location from parsed data");
				return '';
			}
		},

		getDistance: function(data){
			if(data.ContactInfo && data.ContactInfo.distance && data.ContactInfo.distance.distance){
				return data.ContactInfo.distance.distance;
			} else{
				console.log("Can't find distance from parsed data");
				return 0;
			}
		},

		getIndustry: function(data){
			if(data.BasicInfo && data.BasicInfo.basic_info && data.BasicInfo.basic_info.industry_highlight){
				return app.helpers.cleanStrValue(data.BasicInfo.basic_info.industry_highlight);
			} else{
				console.log("Can't find industry from parsed data");
				return '';
			}
		},

		getCompany: function(data){
			if(data.TopCard && data.TopCard.positionsMpr && data.TopCard.positionsMpr.firstTopCurrentPosition && data.TopCard.positionsMpr.firstTopCurrentPosition.companyName){
				return app.helpers.cleanStrValue(data.TopCard.positionsMpr.firstTopCurrentPosition.companyName);
			} else{
				console.log("Can't find company name from parsed data");
				return '';
			}
		},


		getEmail: function(data){
			if(data.ContactInfo && data.ContactInfo.contact_info && data.ContactInfo.contact_info.emails && data.ContactInfo.contact_info.emails.length > 0){
				return data.ContactInfo.contact_info.emails[0].email;
			} else{
				console.log("Can't find email from parsed data");
				return '';
			}
		},

		getProfileUrl: function(data){
			//debugger;
			if(data.TopCard && data.TopCard.public_url && data.TopCard.public_url.canonicalUrl){
				return data.TopCard.public_url.canonicalUrl;
			} else{
				console.log("Can't find profile url from parsed data");
				return '';
			}
		},

		getIsMultipleComp: function(data){
			//debugger;
			if(data.TopCard && data.TopCard.positionsMpr && data.TopCard.positionsMpr.topCurrent){
				return data.TopCard.positionsMpr.topCurrent.length > 1? 'yes' : 'no';
			} else {
				console.log("Can't find multipleComp from parsed data");
				return '';
			}
		},

		getCompanyUrl: function(data){
			if(data.TopCard && data.TopCard.positionsMpr && data.TopCard.positionsMpr.firstTopCurrentPosition && data.TopCard.positionsMpr.firstTopCurrentPosition.companyId){
				var url = _linkedInUrl + "/company/" + data.TopCard.positionsMpr.firstTopCurrentPosition.companyId;
				return url;
			} else{
				console.log("Can't find company url from parsed data");
				return '';
			}
		}
	};
})(console, chrome, app);