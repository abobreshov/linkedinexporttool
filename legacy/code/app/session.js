(function(){
	'use strict';
	
	app.sessions = function(sessionData){
		var self = this;
		var _sessions = sessionData;

		self.getSessions = function(){
			return _sessions;
		};

		self.push = function(results, members, userId){
			var defer = jQuery.Deferred();
			var id = 1;
			if(!_.isEmpty(_sessions) ){
				var mVal = _.max(_sessions, function(session){ return session.id; } );
				id = mVal.id + 1;
			}
			_sessions.push({'id': id, 'userId': userId, 'members': members, 'results': results, 'date': new Date().toDateString() });
			chrome.storage.local.set({'history': _sessions}, function(resp){
				console.log('history has been saved');
				defer.resolve(resp);
			});
			return defer.promise();
		};

		self.pushSession = function(session){
			var s =  _.findWhere(_sessions, {id: session.id});
			var defer = jQuery.Deferred();
			if(s){
				chrome.storage.local.set({'history': _sessions}, function(resp){
					console.log('history has been saved');
					defer.resolve(resp);
				});
			} else {
				var id = 1;
				if(!_.isEmpty(_sessions) ){
					var mVal = _.max(_sessions, function(session){ return session.id; } );
					id = mVal.id + 1;
				}
				_sessions.push({'id': session.id, 'userId': session.userId, 'members': session.members, 'results': session.results, 'date': session.date });	
				
				chrome.storage.local.set({'history': _sessions}, function(resp){
					console.log('history has been saved');
					defer.resolve(resp);
				});			
			}
			return defer.promise();
		}

		self.popById = function(id){
			var session = null;//_.findWhere(_sessions, {id: id} );
			var result = [];
			for(var i = 0; i < _sessions.length; i++){
				if(_sessions[i].id !== id){
					result.push(_sessions[i]);
				} else {
					session = _sessions[i];
				}
			}
			_sessions = result;
			var obj = {'history': result };
			chrome.storage.local.set(obj, self.refresh);
			
			return session;
		};

		self.refresh = function(){
			var defer = jQuery.Deferred();
			chrome.storage.local.get('history',
				function(data){
					if(data.history)
						_sessions = data.history;
					defer.resolve(_sessions);
				});
			return defer.promise();
		};

		return self;
	};
})(console, chrome, app);