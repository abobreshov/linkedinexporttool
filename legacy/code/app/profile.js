(function(){
	'use strict';
	
	var titles = {
		'fullname': 'Contact_Name',
		'birthday': 'Birthday',
		'email': 'Email',
		'phone': 'Phone',
		'location': 'Location',
		'title': 'Title',
		'industry': 'Industry',
		'biz_prof':  'Company_Link',
		'company': 'Company',
		'distance': 'Distance',		
		'profile_url': 'Profile',
		'site': 'Website',
		'im': 'IM',
		'id': 'ID'
		//'multi_comp': 'Has_Several_Companies',
		//'parent': 'Main_Contact'
	};

	app.base.profile = function(){
		var self = this;
		self.makeCSVHeader = function(separator){
			var result = '';
			for(var property in titles){
				if(titles.hasOwnProperty(property)){
					var t = titles[property];
					result = result.concat(t,separator);
				}
			}
			return result;
		};
		self.contactToCSV = function(contact, separator){
			var result = '';
			//console.log(contact);
			for(var property in titles){
				if(contact.hasOwnProperty(property)){
					var v = contact[property];
					var str = '';
					if(typeof v === 'string'){
						str = v;
					} else if(v === parseInt(v)){
						str = (v).toString();
					} else{
						console.log('can not cast to string value:' + v);
					}

					str = str.replace(/,|;/g, " ");

					result = result.concat(str,separator);
				}

			}
			return result;
		};

		self.contactToXml = function(contact){
			var result = '    <contact>\n';
			//console.log(contact);
			for(var property in titles){
				if(titles.hasOwnProperty(property))
				{
					var v = contact[property];
					var t = titles[property];
					var elem = app.exporter.helpers.makeXMLElement(t, v);
					result = result.concat(
						'      ',
						elem,
						'\n'
					);
				}
			}

			result = result.concat('    </contact>\n');
			return result;
		};
		

		self.parseContactHtml = function(htmlText, cid){
			//debugger;
			var profileInfo = [];
			var data = findAllDataContent(htmlText);
			for(var property in data){
				if(data.hasOwnProperty(property)){
					for(var i = 0; i < data[property].length; i++){
						var info = data[property][i].data;
						var profile = data[property][i].getData(info);
						profileInfo.push(profile);
					}

				}
			}		

			//debugger;
			var parsedHtmlProfile = getDataFromHtml(htmlText);
			if(parsedHtmlProfile){
				profileInfo.push(parsedHtmlProfile);
			}

			if(profileInfo.length === 1){
				return profileInfo;
			}

			if(profileInfo.length > 1){
				//todo compare and fill maximum fields for profile
				return profileInfo;				
			}

			return null;
		};


		self.parseContactJson = function(jsonObj){
			var pr = jsonObj.contact_data;
			if(pr){

				var fullname = app.helpers.cleanStrValue(pr.name);
				var title = app.helpers.cleanStrValue(pr.title);
				var location = app.helpers.cleanStrValue(pr.location);
				var company = '';
				if(pr.company){
					company = app.helpers.cleanStrValue(pr.company.name);
				}
				var email = '';
				if(pr.emails_extended && pr.emails_extended.length > 0){
					email = pr.emails_extended[0].email;
				}

				var i = 0; //itterator

				var phone = '';
				if(pr.phone_numbers && pr.phone_numbers.length > 0){
					for(i = 0; i < pr.phone_numbers.length; i++){
						phone = phone.concat(pr.phone_numbers[i].type,': ',pr.phone_numbers[i].number, ';');
					}
				}

				var ims = '';
				if(pr.ims && pr.ims.length > 0){
					for(i = 0; i < pr.ims.length; i++){
						ims = ims.concat(pr.ims[i].type,': ',pr.ims[i].name, ';');
					}
				}

				// "sites": [
				//{"name": "Personal Website", "visible": true, "url": "http://", "blog": false, "source": "LinkedIn", "id": "Personal Website"}, 
				//{"name": "Company Website", "visible": true, "url": "http://", "blog": false, "source": "LinkedIn", "id": "Company Website"}, 
				//{"name": "abobreshov", "visible": true, "url": "http://twitter.com/", "blog": false, "source": "LinkedIn", "id": "abobreshov"}]
				var website = '';
				if(pr.sites && pr.sites.length > 0){
					for(i = 0; i < pr.sites.length; i++){
						website = website.concat(pr.sites[i].url,'   ');
					}
				}
				
				//"birthday": "3/8/1991" "birthday": null
				var birthday = '';
				if(pr.birthday){

					birthday = pr.birthday;
				}

				//empty fields
				var profile_url = ''; //data has link but not to the public profile
				var companyUrl = '';
				var industry = '';
				var distance = 0;

				return {
					'fullname': fullname,
					'birthday': birthday,
					'location': location,
					'title': title,
					'company': company,
					'industry': industry,
					'biz_prof': companyUrl,
					'multi_comp': null,
					'distance': distance,
					'profile_url': profile_url,
					'email': email,
					'phone': phone,
					'im': app.helpers.cleanStrValue(ims), 
					'site': app.helpers.cleanStrValue(website),
					'id': '' //todo
				};//new app.base.userInfo(fullname, location, title, company, industry, companyUrl, null, distance, profile_url, email, null, phone, ims, website, bir);
			} else{
				//todo error
				return null;
			}

		};

		function getDataFromHtml(html){
			var data = $(html);			
			var parser = app.parseHelpers.html;
			//debugger;
			var fullname = parser.getFullName(data);
			var title = parser.getTitle(data);
			var location = parser.getLocation(data);
			var company = parser.getCompany(data);
			var email = parser.getEmail(data);
			var profile_url = parser.getProfileUrl(data);
			var industry = parser.getIndustry(data);
			var distance = parser.getDistance(data);
			var companyUrl = parser.getCompanyUrl(data);
			var multipleComp = '';
			return {
					'fullname': fullname,
					'bir': '',
					'location': location,
					'title': title,
					'company': company,
					'industry': industry,
					'biz_prof': companyUrl,
					'multi_comp': multipleComp,
					'distance': distance,
					'profile_url': profile_url,
					'email': email,
					'phone': '',
					'im': '', 
					'site': '',
					'id': '' //todo
				}; 
		}
		function getDataFromContent(data){
			var parser = app.parseHelpers.content;	
			//data.errorCounter = 0;		
			var fullname = parser.getFullName(data);
			var title = parser.getTitle(data);
			var location = parser.getLocation(data);
			var company = parser.getCompany(data);
			var email = parser.getEmail(data);
			var profile_url = parser.getProfileUrl(data);
			var industry = parser.getIndustry(data);
			var distance = parser.getDistance(data);
			var companyUrl = parser.getCompanyUrl(data);
			var multipleComp = parser.getIsMultipleComp(data);
			return {
					'fullname': fullname,
					'bir': '',
					'location': location,
					'title': title,
					'company': company,
					'industry': industry,
					'biz_prof': companyUrl,
					'multi_comp': multipleComp,
					'distance': distance,
					'profile_url': profile_url,
					'email': email,
					'phone': '',
					'im': '', 
					'site': '',
					'id': '' //todo
				}; 
		}

		function getDataFromContactDetails(data){
			var fullname = '';			
			//data.errorCounter = 0;
			if(data.hasOwnProperty('name')){
				fullname = data.name;
			} //else{
				//data.errorCounter++;
			//}
			//data.errorCounter += 10;			
			return {
					'fullname': fullname,
					'bir': '',
					'location': '',
					'title': '',
					'company': '',
					'industry': '',
					'biz_prof': '',
					'multi_comp': '',
					'distance': '',
					'profile_url': '',
					'email': '',
					'phone': '',
					'im': '', 
					'site': '',
					'id': ''
				};
		}

		function findAllDataContent(html){
			var i = 0, j, s = '', 
				data = {content: [], contactDetails: []}, 
				js;
			try {
				i = html.indexOf('<!--{', i + 1);
				j = html.indexOf('}-->', i);
				//console.log(html);
				while(i > 0){

					s = html.substr(i + 4, j - i - 3)
						.replace(/&dsh;/g, '-')
						.replace(/\\u002d([1-9])/g, '$1');
					try {
						js = JSON.parse(s);
						//console.log(js);
					} catch (e){
						js = {};
						console.log('Could not parse:');
						console.log(s);
						//todo send error
					}
					if(js.content && js.content.BasicInfo && js.content.TopCard && js.content.ContactInfo){
						data.content.push({ data: js.content, getData: getDataFromContent });
					}

					if(js.contact_details){
						data.contactDetails.push({ data: js.contact_details, getData: getDataFromContactDetails} );
					}
					
					i = html.indexOf('<!--{', i + 1);
					j = html.indexOf('}-->', i);
				}


			} catch(e) {
				console.log("Can't parse data [findAllDataContent]");
				console.log(e);
				//todo send error
			}

			return data;
		}

		return self;
	};


})(console, app);
