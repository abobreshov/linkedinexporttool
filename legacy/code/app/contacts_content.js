(function(){
	'use strict';
	//var profile_v2_url = 'https://www.linkedin.com/profile/profile-v2-connections?id={1}&offset={2}&count=10&distance=1&type=ALL';
	var info_url = 'https://www.linkedin.com/contacts/api/contacts/{{id}}/?fields=id,name,emails,emails_extended,birthday,phone_numbers,sites,addresses,company,title,location,ims,profiles,twitter,wechat,display_sources';
	
	app.exporter.resumeExport = function(sessionId, userId, callback){
		var session = app.history.popById(sessionId);
		if(session){
			if(userId && session.userId && userId !== session.userId){
				if(app.history){	
					app.history.pushSession(session).then(function(){
						alert('Can\'t resume export. Session was saved by another user. (Current user id:' + userId + ' Session user id:' + session.userId + ')');
					});
				}
			} else {
				app.ui.createProgressBox();
				var members = session.members;
				var historyResults = session.results;
				callback(members, historyResults, userId);
			}
		} else{
			alert('Please, refresh the page and try it again.');
		}
	};

	app.exporter.doFastExport = function(members){
		app.ui.createProgressBox();
		var results = [];
		var contactsAmount = members.length;
		var profile = new app.base.profile(app.settings.debugMode);
		for(var i = 0; i < members.length; i++){
			app.ui.updateCurrentProgress(i, contactsAmount, 'Contacts:');
			var p = {
					'fullname': members[i].name,
					'birthday': '-',
					'location': members[i].location,
					'title': members[i].title,
					'company': members[i].company,
					'industry': members[i].industry,
					'biz_prof': '-',
					'multi_comp': '-',
					'distance': '-',
					'profile_url': app.helpers.cleanStrValue(members[i].href),
					'email': '-',
					'phone': '-',
					'im': '-', 
					'site': '-',
					'id': members[i].id
				};
			results.push(p);
		}

		downloadFile(profile, results);
	};

	app.exporter.doExport = function(members, historyResults, userId) {
		if(!userId){
			userId = null;
		}
		app.ui.createProgressBox();
		app.ui.enablePauseButton();
		var results = [];
		var contactsAmount = members.length;
		if(historyResults){
			contactsAmount += historyResults.length;
			results = historyResults;
		}
		var profile = new app.base.profile();
		(function next_contact() {
			var cid;

			if (!members.length || app.ui.forceStop) {
				downloadFile(profile, results);
				app.counter.commit();
				return;
			}
			
			var dayParseCount = app.counter.getDayCounter();
			//debugger;
			if(app.ui.pauseClicked || (app.settings.maxContacts > 0 && app.settings.maxContacts <= dayParseCount)){
				if(app.history){
					app.history.refresh().then(function(){
						//debugger;
						app.history.push(results, members, userId);
					});		
					app.counter.commit();		
					if(app.ui.pauseClicked){
						alert('Pause. All other contacts have stored in local storage.');
					} else
						alert('You have downloaded ' + app.settings.maxContacts + ' for today. All other contacts have stored in local storage. Please, resume export tomorrow or use fast export.');
					app.ui.removeProgressBox();					
					return;
				}
			}

			var member = members.shift();	
			app.ui.updateCurrentProgress(contactsAmount - members.length, contactsAmount, 'Contacts:', member.name);
			cid = member.href;
			var id = app.helpers.url.getParameterByName('id', cid);			
			var url = info_url.replace('{{id}}', id);

			if(cid.indexOf('/contacts/') === 0){
				//debugger;
				$.when($.ajax(cid, { method: "GET" }), $.ajax(url, { method: "GET" }))
					.done( function(html, json){
						if(!parseConnections(html, json)) {
							members.push(member);
							if(app.history){
								app.history.refresh().then(function(){
									app.history.push(results, members, userId);
								});	
							}
							app.ui.forceStop = true;
							alert('LinkedIn has asked to logon or banned you. Extraction progress has stored in local storage. Please, refresh the page and resume extraction process from history.');							
							return;
						}
					}).then(next_contact);
			} else{
				//debugger;
				$.when($.ajax(cid, { method: "GET" }))
					.done(parseContactsOfContact).then(next_contact);
			}
			
			app.counter.increment();
			
			function parseConnections(resp1, resp2){
				try{
					//debugger;
					var html = resp1[0];
					if(!validateResponse(html)){
						return false;
					}
					var jsonData = resp2[0];
					var contacts = profile.parseContactHtml(html, cid);	
					var jsonContact = profile.parseContactJson(jsonData);				
					if(jsonContact && contacts && contacts.length >= 0){
						contacts.push(jsonContact);
					}

					var p = mergeParsedInfo(contacts);				

					results.push(p);
					return true;		
					
				} catch(err){
					console.log(err);
					return false;
					//app.logger.log("Error");
					//app.logger.log(err);
				}
			}
			
			function validateResponse(html){
				if($(html).find('#login').length > 0){
					return false;
				}

				return true;
			}

			function mergeParsedInfo(contacts){
				var mainContact = {
						'fullname': member.name,
						'birthday': null,
						'location': null,//member.location,
						'title': null,//member.title,
						'company': null,//member.company,
						'industry': null,
						'biz_prof': null,
						'multi_comp': null,
						'distance': null,
						'profile_url': null,
						'email': null,
						'phone': null,
						'im': null, 
						'site': null,
						'id': member.id
					};
					//debugger;
					var p = app.exporter.helpers.aggregateProfiles(mainContact, contacts);
					if(!p.profile_url || p.profile_url === '-' || p.profile_url === ''){
						p.profile_url = app.helpers.cleanStrValue(member.href);
					}

					return p;
			}

			function parseContactsOfContact(resp1){
				try{
					var html = resp1;
					if(!validateResponse(html)){
						return false;
					}
					var contacts = profile.parseContactHtml(html, cid);	
					var p = mergeParsedInfo(contacts);
					results.push(p);
					return true;
					
				} catch(err){
					//app.logger.log("Error");
					console.log(err);
					return false;
				}
			}
		})();
	};

	function downloadFile(profile, results){
		if(app.settings.fileFormat.type === 'XML'){
			var xml = app.exporter.helpers.makeXML(results, profile.contactToXml);
			app.exporter.download(xml, 'xml');
		} else if(app.settings.fileFormat.type === 'CSV'){
			var header = profile.makeCSVHeader(app.settings.fileFormat.settings.separator) + '\n';
			var csv = header + app.exporter.helpers.makeCSV(results, profile.contactToCSV, app.settings.fileFormat.settings.separator);
			app.exporter.download(csv, 'csv');
		}
		app.ui.removeProgressBox();
	}

	//todo: move to profile.js
	app.exporter.helpers.aggregateProfiles = function(mainProfile, profiles){
		if(!profiles || !mainProfile){
			//todo: check 
			return null;
		}
		var prop;
		for(var i = 0; i < profiles.length; i++){
			var profile = profiles[i];
			
			for(prop in profile){
				if(mainProfile.hasOwnProperty(prop) && profile.hasOwnProperty(prop)){
					if(!mainProfile[prop] || mainProfile[prop] === ''){
						if(profile[prop] && profile[prop] !== '')
							mainProfile[prop] = profile[prop];
					}
				}
			}
		}

		//mark empty
		for(prop in mainProfile){
			if(mainProfile.hasOwnProperty(prop) && !mainProfile[prop]){
				mainProfile[prop] = '-';
			}
		}

		return mainProfile;
	};

})(console, chrome, app);