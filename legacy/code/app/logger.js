(function(){
	'use strict';
	var MAX_STORAGE_SIZE = 2*1000*1000; //~ 2 Mb

	app.logger = {
		logContext: [],
		log: function(value){
			if(value && value !== null){
				console.log(value);
				this.logContext.push(value);
			}
		},
		getLogFile: function(){
			var str = "";
			for (var i = 0; i < this.logContext.length; i++) {
				var info = this.logContext[i];
				str = str.concat(getInfoStr(info), '\n');
			}
			app.exporter.download(str, 'txt', 'log');
		},
		commit: function(){
			chrome.storage.local.set({'log': this.logContext});
		}

	};

	chrome.storage.local.get('log', function(data){
		if(data.log)
			app.logger.logContext = data.log;
	});
	chekStorage();

	function getInfoStr(obj){
		if(typeof obj === 'string'){
			return obj;
		} else if(typeof obj !== 'function' && typeof obj.prototype === 'undefined'){
			return JSON.stringify(obj);
		} else {
			console.log('WARN: possible error in getInfoStr() function');
			return '';
		}
	}

	function chekStorage(){
		chrome.storage.local.getBytesInUse('log', function(data){
			if(data > MAX_STORAGE_SIZE){
				chrome.storage.local.remove('log', function(resp){
					console.log('log is removed');
				});
			}
		});
	}

	function clearLogs(){
		chrome.storage.local.remove('log', function(resp){
			console.log('log is removed');
		});
	}

})(console, chrome, app);