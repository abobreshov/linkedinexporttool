(function(){
	'use strict';

	var CWS_LICENSE_API_URL = 'https://www.googleapis.com/chromewebstore/v1.1/userlicenses/';
	var TRIAL_PERIOD_DAYS = 15;	

	app.license.init = function(){
		var retry = true, interactive = true;
		var d = jQuery.Deferred();
		// delete it for normal edition
		app.license.accessLevel = 2;//--
		d.resolve(app.license.accessLevel);//--
		return d.promise();//--
		//--------------------------------------
		function getToken() {
			console.log("Calling chrome.identity.getAuthToken", interactive);
			chrome.identity.getAuthToken({ interactive: interactive }, function(token) {
				if (chrome.runtime.lastError) {
					console.log(chrome.runtime.lastError);
					console.log(token);
					d.resolve(0);
					return; 
				}
				console.log("chrome.identity.getAuthToken returned a token", token);
				var url = CWS_LICENSE_API_URL + chrome.runtime.id;
				$.ajax({
					url: url,
					type: 'GET',
					headers: {
						'Authorization': 'Bearer ' + token
					},
					success: function(data, textStatus, jqXHR){
						console.log("Authenticated completed");
						if (this.status === 401 && retry) {
							retry = false;
							chrome.identity.removeCachedAuthToken({ token: token },getToken);
						} else {
							app.license.accessLevel = parseLicense(data);
							d.resolve(app.license.accessLevel);
						}
					},
					error: function(){
						app.logger.log('Error: Can not get license');
						d.resolve(0);
					}
				});
			});			
		}

		function parseLicense(license) {
			if (license.result && license.accessLevel === "FULL") {
				//FULL
				return 2;
			} else if (license.result && license.accessLevel === "FREE_TRIAL") {
			var daysAgoLicenseIssued = Date.now() - parseInt(license.createdTime, 10);
			daysAgoLicenseIssued = daysAgoLicenseIssued / 1000 / 60 / 60 / 24;
			if (daysAgoLicenseIssued <= TRIAL_PERIOD_DAYS) {
				//FREE_TRIAL
				return 1;
			} else {
				//EXPIRED
				return 0;
			}
			} else {
				//EXPIRED
				return 0;
			}
		}
		getToken();
		return d.promise(); 
	};


})(console, chrome, app);