var License_Types = ["EXPIRED", "FREE_TRIAL", "FULL"];
/*
0 - free - no licence
1 - trial period license
2 - full license
*/
var License_Restriction = {
	maxContacts: 500
};

var app = {
	license: { accessLevel: 2 },
	exporter: {
		helpers: {}
	},
	account: { accountId: null },
	ui: {},
	base: {},
	parseHelpers:{
		
	},
	helpers: {
		url: {}
	},
	counter: null,
	history: null,
	settings: {
		'fileFormat': {type: 'XML', settings: {separator: ';'} },
		'stat': {
			dayCounter: 0,
			lastDate: new Date().getTime(),
			contactsCounter: 0
		},
		'debugMode': {enabled: false},
		'maxContacts': License_Restriction.maxContacts,
		setStat: function(s){
			this.stat.lastDate = s.lastDate;
			this.stat.contactsCounter = s.contactsCounter;
			this.stat.dayCounter = s.dayCounter;
			if(!app.counter)
				app.counter = new app.base.counter();
			app.counter.set(this.stat.dayCounter, this.stat.contactsCounter, this.stat.lastDate);
		},
		init: function(){
			if(!app.counter)
				app.counter = new app.base.counter();
			chrome.storage.sync.get(['fileFormat', 'stat', 'debugMode', 'maxContacts'],
				function(data){
					if(data.fileFormat)
						app.settings.fileFormat = data.fileFormat;

					if(data.stat) {
						if(data.stat.lastDate && typeof data.stat.lastDate === 'number'){
							var ld = new Date(data.stat.lastDate);
							var cd = new Date();
							if(app.helpers.isSameDay(cd, ld)){
								app.counter.set(data.stat.dayCounter, data.stat.contactsCounter, data.stat.lastDate);
							} else {
								app.counter.set(0, data.stat.contactsCounter, data.stat.lastDate);
							}
						}
						
					}
					/*
					if(data.debugMode)
						app.settings.debugMode = data.debugMode;
					*/
					if(data.maxContacts)
						app.settings.maxContacts = data.maxContacts;
				});
			
			chrome.storage.local.get('history', function(data){
					if(data.history){
						app.history = new app.sessions(data.history);
					} else{
						app.history = new app.sessions([]);
					}
			});
		} 
	},
	sessions: {
		sync: null
	}
};

(function(){
	'use strict';

	app.account.getAccountType = function(){
		app.account.accountType = $(".account-settings .account-type .act-set-name").text().trim();
		return app.account.accountType;
	};
	
	app.account.getAccountId = function(){
		var href = $('div.account-sub-nav-options li.self .act-set-icon a').attr('href');
		app.account.accountId = app.helpers.url.getParameterByName('id', href);
		return app.account.accountId;
	};
	
})(console, chrome, app);