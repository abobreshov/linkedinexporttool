(function(){
	'use strict';
	
	app.base.counter = function(){
		var self = this;
		var _dayCounter = 0, _counter = 0;
		var _date = new Date().getTime();

		function isSameDay(date1, date2){
			var year1 = date1.getUTCFullYear(), year2 = date2.getUTCFullYear();
			var month1 = date1.getUTCMonth(), month2 = date2.getUTCMonth();
			var day1 = date1.getUTCDate(), day2 = date2.getUTCDate();

			return year1 === year2 && month1 === month2 && day1 === day2;
		}

		self.set = function(dayCounter, mainCounter, lastDate){
			_counter = mainCounter;

			if(lastDate && typeof lastDate === 'number'){
				var ld = new Date(lastDate);
				var cd = new Date(_date);
				if(isSameDay(cd, ld)){
					_dayCounter = dayCounter;
				}
			}

		};

		self.get = function(){
			return { dayCounter: _dayCounter, counter: _counter };
		};

		self.getDayCounter = function(){
			return _dayCounter;
		};

		self.getCommonCounter = function(){
			return _counter;
		};

		self.increment = function(){
			var d = new Date();
			var ld = new Date(_date);
			if(isSameDay(d, ld)){
				_dayCounter++;
			} else{
				_dayCounter = 0;
				_date = d.getTime();
			}
			
			_counter++;
		};

		self.commit = function(){
			var defer = jQuery.Deferred();
			var obj = { 'stat': {
					dayCounter: _dayCounter,
					lastDate: new Date().getTime(),
					contactsCounter: _counter
				} 
			};
			chrome.storage.sync.set(obj, function(resp){
                  console.log('stat is saved');
                  defer.resolve(resp);
            });

            return defer.promise();
		};

		return self;
	};

})(console, chrome, app);