(function(){
	'use strict';
	
	app.exporter.helpers.makeCSV = function(results, csvPredicate, separator){
		return  '' + 
			results.map(function (tp) {
				if(tp){
					return csvPredicate(tp, separator);
				} else{
					return '';
				}                
            }).join('\n');
	};

	app.exporter.helpers.makeXML = function(results, xmlPredicate){
		//debugger;
		return '<result>\n' + 
			results.map(function (tp) {
				if(tp){
					return xmlPredicate(tp);
				} else{
					return '';
				}
                
            }).join('\n') + 
            '\n</result>';
	};

	app.exporter.helpers.makeXMLElement = function(name, value){
		return '<' + name + '>' + value + '</' + name + '>';
	};

	app.exporter.download = function(str, contentType, fileName){
		if(!fileName || fileName === '' || typeof fileName !== 'string'){
			fileName = 'LinkedIn Connections';
		}
		var a = document.createElement('a');
		a.id = 'mysave';
		a.download = fileName + '.' + contentType;
		a.href = 'data:application/' + contentType + ';charset=utf-8,' + encodeURIComponent(str);
		document.body.appendChild(a);
		a.click();
		document.body.removeChild(document.getElementById('mysave'));
	};


})(console, chrome, app);