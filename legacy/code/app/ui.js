(function(){
	'use strict';
		
	app.ui.progressBoxOptions = {
		id: "extProgressBox",
		title: "Exporting connections",
		buttonId: "exp_conn_pause_btn"
	};
	app.ui.pauseClicked = false;

	app.ui.enablePauseButton = function(){
		$('#' + app.ui.progressBoxOptions.buttonId).removeAttr('disabled');
	};

	app.ui.createProgressBox = function(){
		$('#' + app.ui.progressBoxOptions.id).remove();
		$('<div id="' + app.ui.progressBoxOptions.id + '" class="ext_ProgressBox"><p>' + app.ui.progressBoxOptions.title + '</p><div class="ext_ExportBox ext_info"><span></span><br/><span></span><div><button disabled class="ext_PauseButton" id="'+app.ui.progressBoxOptions.buttonId+'">Pause</button></div></div></div>')
		.appendTo('body');
		$('#' + app.ui.progressBoxOptions.buttonId).click(function(){
			app.ui.pauseClicked = true;
		});
	};

	app.ui.removeProgressBox = function(){
		app.ui.pauseClicked = false;
		$('#' + app.ui.progressBoxOptions.id).remove();
	};

	app.ui.updateCurrentProgress = function(currentVal, maxVal, description, currentStateInfo){
		if(!description){
			description = '';
		}
		if(!currentStateInfo){
			currentStateInfo = '';
		}
		var progressStr = '';
		if(currentVal && maxVal){
			progressStr = currentVal + ' / ' + maxVal;
		}
		$('#' + app.ui.progressBoxOptions.id + ' div span').eq(0).text(currentStateInfo)
		.parent().find('span').eq(1).text(description + ' ' + progressStr);
	};

})(console, chrome, app);