(function(){
	'use strict';
	var _linkedInUrl = 'https://linkedin.com',
		specialChars = {
            '&amp;'     :   '&',
            '&gt;'      :   '>',
            '&lt;'      :   '<',
            '&quot;'    :   '"',
            '&#39;'     :   "'"
        };

    function cleanFromHtml(htmlStr){
    	var html = htmlStr;
		var div = document.createElement("div");
		div.innerHTML = html;
		return div.textContent || div.innerText || "";
    }

	app.helpers.cleanStrValue = function(str){
		if(str && typeof str === "string") {
			str = cleanFromHtml(str);
			var key;
			for(key in specialChars){
				var r = new RegExp('(' + specialChars[key] + ')', 'g');
				str = str.replace(r, key);
			} 
			return str.replace(/<\/?[^>]+(>|$)/g, "");
		} else
		{
			return '';
		}
	};
	
	app.helpers.isSameDay = function(date1, date2){
		var year1 = date1.getUTCFullYear(), year2 = date2.getUTCFullYear();
		var month1 = date1.getUTCMonth(), month2 = date2.getUTCMonth();
		var day1 = date1.getUTCDate(), day2 = date2.getUTCDate();

		return year1 === year2 && month1 === month2 && day1 === day2;
	};

	app.helpers.url.getParameterByName = function(name, url) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(url);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	};

	app.helpers.url.setParameterByName = function(url, name, value){
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
		return url.replace(regex, '&' + name + '=' + value);
	};

	/*
	app.parseHelpers.getParametrFromUrl = function(name, url){
		var vars = [], hash;
		var hashes = url.slice(url.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++)
		{
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		if(vars[name])
			return vars[name];
		else
			return null;
	};*/
})(console, chrome, app);