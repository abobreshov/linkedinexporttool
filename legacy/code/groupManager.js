(function(){
	'use strict';

	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		app.settings.init();
		app.license.accessLevel = message.license;

		if (message.action === "doExport" ){			
			exportConnections(false);			
		}

		if (message.action === "doLiteExport"){
			exportConnections(true);
		}	

		if(message.action === "resumeDownload" && message.data){
			resumeExportConnections(message.data);
		}	
	});
	app.settings.init();
	app.account.getAccountId();

	function getMembersFromPage(htmlText, members){
		var membersContainer = $(htmlText).find('table.manager-list');
		var memberList = membersContainer.find('td.participant');

		for(var i = 0; i < memberList.length; i++){
			var member = $(memberList[i]);
			var memberId = member.attr('data-li-itemkey').slice(4);
			var memberLink = member.find('div.content h3 a');
			var href = memberLink.attr('href');
			var name = memberLink.text();
			members.push({
				'href': href, 
				'name': app.helpers.cleanStrValue(name), 
				'title': '', 
				'company': '', 
				'location': '',
				'distance': '',
				'industry': '',
				'id': memberId
			});
		}		
	}

	function getMembers() {
		var offset = 1;
		var members = [];
		var defer = jQuery.Deferred();
		var info_url = $('.paginate a').attr('href');
		var groupId = $('#gid-participantListForm').val();
		if(!info_url){
			var html = document.getElementsByTagName("body")[0].innerHTML;
			getMembersFromPage(html,members);
			defer.resolve(members);
		} else {
			app.ui.createProgressBox();
			(function next_page(){				
				var url = app.helpers.url.setParameterByName(info_url, 'split_page', offset);
				$.ajax(url, { method: "GET" }).done(function(resp){
					var htmlElement = $(resp);
					if(htmlElement.find('p.paginate span').first().text() === 'Next'){
						getMembersFromPage(resp,members);
						app.ui.removeProgressBox();
						defer.resolve(members);	
					} else {
						app.ui.updateCurrentProgress(null, null, null, 'Get links from page: ' + offset);
						getMembersFromPage(resp,members);
						offset++;
						next_page();
					}
				});
			})();
		}
		return defer.promise();
	}
 
	function resumeExportConnections(sessionId){
		app.exporter.resumeExport(sessionId, app.account.accountId, app.exporter.doExport);
	}

	function exportConnections(isFastTrack){
        if(app.license.accessLevel === 0 || !app.license.accessLevel){
			alert('To enable export contacts from group, please, buy subscription for LinkedIn Export Tool.');
			return;
        }		 
        try {
			var promise = getMembers();
			promise.done(function(members){
				var acc_type = app.account.getAccountType();
				var acc_id = app.account.getAccountId();
				if (!members.length) {
					alert('Please, specify target contacts to process.');
					return;
				}

				if(isFastTrack){
					app.exporter.doFastExport(members);
				} else
					app.exporter.doExport(members);
			});        	
        } catch(error){
        	console.log('Error on group members export...');
        	console.log(error);
        }

	}

})(chrome, console, app);