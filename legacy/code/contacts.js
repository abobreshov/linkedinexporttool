(function(){
	'use strict';	

	var members_link = 'https://www.linkedin.com/contacts/api/contacts/{{action}}/?start={{0}}&count={{1}}&fields=id%2Cname%2Cfirst_name%2Clast_name%2Ccompany%2Ctitle%2Cgeo_location%2Ctags%2Cemails%2Csources%2Cdisplay_sources%2Clast_interaction%2Csecure_profile_image_url&{{sort}}';
	var contact_url = '/contacts/view?id={{0}}&trk=contacts-contacts-list-contact_name-0';
	
	var filterQueryList = [
		'tag',
		'location',
		'company',
		'title',
		'source',
		'searchQuery',
		'letter'
	];
				
	var filterMapper = {
		'tag': {name: 'tag_id', action: 'full'},
		'location': {name: 'location', action: 'full'},
		'company': {name: 'company_id', action: 'full'},
		'title': {name: 'title', action: 'full'},
		'source': {name: 'source', action: 'full'},
		'searchQuery': {name: 'name', action: 'search'},
		'letter': {name: 'name', action: 'search', additional: 'starts_with=true'}		
	};

	var filterOrderList = ['sortOrder',	'filter'];

	var filterOrderMapper = {
		'new': '-created',
		'recent': '-last_interaction',
		'first_name': 'name',
		'last_name': 'last_name'
	};

	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		app.settings.init();

		if (message.action === 'doExport'){
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			exportConnections(true);
		}	

		if(message.action === 'resumeDownload' && message.data){
			resumeExportConnections(message.data);
		}

		if(message.action === 'updateStat' && message.data){			
			app.settings.setStat(message.data);
		}
	});

	app.settings.init();
	app.account.getAccountId();
	
	function getMembers() {
		var countText = $('span.contacts-count strong').text();
		var contactsCount = parseInt(countText.slice(1,-1));
		contactsCount = 100;	
		var offset = 0;
		var members = [];
		var defer = jQuery.Deferred();
		var link = members_link;
		if($('#select-all-checkbox-container').has(':checked').length > 0){
			var current_url = window.location.href;			
			//debugger;
			//set proper sorting
			var sortQuery =  _.find(filterOrderList, function(prop){return !_.isEmpty(app.helpers.url.getParameterByName(prop,current_url));});
		
			if(sortQuery){
				var sortParam = app.helpers.url.getParameterByName(sortQuery,current_url);
				link = link.replace('{{sort}}', 'sort=' + filterOrderMapper[sortParam]);
			} else {
				link = link.replace('{{sort}}', 'sort=-last_interaction');
			}

			//set proper filtering
			var filterQuery = _.find(filterQueryList, function(prop){return !_.isEmpty(app.helpers.url.getParameterByName(prop,current_url));});
			var query = '&';
			if(filterQuery){
				var value = app.helpers.url.getParameterByName(filterQuery,current_url);
				var mappedQuery = filterMapper[filterQuery];
				query = query.concat(mappedQuery.name, '=', encodeURIComponent(value));
				if(mappedQuery.additional){
					query = query.concat('&', mappedQuery.additional);
				}
				link = link.replace('{{action}}', mappedQuery.action);
			} else {
				link = link.replace('{{action}}', 'more');
			}

			app.ui.createProgressBox();
			(function next_page(){
				var url = link.replace('{{0}}', offset).replace('{{1}}', contactsCount).concat(query);
				$.ajax(url, { method: 'GET' }).done(function(resp){
					if(resp.status === 'success' && resp.contacts && resp.contacts.length > 0){
						for(var i = 0; i < resp.contacts.length; i++){
							var c = resp.contacts[i];
							var company = '';
							if(c.company){
								company = c.company.name;
							}
							var location = '';
							if(c.geo_location){
								location = c.geo_location.name;
							}
							var member = {
								'href': contact_url.replace('{{0}}', c.id), 
								'name': app.helpers.cleanStrValue(c.name), 
								'title': app.helpers.cleanStrValue(c.title), 
								'company': app.helpers.cleanStrValue(company), 
								'location': location,
								'distance': '',
								'id': c.id,
							};
							app.ui.updateCurrentProgress(i + offset + 1, resp.paging.total, 'Get contacts links: ', '');
							members.push(member);
						}

						if(resp.paging && resp.paging.total > (resp.paging.count + resp.paging.start)){
							offset += contactsCount;
							next_page();
						} else {
							app.ui.removeProgressBox();
							defer.resolve(members);						
						}
					}					
				});
			})();
		} else {
			defer.resolve(getSelectedMembers());
		}		

		return defer.promise();
	}


	function getSelectedMembers() {
		return $.map(
			$('.contact-item-view:has(input:checked)'),
			function (elem) { 
				var v = $(elem);
				var link = v.find('div.body .name a');
				var h = link.attr('href');
				var name = link.text();
				var title = v.find('div.body a.title').text();
				var company = v.find('div.body a.company').text();
				var location = v.find('div.body a.location').text();
				var id = app.helpers.url.getParameterByName('id', h);
				if(!id){
					id = '';
				}
				return {'href': h, 
						'name': app.helpers.cleanStrValue(name), 
						'title': app.helpers.cleanStrValue(title), 
						'company': app.helpers.cleanStrValue(company), 
						'location': app.helpers.cleanStrValue(location),
						'distance': '', //todo
						'industry': '',
						'id': id
					}; 
			}
		);
	}

	function resumeExportConnections(sessionId){
		app.exporter.resumeExport(sessionId, app.account.accountId, app.exporter.doExport);
	}

	function exportConnections(isFastTrack){
		var acc_type = app.account.getAccountType();
		var acc_id = app.account.getAccountId(); 

		if(app.settings.debugMode.enabled){
			console.log(acc_type);	
		}
		var promise = getMembers();		
		promise.done(function(members){
			if (!members.length) {
				alert('Please, specify target contacts to process.');
				return;
			}			
			
			if(isFastTrack){
				app.exporter.doFastExport(members);
			} else {
				app.exporter.doExport(members, null, acc_id);
			}
		});
	}

})(chrome, console, app);
