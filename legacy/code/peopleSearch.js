(function(){
	'use strict';
	//var url_base = '/vsearch/pj?';
	//var url_replace = '/vsearch/p?';
	
	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		app.settings.init();
		app.license.accessLevel = message.license;

		if (message.action === "doExport" ){			
			exportConnections(false);			
		}

		if (message.action === "doLiteExport"){
			exportConnections(true);
		}	

		if(message.action === "resumeDownload" && message.data){
			resumeExportConnections(message.data);
		}	
	});
	app.settings.init();
	app.account.getAccountId();

	function fillMembers(array, members){
		for(var i = 0; i < array.length; i++){
			var member = array[i].person;
			if(member){
				var h;
				if(member.link_nprofile_view_3){
					h = member.link_nprofile_view_3;
				} else if(member.link_nprofile_view_headless){
					h = member.link_nprofile_view_headless;
				} else {
					console.log(member);
					continue;
				}
				var name = '';
				if(member.fmt_name){
					name = app.helpers.cleanStrValue(member.fmt_name);
				}
				var location = '';
				if(member.fmt_location){
					location = app.helpers.cleanStrValue(member.fmt_location);
				}
				var industry = '';
				if(member.fmt_industry){
					industry = app.helpers.cleanStrValue(member.fmt_industry);
				}
				var connectionCount = '';
				if(member.connectionCount){
					connectionCount = member.connectionCount;
				}
				var m = {
					'href': h, 
					'name': name, 
					'title': '', 
					'company': '', 
					'location': location,
					'distance': member.distance,
					'industry': industry,
					'connectionCount': connectionCount,
					'id': member.id			
				};
				members.push(m);				
			}
		}
	}

	function getMembersFromPage(htmlText, members){
		var list = $(htmlText).find('#results-container .search-results li.people .bd');
		for(var i=0; i < list.length; i++ ){
			var c = list[i];
			var href = $(c).find('a.title').attr('href');
			var name = $(c).find('a.title').text();
			var title = $(c).find('div.description').text();
			var industry = $(c).find('.demographic dd').not('.separator').text();
			var location = $(c).find('.demographic bdi').not('.separator').text();
			var distance = $(c).find('.degree-icon').first();
			if(distance && distance[0]){
				var digit = parseInt(distance[0].childNodes[0]);
				if(!isNaN(digit))
					distance = digit;
				else
					distance = '';
			} else {
				distance = '';
			}
			var id = app.helpers.url.getParameterByName('id', href);
			if(!id){
				id = '';
			}
			//debugger;
			var member = {
				'href': href, 
				'name': app.helpers.cleanStrValue(name), 
				'title': app.helpers.cleanStrValue(title), 
				'company': '', 
				'location': app.helpers.cleanStrValue(location),
				'distance': distance,
				'industry': app.helpers.cleanStrValue(industry),
				'id': id
			};

			members.push(member);
		}
	}

	function getMembers() {
		var offset = 1;
		var members = [];
		var defer = jQuery.Deferred();

		var current_urlpath = document.location.pathname;
		var info_url = $('#results-pagination a').attr('href');
		if(!info_url){
			var html = document.getElementsByTagName("body")[0].innerHTML;
			getMembersFromPage(html,members);
			defer.resolve(members);
		} else {
			var count = parseInt($('div.search-info strong').text());
			var url = info_url.replace(current_urlpath, current_urlpath + 'j');
			app.ui.createProgressBox();

			(function next_page(){				
				url = app.helpers.url.setParameterByName(url, 'page_num', offset);
				$.ajax({
					url: url,
					method: "GET",
					success: function(resp){
						//resp.content.page.voltron_unified_search_json.search.baseData.resultCount
						//resp.content.page.voltron_unified_search_json.search.baseData.resultPagination
						if(resp.content && resp.content.page && resp.content.page.voltron_unified_search_json && resp.content.page.voltron_unified_search_json.search && resp.content.page.voltron_unified_search_json.search.baseData.resultPagination){
							var search = resp.content.page.voltron_unified_search_json.search;
							if(search.results && search.results.length > 0){
								app.ui.updateCurrentProgress(null, null, null, 'Get links from page: ' + offset);
								fillMembers(search.results, members);
								offset++;							
								next_page();
							} else{
								app.ui.removeProgressBox();
								defer.resolve(members);
							}
						} else {
							app.ui.removeProgressBox();
							defer.resolve(members);
						}			
					},
					error: function(resp){
						console.log(resp);
						defer.resolve(members);
					}
				});
			})();
		}
		return defer.promise();
	}
 
	function resumeExportConnections(sessionId){
		app.exporter.resumeExport(sessionId, app.account.accountId, app.exporter.doExport);
	}

	function exportConnections(isFastTrack){
        if(app.license.accessLevel === 0 || !app.license.accessLevel){
			alert('To enable export contacts of contact, please, buy subscription for LinkedIn Export Tool.');
			return;
        }

		var promise = getMembers();
		promise.done(function(members){
			var acc_type = app.account.getAccountType();
			var acc_id = app.account.getAccountId(); 

			if(app.settings.debugMode.enabled){
				console.log(acc_type);	
			}
			
			if (!members.length) {
				alert('Please, specify target contacts to process.');
				return;
			}

			if(isFastTrack){
				app.exporter.doFastExport(members);
			} else
				app.exporter.doExport(members);
		});
	}
})(chrome, console, app);
