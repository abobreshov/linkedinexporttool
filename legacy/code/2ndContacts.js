(function(){
	'use strict';
	var info_url = 'https://www.linkedin.com/profile/profile-v2-connections?id={{id}}&offset={{offset}}&count=10&distance=1&type=ALL';
	
	
	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		app.settings.init();
		app.license.accessLevel = message.license;

		if (message.action === "doExport" ){			
			exportConnections(false);			
		}

		if (message.action === "doLiteExport"){
			exportConnections(true);
		}	

		if(message.action === "resumeDownload" && message.data){
			resumeExportConnections(message.data);
		}	
	});
	app.settings.init();

	function getMembers() {
		//debugger;
		var offset = 0;
		var id = app.parseHelpers.getCurrentId();
		var members = [];
		var defer = jQuery.Deferred();

		app.ui.createProgressBox();

		(function next_page(){
			var url = info_url.replace('{{id}}', id).replace('{{offset}}', offset);
			$.ajax(url, { method: "GET" }).then(function(resp){
				//debugger;
				if(resp.content.connections && resp.content.connections.connections && resp.content.connections.connections.length > 0){
					var connections = resp.content.connections.connections;
					for(var i = 0; i < connections.length; i++){
						var c = connections[i];
						app.ui.updateCurrentProgress(i + offset, resp.content.connections.numAll, 'Get contacts of contact:', '');
						var company = '';
						var tmpStrArr = c.headline.split(' at ');
						if(tmpStrArr.length > 1){
							company = tmpStrArr[1];
						}
						try {
							if(!c.pview){
								continue;
							}
							var member = {
								'href': c.pview, 
								'name': app.helpers.cleanStrValue(c.fmt__full_name), 
								'title': app.helpers.cleanStrValue(c.headline), 
								'company': app.helpers.cleanStrValue(company), 
								'location': '',
								'distance': c.distance,
								'industry': '',
								'id': c.memberID
							};							
						} catch(err){
							console.log(err);
							continue;
						}						

						members.push(member);
					}
					offset += 10;
					next_page();
				} else {
					app.ui.removeProgressBox();
					defer.resolve(members);
				}					
			});
		})();

		return defer.promise();
	}
 
	function resumeExportConnections(sessionId){
		app.exporter.resumeExport(sessionId, app.account.accountId, app.exporter.doExport);
	}

	function exportConnections(isFastTrack){
        if(app.license.accessLevel === 0 || !app.license.accessLevel){
			alert('To enable export contacts of contact, please, buy subscription for LinkedIn Export Tool.');
			return;
        }

		var promise = getMembers();
		promise.done(function(members){
			var acc_type = app.account.getAccountType();
			var acc_id = app.account.getAccountId(); 
			if(app.settings.debugMode.enabled){
				console.log(acc_type);	
			}
			
			if (!members.length) {
				alert('Please, specify target contacts to process.');
				return;
			}
			
			if(isFastTrack){
				app.exporter.doFastExport(members);
			} else
				app.exporter.doExport(members);
		});
	}

})(chrome, console, app);