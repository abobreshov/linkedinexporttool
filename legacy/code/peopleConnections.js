(function(){
	'use strict';	

	//var members_link = ''
	var contact_url = 'https://www.linkedin.com/profile/view?id={{0}}';
	
	chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
		app.settings.init();

		if (message.action === 'doExport'){
			exportConnections(false);
		}

		if (message.action === 'doLiteExport'){
			exportConnections(true);
		}	

		if(message.action === 'resumeDownload' && message.data){
			resumeExportConnections(message.data);
		}

		if(message.action === 'updateStat' && message.data){			
			app.settings.setStat(message.data);
		}
	});

	app.settings.init();
	app.account.getAccountId();

	function getMembers() {
		return $.map(
			$('#list-panel li:has(input:checked)'),
			function (elem) { 
				var v = $(elem);
				var h = contact_url.replace('{{0}}',elem.id);
				var name = v.find('span.conn-name').text();				
				var title = v.find('span.conn-headline').text();
				var company = v.find('span.company-name').text();
				return {'href': h, 
						'name': app.helpers.cleanStrValue(name), 
						'title': app.helpers.cleanStrValue(title), 
						'company': app.helpers.cleanStrValue(company), 
						'location': '',
						'distance': '',
						'industry': '',
						'id': elem.id,
						'connections': '' //todo
					}; 
			}
		);
	}

	function resumeExportConnections(sessionId){
		app.exporter.resumeExport(sessionId, app.account.accountId, app.exporter.doExport);
	}

	function exportConnections(isFastTrack){
		var acc_type = app.account.getAccountType();
		var acc_id = app.account.getAccountId(); 

		if(app.settings.debugMode.enabled){
			console.log(acc_type);	
		}
		var members = getMembers();	
		if (!members.length) {
			alert('Please, specify target contacts to process.');
			return;
		}			
		
		if(isFastTrack){
			app.exporter.doFastExport(members);
		} else {
			app.exporter.doExport(members, null, acc_id);
		}
	}

})(chrome, console, app);