
(function(){
    'use strict';
    var lic = app.license.init();

    function getValidStat(stat){
      if(stat.lastDate && typeof stat.lastDate === 'number'){
        var ld = new Date(stat.lastDate);
        var cd = new Date();
        if(!app.helpers.isSameDay(cd, ld)){
          stat.dayCounter = 0;
        }
      }

      return stat;
    }

    var settingsApp = angular.module('options', []);
    var c = settingsApp.controller('optionsController', ['optionsData', '$log', function(optionsData, $log){
      var self = this;
      optionsData.getFileType().then(function(val){
        self.fileType = val;
      });
      
      optionsData.getStat().then(function(val){
        self.stat = getValidStat(val);
      });

      optionsData.getHistorySessions().then(function(val){
        self.historySessions = val;
      });

      optionsData.getMaxContacts().then(function(val){
        self.maxContacts = val;      
      });

      self.exportFormatClicked = function(formatType){
        self.fileType.type = formatType;
        switch(formatType){
          case 'XML':              
              self.fileType.hasAdditionalSettings = false;
              break;
          case 'CSV':
              self.fileType.hasAdditionalSettings = true;
              break;
        }
        optionsData.setFileType(self.fileType);
      };

      self.exportFormatSeparatorClicked = function(separator){
        //debugger;
        self.fileType.settings.separator = separator;
        optionsData.setFileType(self.fileType);
      };

      self.debugModeClicked = function(isOn){
        self.debugMode.enabled = isOn;
        optionsData.setDebugMode(self.debugMode.enabled);
      };

      self.getLogClicked = function(){
        //debugger;
        app.logger.getLogFile();
      };

      self.saveMaxContacts = function(){
        var number = parseInt(self.maxContacts);
        if(!isNaN(number) && number > -1) {
          if(app.license.accessLevel === 0 || !app.license.accessLevel){
            if(number > License_Restriction.maxContacts ){
              number = 500;
              alert('Please, buy subscription if you want to update daily limit');
            }
          } 
          optionsData.setMaxContacts(number);
        }
      }; 

      self.calculateSessionProgress = function(resultsLength, membersLength){
        return 100*resultsLength/(resultsLength + membersLength);
      };

      self.removeResults = function(session){
        var hs = [];
        for (var i = 0; i < self.historySessions.length; i++) {
          if(self.historySessions[i].id != session.id)
            hs.push(self.historySessions[i]);
        }

        self.historySessions = hs;
        chrome.storage.local.set({'history': self.historySessions});
      };

      self.downloadResults = function(results){
        var profile = new app.base.profile(self.debugMode);
        if(results.length > 0){
          if(self.fileType.type === 'XML'){
            var xml = app.exporter.helpers.makeXML(results, profile.contactToXml);
            app.exporter.download(xml, 'xml');
          } else if(self.fileType.type === 'CSV'){
            var header = profile.makeCSVHeader(self.fileType.separator) + '\n';
            var csv = header + app.exporter.helpers.makeCSV(results, profile.contactToCSV, self.fileType.settings.separator);
            app.exporter.download(csv, 'csv');
          }
        }
      };
    }]);

    var f = settingsApp.factory('optionsData', function ($q, $log) {
        var service = {
            getHistorySessions: function(){
              var defer = $q.defer();
              chrome.storage.local.get('history',
                function(data){
                  if(data.history){
                      $log.info('history');
                      $log.info(data.history);                    
                      defer.resolve(data.history);
                  } else {
                    defer.resolve([]);
                  }
                });
              return defer.promise;
            },
            
            getMaxContacts: function(){
                var defer = $q.defer();
                lic.then(function(lic_type){
                  chrome.storage.sync.get('maxContacts', function(data){
                    if(!data.maxContacts)
                      data.maxContacts = License_Restriction.maxContacts;

                    if(lic_type === 0 || !lic_type){
                      if(data.maxContacts > License_Restriction.maxContacts ){
                        data.maxContacts = License_Restriction.maxContacts;
                      }
                    }                     
                    defer.resolve(data.maxContacts);
                  });
       
                });
 
                return defer.promise;
            },

            setMaxContacts: function(number){
              var obj = {'maxContacts': number };
              var defer = $q.defer();
              chrome.storage.sync.set(obj, function(resp){
                  $log.info('maxContacts is saved');
                  defer.resolve(resp);
              });
              return defer.promise; 
            },

            getFileType: function() {
                var defer = $q.defer();
                chrome.storage.sync.get('fileFormat',
                function(data){
                  if(data.fileFormat){      
                    $log.info('file settings:');
                    $log.info(data);  
                    app.settings.fileFormat = data.fileFormat;                   
                    defer.resolve(data.fileFormat);
                  } else{
                    var defaultSettings = {type: 'XML', settings: {separator: ';'}, hasAdditionalSettings: false };                    
                    defer.resolve(defaultSettings);
                  }
                });
                return defer.promise;
            },

            setFileType: function(fileType){
              var obj = {'fileFormat': fileType};
              var defer = $q.defer();
              chrome.storage.sync.set(obj, function(resp){
                  $log.info('file format is saved');
                  defer.resolve(resp);
              });
              return defer.promise; 
            },

            getStat: function(){
                var defer = $q.defer();
                chrome.storage.sync.get('stat',
                  function(data){
                    //debugger;
                    if(data.stat){      
                      $log.info('statistic');
                      $log.info(data);                    
                      defer.resolve(data.stat);
                    } else{
                      var defaultSettings = {
                        dayCounter: 0,
                        lastDate: new Date(),
                        contactsCounter: 0
                      };    
                      defer.resolve(defaultSettings);
                    }                    
                  
                });

                return defer.promise;
            }
        };

        return service;
    });
})(chrome, console, angular, app);

