var requestor = function(url){
	var promise = $.ajax({
	    url: 'https://api.github.com/users?since=',
	    dataType: 'jsonp',
	    data: {
	        action: 'opensearch',
	        format: 'json',
	        search: 'since=100'
	    }
	}).promise();
	var observable =  Rx.Observable.fromPromise(promise);	
	return observable;
};

var url = 'bla-bla';
var count = 3;

(function onNext(){
	

	var subscription = requestor(url).subscribe(
		function(x){console.log(x);},
		function(err){console.log(err);},
		function(c){
			count--;
			if(count > 0){
				console.log('Next');
				onNext();
			} else{
				console.log('done');
			}
		});
	// ??
	subscription.dispose();
})();



//note: this peace of code is candidate to be splited and refactored in near future
/*
members.forEach(
	response => {},
	error => {},
	() => {}
	//do smthing
	);*/
//var results = members.forEach(reuest).concatAll();
//downloadFile(results);

//requestPromise.done(callback).fail(error).then(next_contact);

function(window, getJSON, showMovieLists, showError) {
	var movieListsSequence =
		Observable.zip(
			getJSON("http://api-global.netflix.com/abTestInformation").
				concatMap(function(abTestInformation) {
					return Observable.zip(
						getJSON("http://api-global.netflix.com/" + abTestInformation.urlPrefix + "/config").
							concatMap(function(config) {
								if (config.showInstantQueue) {
									return getJSON("http://api-global.netflix.com/" + abTestInformation.urlPrefix + "/queue").
										map(function(queueMessage) {
											return queueMessage.list;
										});
								}
								else {
									return Observable.returnValue(undefined);
								}
							}),
						getJSON("http://api-global.netflix.com/" + abTestInformation.urlPrefix + "/movieLists"),
						function(queue, movieListsMessage) {
							var copyOfMovieLists = Object.create(movieListsMessage.list);
							if (queue !== undefined) {
								copyOfMovieLists.push(queue);
							}

							return copyOfMovieLists;
						});
				}),
			Observable.fromEvent(window, "load"),
			function(movieLists, loadEvent) {
				return movieLists;
			});

	movieListsSequence.
		forEach(
			function(movieLists) {
				showMovieLists(movieLists);
			},
			function(err) {
				showError(err);
			});
}
		
function(window, $) {
	var getJSON = function(url) {
		return Observable.create(function(observer) {
			var subscribed = true;

			$.getJSON(url,
				{
					success:
						function(data) {
							// If client is still interested in the results, send them.
							if (subscribed) {
								// Send data to the client
								observer.onNext(data);
								// Immediately complete the sequence
								observer.onCompleted();
							}
						},
					error: function(ex) {
						// If client is still interested in the results, send them.
						if (subscribed) {
							// Inform the client that an error occurred.
							observer.onError(ex);
						}
					}
				});

			// Definition of the Subscription objects dispose() method.
			return function() {
				subscribed = false;
			}
		});
	};

	var subscription =
		getJSON("http://api-global.netflix.com/abTestInformation").
			forEach(
				// observer.onNext()
				function(data) {
					alert(JSON.stringify(data));
				},
				// observer.onError()
				function(err) {
					alert(err)
				},
				// observer.onCompleted()
				function() {
					alert("The asynchronous operation has completed.")
				});
}