/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Metadata.
    pkg: grunt.file.readJSON('package.json'),
    
    jshint: {
      options: {
        browser: true, 
        devel: true,
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        unused: false,
        boss: true,
        esnext: true,
        eqnull: true,
        reporter: require('jshint-summary'),//'node_modules/jshint-stylish',//require('jshint-summary'),
        globals: { 
          'jQuery': true,
          'chrome': true,
          'linkedInTool': true,
          '$': true,
          '_': true,
          'Bugsnag': true,
          'Rx': true,
          'ko': true
        },
        ignores: ['source/libs/*.js', 'source/ui/bootstrap/js/*.js']
      },
      code: ['source/**/*.js'],
      build: ['build/tmp/**/*.js']
    },
    
    copy:{
      build:
      {
        files: [ 
          {
            expand: true, 
            cwd: 'source', 
            src: ['libs/*.js',  'ui/**', '!ui/js/**', '!ui/html/exportProgress.html', 'background.js', '!manifest.json'], 
            dest: 'bin/'
          },
          {
            expand: true, 
            cwd: 'config', 
            src: ['manifest.json'], 
            dest: 'bin/'
          },
          {
            expand: true, 
            cwd: 'build/tmp/pages', 
            src: ['*.js'], 
            dest: 'bin/'
          },
          {
            expand: true, 
            cwd: 'build/tmp', 
            src: ['ui/js/*.js'], 
            dest: 'bin/'
          }
        ]
      },
      test:{
         files: [ 
          {
            expand: true, 
            cwd: 'source', 
            src: ['libs/*.js',  'ui/**', '!ui/js/**', 'background.js'], 
            dest: 'bin/'
          },
          {
            expand: true,
            cwd: 'config', 
            src: 'manifest.json', 
            dest: 'bin/'
          },
          {
            expand: true, 
            cwd: 'build/tmp/pages/', 
            src: ['*.js'], 
            dest: 'bin/app/'
          },
          {
            expand: true, 
            cwd: 'build/tmp/ui/', 
            src: ['*.js'], 
            dest: 'bin/ui/'
          }
        ]
      } 
    },

    clean: {
      bin: ['bin/**/'],
      build: ['build/**/']
    },
    
    concat: {
      basic: {
        src: [ 
          'source/app.js',
          'source/app/counter.js',
          'source/app/exporter.js',
          'source/app/exportHelper.js',
          'source/app/helpers.js',
          //'source/app/license.js',
          'source/app/parseHelper.js',
          'source/app/parser.js',
          'source/app/profile.js',
          'source/app/session.js',
          //'source/app/settings.js',
          'source/app/ui.js'
        ],
        dest: 'build/tmp/app.js'
      },
      pages: {
        files: {
          'build/tmp/pages/search.js' : ['build/tmp/app.js', 'source/app/pages/search.js'],
          'build/tmp/pages/connected.js'  : ['build/tmp/app.js', 'source/app/pages/connected.js'],
          //'build/tmp/pages/salesSearch.js'  : ['build/tmp/app.js', 'source/app/pages/salesSearch.js'],
        }
      },
      /*
    <script src="../../app.js"></script>
    <script src="../../app/helpers.js"></script>
    <script src="../../app/counter.js"></script>
    <script src="../../app/exportHelper.js"></script>
    <script src="../../app/profile.js"></script>
    <script src="../../app/license.js"></script>
    <script src="../../app/settings.js"></script>
    <script src="../js/popup.js"></script>
    <script src="../js/uicommon.js"></script>

        <script src="../../app.js"></script>
    <script src="../../app/helpers.js"></script>
    <script src="../../app/counter.js"></script>
    <script src="../../app/exportHelper.js"></script>
    <script src="../../app/profile.js"></script>
    <script src="../../app/license.js"></script>
    <script src="../../app/settings.js"></script>
    <script src="../js/options.js"></script>
    <script src="../js/uicommon.js"></script>
    */
      ui : {
        files: {
          'build/tmp/ui/js/popup.js'   :   ['source/app.js',
                                            'source/app/counter.js',
                                            'source/app/exportHelper.js',
                                            'source/app/helpers.js',
                                            'source/app/license.js',
                                            'source/app/profile.js',
                                            'source/app/settings.js',
                                            'source/ui/js/uicommon.js',
                                            'source/ui/js/popup.js' ],
          'build/tmp/ui/js/options.js' :   ['source/app.js',
                                            'source/app/counter.js',
                                            'source/app/exportHelper.js',
                                            'source/app/helpers.js',
                                            'source/app/license.js',
                                            'source/app/profile.js',
                                            'source/app/settings.js',
                                            'source/ui/js/uicommon.js',
                                            'source/ui/js/options.js' ],
          'build/tmp/ui/js/details.js' :   ['source/ui/js/details.js' ]
        }
      }
    },

    uglify: {
      buildJS: {
        options: {
          mangle: {
            except: ['jQuery', 'ko', 'Rx', '_', 'Bugsnag', 'chrome', 'console']
          },
          reserved: '$,require,define,chrome,linkedInTool,_,Rx,ko,console,Bugsnag'
        },
        files: {
          'bin/app/search.js'       : ['build/tmp/pages/search.js'],
          'bin/app/connected.js'      : ['build/tmp/pages/connected.js']
          //'bin/app/salesSearch.js'    : ['build/tmp/pages/salesSearch.js']
        }
      },
      buildUI: {
        options: {
          mangle: false
        },
        files: {
          'bin/ui/options.js'         : ['build/tmp/ui/options.js'],
          'bin/ui/popup.js'           : ['build/tmp/ui/popup.js'],
          'bin/ui/details.js'         : ['build/tmp/ui/details.js']
        }
      },
      beautifyJS: {
        options: {
          mangle: false,
          beautify: true
        },
        files: {
          'bin/app/search.js'       : ['build/tmp/pages/search.js'],
          'bin/app/connected.js'      : ['build/tmp/pages/connected.js']
        }
      }
    },

    browserify: {
        dist: {
            files: {
              'bin/app/search.js'         : ['build/tmp/pages/search.js'],
              'bin/app/connected.js'      : ['build/tmp/pages/connected.js']
            }
        },
        options: {
            transform: ['uglifyify']
        }
    },

    zip: {
      build: {
        cwd: 'bin',
        src: ['bin/**'],
        dest: 'bin/linkedinexport.zip'
      }
    },
    manifestPath: 'source/manifest.json'
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-zip');
  

  grunt.registerTask('updateVersion', function (updateIndex) {
      var i = updateIndex || 2;
      
      var path = grunt.config('manifestPath');
      
      if (!grunt.file.exists(path)){
         grunt.log.writeln('Error: File ' + path + ' not found ');
         return false;
      }

      var manifest = grunt.file.readJSON(path);
      var version = manifest['version'].split('.');
      if(i < version.length){
        var numberToIncrease = parseInt(version[i],10) + 1; 
        version[i] = numberToIncrease;
        manifest['version'] = version.join('.');
        grunt.log.writeln(manifest['version']);
        grunt.file.write(path, JSON.stringify(manifest, null, 2));
      } else {
        grunt.log.writeln('Error: updateIndex incorrect');
        return false;
      }


      return true;
  });

  grunt.registerTask('setVersion', function (newVersion) {
      var path = grunt.config('manifestPath');
      
      if (!grunt.file.exists(path)){
         grunt.log.writeln('Error: File ' + path + ' not found ');
         return false;
      }

      var manifest = grunt.file.readJSON(path);
      manifest['version'] = newVersion;
      grunt.log.writeln(manifest['version']);
      grunt.file.write(path, JSON.stringify(manifest, null, 2));      
      
      return true;
  });

  // Default task.
  //grunt.registerTask('playground', ['clean:bin', 'concat:basic', 'concat:pages', 'concat:ui', 'copy:test', 'clean:build']);

  //grunt.registerTask('test', ['clean:bin', 'concat:basic', 'concat:pages', 'concat:ui', 'jshint:build', 'copy:test', 'clean:build']);
  grunt.registerTask('build', ['clean:bin', 'jshint:code', 'concat:basic', 'concat:pages', 'concat:ui', 'jshint:build', 'copy:build', 'clean:build', 'zip:build']);
  //grunt.registerTask('beautyBuild', ['clean:bin', 'concat:basic', 'concat:pages', 'concat:ui', 'uglify:beautifyJS', 'uglify:buildUI', 'jshint:build', 'copy:build', 'clean:build']);
};
